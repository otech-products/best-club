﻿using System.Web.Mvc;
using Nop.Web.Framework.Security;
using Nop.Web.Models.Common;
using Nop.Core;
using Nop.Services.Messages;
using Nop.Web.Framework;
using Nop.Web.Models.Newsletter;
using Nop.Web.Models.Home;
using Nop.Web.Models.Customer;
using Nop.Core.Domain.Customers;
using Nop.Services.Stores;
using System;
using Nop.Services.Orders;
using System.Collections.Generic;
using Nop.Services.Customers;
using Nop.Services.Localization;
using WebGrease.Css.Extensions;
using System.Net.Http;
using Nop.Core.Domain.Stores;
using Nop.Web.Framework.Security.Captcha;
using Nop.Core.Domain.Messages;
using System.Web;
using Nop.Web.ServiceReference1;
using Nop.Services.Logging;
using System.ServiceModel;
using System.Text;

namespace Nop.Web.Controllers
{
    public partial class HomeController : BasePublicController
    {
        private readonly IStoreService _storeService;
        private readonly IWorkContext _workContext;
        private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
        private readonly IStoreContext _webStoreContext;
        private readonly IEmailSender _emailSender;
        private readonly IRewardPointService _rewardPointService;
        private readonly ICustomerAttributeParser _customerAttributeParser;
        private readonly ICustomerAttributeService _customerAttributeService;
        private readonly ILocalizationService _localizationService;
        private readonly IWebHelper _webHelper;
        private readonly ILogger _logger;

        public HomeController(IStoreService storeService, IRewardPointService rewardPointService, IEmailSender emailSender, IWorkContext workContext, INewsLetterSubscriptionService newsLetterSubscriptionService, IStoreContext webStoreContext,
            ICustomerAttributeParser customerAttributeParser, IWebHelper webHelper, ILocalizationService localizationService, ICustomerAttributeService customerAttributeService, ILogger logger)
        {
            this._workContext = workContext;
            this._newsLetterSubscriptionService = newsLetterSubscriptionService;
            this._webStoreContext = webStoreContext;
            this._storeService = storeService;
            this._emailSender = emailSender;
            this._rewardPointService = rewardPointService;
            this._customerAttributeParser = customerAttributeParser;
            this._customerAttributeService = customerAttributeService;
            this._localizationService = localizationService;
            this._webHelper = webHelper;
            this._logger = logger;

            //HttpClient cli = new HttpClient();

            //    cli.DefaultRequestHeaders.Add("Connection", "keep-alive");

        }

        [NonAction]
        protected virtual void PrepareCustomerRegisterModel(RegisterModel model, Store currentStore)
        {

            if (model == null)
                throw new ArgumentNullException("model");

            //form fields
            model.GenderEnabled = currentStore.GenderEnabled;
            model.DateOfBirthEnabled = currentStore.DateOfBirthEnabled;
            model.DateOfBirthRequired = currentStore.DateOfBirthRequired;

            model.Date2Enabled = currentStore.date2Enabled;
            model.Date2Required = currentStore.date2Required;
            model.Date2Name = currentStore.date2Name;
            model.Date3Enabled = currentStore.date3Enabled;
            model.Date3Required = currentStore.date3Required;
            model.Date3Name = currentStore.date3Name;
            model.check1 = currentStore.check1;
            model.check1String = currentStore.check1String;
            model.check2 = currentStore.check2;
            model.check2String = currentStore.check2String;
            model.check3 = currentStore.check3;
            model.check3String = currentStore.check3String;
            model.check4 = currentStore.check4;
            model.check4String = currentStore.check4String;
            model.check5 = currentStore.check5;
            model.check5String = currentStore.check5String;
            model.multiField = currentStore.multiField;
            model.multiFieldName = currentStore.multiFieldName;
            model.AllowTZ = currentStore.AllowTZ;





            var customAttributes = PrepareCustomCustomerAttributes(currentStore.Id, false);
            customAttributes.ForEach(model.CustomerAttributes.Add);

        }






        //[NopHttpsRequirement(SslRequirement.No)]

        //header links
        //[ChildActionOnly]
        //public ActionResult HeaderLinks()
        //{
        //    var customer = _workContext.CurrentCustomer;

        //    //var unreadMessageCount = GetUnreadPrivateMessages();
        //    var unreadMessage = string.Empty;
        //    var alertMessage = string.Empty;
        //    //if (unreadMessageCount > 0)
        //    //{
        //    //    unreadMessage = string.Format(_localizationService.GetResource("PrivateMessages.TotalUnread"), unreadMessageCount);

        //    //    //notifications here
        //    //    if (_forumSettings.ShowAlertForPM &&
        //    //        !customer.GetAttribute<bool>(SystemCustomerAttributeNames.NotifiedAboutNewPrivateMessages, _storeContext.CurrentStore.Id))
        //    //    {
        //    //        _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.NotifiedAboutNewPrivateMessages, true, _storeContext.CurrentStore.Id);
        //    //        alertMessage = string.Format(_localizationService.GetResource("PrivateMessages.YouHaveUnreadPM"), unreadMessageCount);
        //    //    }
        //    //}

        //    var model = new HeaderLinksModel
        //    {
        //        IsAuthenticated = customer
        //        CustomerEmailUsername = customer.Username,
        //        //ShoppingCartEnabled = _permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart),
        //        //WishlistEnabled = _permissionService.Authorize(StandardPermissionProvider.EnableWishlist),
        //        //AllowPrivateMessages = customer.IsRegistered() && _forumSettings.AllowPrivateMessages,
        //        //UnreadPrivateMessages = unreadMessage,
        //        //AlertMessage = alertMessage,
        //    };
        //    //performance optimization (use "HasShoppingCartItems" property)
        //    //if (customer.HasShoppingCartItems)
        //    //{
        //    //    model.ShoppingCartItems = customer.ShoppingCartItems
        //    //        .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
        //    //        .LimitPerStore(_storeContext.CurrentStore.Id)
        //    //        .ToList()
        //    //        .GetTotalProducts();
        //    //    model.WishlistItems = customer.ShoppingCartItems
        //    //        .Where(sci => sci.ShoppingCartType == ShoppingCartType.Wishlist)
        //    //        .LimitPerStore(_storeContext.CurrentStore.Id)
        //    //        .ToList()
        //    //        .GetTotalProducts();
        //    //}

        //    return PartialView(model);
        //}
        [NonAction]
        protected virtual bool PrepareLightSubModel(string phone, lightSubModel model, int storeID)
        {
            var sub = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(phone, storeID);
            if (model == null)
                return false;
            var currentStore = _webStoreContext.CurrentStoreCustomer(storeID);
            model.FirstName = sub.FirstName;
            model.LastName = sub.LastName;
            model.points = sub.points;
            model.Email = sub.Email;
            model.gift1 = sub.gift1;
            model.gift1 = sub.gift2;
            model.gift1 = sub.gift3;
            model.gift1 = sub.gift4;
            model.gift1Name = currentStore.gift1Name;
            model.gift2Name = currentStore.gift2Name;
            model.gift3Name = currentStore.gift3Name;
            model.gift4Name = currentStore.gift4Name;
            model.storeID = storeID;
            return true;

        }

        [NonAction]
        // protected virtual IList<CustomerAttributeModel> PrepareCustomCustomerAttributes(Customer customer,
        //   string overrideAttributesXml = "")
        protected virtual IList<CustomerAttributeModel> PrepareCustomCustomerAttributes(int storeID, bool plus)
        {


            var result = new List<CustomerAttributeModel>();

            var customerAttributes = _customerAttributeService.GetAllCustomerAttributesByStoreID(storeID);

            foreach (var attribute in customerAttributes)
            {
                var attributeModel = new CustomerAttributeModel();
                if (plus)
                {

                    if (attribute.DisplayOrder > 100)
                    {


                        attributeModel.DisplayOrder = attribute.DisplayOrder;
                        attributeModel.Id = attribute.Id;
                        attributeModel.Name = attribute.GetLocalized(x => x.Name);
                        attributeModel.IsRequired = attribute.IsRequired;
                        attributeModel.AttributeControlType = attribute.AttributeControlType;


                        if (attribute.ShouldHaveValues())
                        {
                            //values
                            var attributeValues = _customerAttributeService.GetCustomerAttributeValues(attribute.Id);
                            foreach (var attributeValue in attributeValues)
                            {
                                var valueModel = new CustomerAttributeValueModel
                                {
                                    Id = attributeValue.Id,
                                    Name = attributeValue.GetLocalized(x => x.Name),
                                    IsPreSelected = attributeValue.IsPreSelected
                                };
                                attributeModel.Values.Add(valueModel);
                            }
                        }


                        result.Add(attributeModel);
                    }
                }
                else
                {
                    if (attribute.DisplayOrder < 100)
                    {


                        attributeModel.DisplayOrder = attribute.DisplayOrder;
                        attributeModel.Id = attribute.Id;
                        attributeModel.Name = attribute.GetLocalized(x => x.Name);
                        attributeModel.IsRequired = attribute.IsRequired;
                        attributeModel.AttributeControlType = attribute.AttributeControlType;


                        if (attribute.ShouldHaveValues())
                        {
                            //values
                            var attributeValues = _customerAttributeService.GetCustomerAttributeValues(attribute.Id);
                            foreach (var attributeValue in attributeValues)
                            {
                                var valueModel = new CustomerAttributeValueModel
                                {
                                    Id = attributeValue.Id,
                                    Name = attributeValue.GetLocalized(x => x.Name),
                                    IsPreSelected = attributeValue.IsPreSelected
                                };
                                attributeModel.Values.Add(valueModel);
                            }
                        }


                        result.Add(attributeModel);
                    }
                }
            }


            return result;
        }

        //footer

        #region Utilities

        [NonAction]
        protected virtual bool PrepareSubModel(string phone, subModel model, int storeId)
        {
            var sub = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(phone, storeId);
            if (model == null)
                return false;
            else
            {
                model.Active = sub.Active;
                model.City = sub.City;
                model.company = sub.company;
                // model.CreatedOnUtc = sub.CreatedOnUtc;
                model.Date2 = sub.Date2;
                model.Date3 = sub.Date3;
                model.DateOfBirth = sub.DateOfBirth;
                model.Email = sub.Email;
                model.FirstName = sub.FirstName;
                model.LastName = sub.LastName;
                model.lastVisit = sub.lastVisit;
                model.StreetAddress = sub.StreetAddress;
                model.StreetAddress2 = sub.StreetAddress2;
                model.points = sub.points;
                model.phone = sub.phone;
                model.gift1 = sub.gift1;
                model.gift2 = sub.gift2;
                return true;

            }

        }

        [NonAction]
        protected virtual void PrepareStoreModel(StoreModel model, int id)
        {
            var current = _webStoreContext.CurrentStoreCustomer(id);
            model.NumberOfCustomers = current.NumberOfCustomers;
            model.weeklyCustomerCountInt = current.weeklyCustomerCountInt;
            model.id = id;
            model.isPoints = current.isPoints;
            model.rgb = current.rgb;
        }



        #endregion
        [Route("{id}/Home")]
        public ActionResult Index(int id)


        {
            if (!(_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator()))
            {
                if (_workContext.CurrentCustomer.StoreId != id)
                    return RedirectToRoute("login");
            }
            var model = new StoreModel();
            PrepareStoreModel(model, id);

            ViewBag.AdminComment = _workContext.CurrentCustomer.AdminComment;


            return View(model);
        }

        [Route("")]
        public ActionResult Index2()


        {
            if (_workContext.CurrentCustomer.IsRegistered())
            {
                return Index(_workContext.CurrentCustomer.StoreId);
            }


            if (_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator())
            {
                return Index(1);
            }





            return RedirectToRoute("login");
        }


        [Route("{id}/findSubscription")]
        public ActionResult findSubscription(int id)
        {
            int storeID = id;
            if (!(_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator()))
            {
                if (_workContext.CurrentCustomer.StoreId != storeID)
                    return RedirectToRoute("login");
            }
            var currentStore = _webStoreContext.CurrentStoreCustomer(storeID);
            var model = new findSubModel();
            model.rgb = currentStore.rgb;


            return View(model);



        }
        [Route("{id}/findSubscription")]
        [HttpPost]
        public ActionResult findSubscription(findSubModel findSubmodel, int id)
        {
            int storeID = id;
            if (!(_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator()))
            {
                if (_workContext.CurrentCustomer.StoreId != storeID)
                    return RedirectToRoute("login");
            }
            var sub = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(findSubmodel.phone, storeID);
            if (sub == null)
                return PartialView("_none");
            if (sub.Active == false)
                return RedirectToRoute("_notActive");
            var currentStore = _webStoreContext.CurrentStoreCustomer(storeID);
            var model = new lightSubModel();
            model.FirstName = sub.FirstName;
            model.LastName = sub.LastName;
            model.points = sub.points;
            model.Email = sub.Email;
            model.gift1 = sub.gift1;
            model.gift2 = sub.gift2;
            model.gift3 = sub.gift3;
            model.gift4 = sub.gift4;
            model.gift1Name = currentStore.gift1Name;
            model.gift2Name = currentStore.gift2Name;
            model.gift3Name = currentStore.gift3Name;
            model.gift4Name = currentStore.gift4Name;
            model.storeID = storeID;
            model.isPoints = currentStore.isPoints;
            model.rgb = currentStore.rgb;
            if (sub.DateOfBirthWithYear.HasValue)
            {
                model.dateOfBirth = sub.DateOfBirthWithYear.Value.Date.ToString();
            }
            else
            {
                if (sub.DateOfBirth.HasValue)
                { model.dateOfBirth = sub.DateOfBirth.Value.Date.ToString(); }
            }
            var customAttributes = PrepareCustomCustomerAttributes(currentStore.Id, true);
            customAttributes.ForEach(model.CustomerAttributes.Add);


            return View("Subscription", model);




        }


        [HttpPost]
        [PublicAntiForgery]
        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult useGift(int giftNum, int storeID, string Email, string inovice, string employee, decimal sum = 0)
        {
            if (inovice != "00000" && inovice != "11111")
            {
                var rew = _rewardPointService.GetRewardPointsHistoryByParamsAndStoreId(storeID, null, inovice, null, null, null);
                if (rew.Count > 0)
                {
                    return Json(new
                    {
                        redirect = "wrong inovice"
                    });

                }
            }
            string giftMsg = "";
            string giftName = "";
            var CurrentStore = _webStoreContext.CurrentStoreCustomer(storeID);
            var sub2 = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(Email, storeID);
            if (giftNum == 1)
            {
                sub2.gift1 = false;
                CurrentStore.gift1Imps += 1;
                CurrentStore.gift1NotImp -= 1;
                giftName = CurrentStore.gift1Name;
                try
                {
                    giftMsg = CurrentStore.gift1Msg.Replace("$N$", sub2.FirstName);
                }
                catch
                {

                }


            }
            if (giftNum == 2)
            {
                sub2.gift2 = false;
                CurrentStore.gift2Imps += 1;
                CurrentStore.gift2NotImp -= 1;
                giftName = CurrentStore.gift2Name;
                try
                {
                    giftMsg = CurrentStore.gift2Msg.Replace("$N$", sub2.FirstName);
                }
                catch
                {

                }
            }
            if (giftNum == 3)
            {
                sub2.gift3 = false;
                CurrentStore.gift3Imps += 1;
                CurrentStore.gift3NotImp -= 1;
                giftName = CurrentStore.gift3Name;
                try
                {
                    giftMsg = CurrentStore.gift3Msg.Replace("$N$", sub2.FirstName);
                }
                catch
                {

                }
            }
            if (giftNum == 4)
            {
                CurrentStore.gift4NotImp -= 1;
                sub2.gift4 = false;
                CurrentStore.gift4Imps += 1;
                giftName = CurrentStore.gift4Name;
                try
                {
                    giftMsg = CurrentStore.gift4Msg.Replace("$N$", sub2.FirstName);
                }
                catch
                {

                }

            }

            switch (sub2.origin)
            {
                case 2:
                    CurrentStore.face2Ez += 1;
                    break;
                case 3:
                    CurrentStore.Instagram2Ez += 1;
                    break;
                case 4:
                    CurrentStore.Website2Ez += 1;
                    break;
                case 5:
                    CurrentStore.Website2Ez += 1;
                    break;
                case 6:
                    CurrentStore.internet2Ez += 1;
                    break;
                case 7:
                    CurrentStore.Legacy2Ez += 1;
                    break;
                case 8:
                    CurrentStore.Other2Ez += 1;
                    break;

            }
            sub2.origin = 1;
            sub2.visits += 1;
            sub2.lastVisit = DateTime.Now;
            CurrentStore.weeklyVisits += 1;
            CurrentStore.monthlyVisits += 1;
            CurrentStore.visits += 1;
            _rewardPointService.AddRewardPointsHistoryEntry2(sub2, 0, 0, storeID, (int)sum, employee, inovice, giftName);

            try
            {
                _storeService.UpdateStore(CurrentStore);
                _newsLetterSubscriptionService.UpdateNewsLetterSubscription(sub2);

                _emailSender.SendSmS(sub2.Email, giftMsg, CurrentStore.SmsUserName, CurrentStore.SmsPass, "", CurrentStore.preMsg, CurrentStore.smsRemovalLink);


            }
            catch
            {
                return PartialView("none");
            }


            return Json(new
            {
                redirect = "/" + storeID + "/register"
            });



        }

        //[NopHttpsRequirement(SslRequirement.Yes)]
        ////available even when navigation is not allowed

        //[Route("{id}/inc")]
        //public ActionResult inc(int id)
        //{
        //    if (!(_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator()))
        //    {
        //        if (_workContext.CurrentCustomer.StoreId != id)
        //            return RedirectToRoute("login");
        //    }
        //    var model = new subPointsModel();

        //    return View("inc",model);

        //}
        //[Route("{storeId}/inc")]
        //[HttpPost]
        //public ActionResult inc(string phone, int storeId, int sum)
        //{
        //    var CurrentStore = _webStoreContext.CurrentStoreCustomer(storeId);


        //    int visitPoints;
        //    string pointsMsgSms;
        //    var sub = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(phone, storeId);
        //    if (sub == null || (sub.Active == false))
        //    {
        //        return PartialView("none");
        //    }
        //    if (sum > 0)
        //    {

        //        sub.visits += 1;
        //        CurrentStore.weeklyVisits += 1;
        //        CurrentStore.monthlyVisits += 1;
        //        CurrentStore.visits += 1;




        //        try
        //        {
        //            if (_webStoreContext.CurrentStore.sum2PointsCoefficient > 0)
        //            {
        //                visitPoints = (int)((float)sum / (_webStoreContext.CurrentStore.sum2PointsCoefficient));
        //                sub.points += visitPoints;
        //                CurrentStore.totalCustomersPoints += visitPoints;
        //                pointsMsgSms = CurrentStore.preMsg + " " + sub.FirstName + ",\n" + CurrentStore.pointsMsg + " " + visitPoints.ToString() + "נקודות" + " " + CurrentStore.pointsMsg2;

        //                _storeService.UpdateStore(CurrentStore);
        //                _newsLetterSubscriptionService.UpdateNewsLetterSubscription(sub);
        //                _emailSender.SendSmS(sub.Email, pointsMsgSms, CurrentStore.SmsUserName, CurrentStore.SmsPass, "0", CurrentStore.Name);
        //            }
        //            return PartialView("ok");

        //        }
        //        catch
        //        {
        //            return PartialView("error");
        //        }

        //    }
        //    else
        //    {
        //        return PartialView("error");
        //    }


        //}


        [HttpPost]
        [PublicAntiForgery]
        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult visit(int storeID, string Email)
        {

            var CurrentStore = _webStoreContext.CurrentStoreCustomer(storeID);
            var sub = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(Email, storeID);
            sub.visits += 1;
            sub.lastVisit = DateTime.Now;
            CurrentStore.visits += 1;
            CurrentStore.weeklyVisits += 1;
            CurrentStore.monthlyVisits += 1;
            try
            {
                _newsLetterSubscriptionService.UpdateNewsLetterSubscription(sub);
            }
            catch
            {
                return Json(new
                {
                    redirect = "0"
                });
            }
            try
            {
                _storeService.UpdateStore(CurrentStore);
            }
            catch
            {
                return Json(new
                {
                    redirect = "0"
                });

            }


            return Json(new
            {
                redirect = "1"
            });
        }
        [HttpPost]
        [PublicAntiForgery]
        [NopHttpsRequirement(SslRequirement.Yes)]
        public ActionResult points(int dicPoints, decimal sum, int storeID, string Email, string inovice, string employee)
        {
            if (inovice != "00000" && inovice != "11111")
            {
                var rew = _rewardPointService.GetRewardPointsHistoryByParamsAndStoreId(storeID, null, inovice, null, null, null);
                if (rew.Count > 0)
                {
                    return Json(new
                    {
                        redirect = "wrong inovice"
                    });

                }
            }
            string pointsMsgSms = String.Empty;
            int visitPoints;
            int incPoints;
            int incPointsTemp;
            var CurrentStore = _webStoreContext.CurrentStoreCustomer(storeID);
            var sub = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(Email, storeID);
            if (sub == null || (sub.Active == false))
            {
                return PartialView("_none");
            }
            if (dicPoints >= 0 && sub.points >= dicPoints && sum >= dicPoints)
            {
                incPoints = (int)(((int)sum - dicPoints) / CurrentStore.sum2PointsCoefficient);
                //incPoints = incPointsTemp - dicPoints;
                visitPoints = incPoints - dicPoints;
                sub.points += visitPoints;
                sub.visits += 1;
                sub.lastVisit = DateTime.Now;
                CurrentStore.weeklyVisits += 1;
                CurrentStore.monthlyVisits += 1;
                CurrentStore.visits += 1;
                CurrentStore.pointImpsVisits += 1;
                CurrentStore.pointImpsSum += dicPoints;
                CurrentStore.totalCustomersPoints += visitPoints;
                switch (sub.origin)
                {
                    case 2:
                        CurrentStore.face2Ez += 1;
                        break;
                    case 3:
                        CurrentStore.Instagram2Ez += 1;
                        break;
                    case 4:
                        CurrentStore.Website2Ez += 1;
                        break;
                    case 5:
                        CurrentStore.Website2Ez += 1;
                        break;
                    case 6:
                        CurrentStore.internet2Ez += 1;
                        break;
                    case 7:
                        CurrentStore.Legacy2Ez += 1;
                        break;
                    case 8:
                        CurrentStore.Other2Ez += 1;
                        break;

                }
                sub.origin = 1;
                // pointsMsgSms = CurrentStore.preMsg + " " + sub.FirstName + ",\n" + CurrentStore.pointsMsg + " " + incPoints.ToString() + " נקודות" + " " + CurrentStore.pointsMsg2;
                if (!CurrentStore.useDefaultMsg)
                {
                    if (!String.IsNullOrWhiteSpace(CurrentStore.pointsMsg))
                    {
                        if (dicPoints <= 0)
                        {

                            pointsMsgSms = CurrentStore.pointsMsg.Replace("$N$", sub.FirstName);
                            pointsMsgSms = pointsMsgSms.Replace("$U$", incPoints.ToString());
                            pointsMsgSms = pointsMsgSms.Replace("$D$", dicPoints.ToString());
                            pointsMsgSms = pointsMsgSms.Replace("$P$", sub.points.ToString());
                            pointsMsgSms = pointsMsgSms + "\n" + CurrentStore.smsRemovalLink;


                        }
                        else
                        {
                            if (!String.IsNullOrWhiteSpace(CurrentStore.pointsMsg2))
                            {
                                pointsMsgSms = CurrentStore.pointsMsg2.Replace("$N$", sub.FirstName);
                                pointsMsgSms = pointsMsgSms.Replace("$U$", incPoints.ToString());
                                pointsMsgSms = pointsMsgSms.Replace("$D$", dicPoints.ToString());
                                pointsMsgSms = pointsMsgSms.Replace("$P$", sub.points.ToString());
                                pointsMsgSms = pointsMsgSms + "\n" + CurrentStore.smsRemovalLink;
                            }

                        }
                    }
                    else
                    {
                        var storeDefault = _storeService.GetStoreById(2);


                        if (dicPoints <= 0)
                        {
                            try
                            {
                                pointsMsgSms = storeDefault.pointsMsg.Replace("$N$", sub.FirstName);
                                pointsMsgSms = pointsMsgSms.Replace("$U$", incPoints.ToString());
                                pointsMsgSms = pointsMsgSms.Replace("$D$", dicPoints.ToString());
                                pointsMsgSms = pointsMsgSms.Replace("$P$", sub.points.ToString());
                                pointsMsgSms = pointsMsgSms + "\n" + CurrentStore.smsRemovalLink;
                            }
                            catch
                            {

                            }
                        }
                        else
                        {
                            try
                            {
                                pointsMsgSms = storeDefault.pointsMsg2.Replace("$N$", sub.FirstName);
                                pointsMsgSms = pointsMsgSms.Replace("$U$", incPoints.ToString());
                                pointsMsgSms = pointsMsgSms.Replace("$D$", dicPoints.ToString());
                                pointsMsgSms = pointsMsgSms.Replace("$P$", sub.points.ToString());
                                pointsMsgSms = pointsMsgSms + "\n" + CurrentStore.smsRemovalLink;
                            }
                            catch
                            {

                            }

                        }

                    }
                }







                try
                {
                    _storeService.UpdateStore(CurrentStore);
                    _newsLetterSubscriptionService.UpdateNewsLetterSubscription(sub);
                    if (!String.IsNullOrWhiteSpace(CurrentStore.pointsMsg))
                    {
                        if (inovice == "00000")  // for store owner points adjustment
                        {
                            try {
                                var contacts = CurrentStore.contactPhone.Split(';');
                                foreach (string con in contacts)
                                {

                                    string conMsg = _localizationService.GetResource("Admin.Contacts.OwnerInovice");
                                    conMsg = conMsg.Replace("$FIRST$", sub.FirstName);
                                    conMsg = conMsg.Replace("$LAST$", sub.FirstName);
                                    conMsg = conMsg.Replace("$EMP$", employee);
                                    conMsg = conMsg.Replace("$PHONE$", sub.Email);
                                    _emailSender.SendSmS(con, conMsg, CurrentStore.SmsUserName, CurrentStore.SmsPass, "", "EZclub");
                                }
                            }
                            catch
                            {

                            }
                        }

                        else
                        {
                            _emailSender.SendSmS(sub.Email, pointsMsgSms, CurrentStore.SmsUserName, CurrentStore.SmsPass, "", CurrentStore.preMsg);
                        }

                    }
                    _rewardPointService.AddRewardPointsHistoryEntry2(sub, incPoints, dicPoints, storeID, (int)sum, employee, inovice);
                    string re = "/" + storeID.ToString() + "/home";
                    payModel payModel1;

                    if (sum - dicPoints > 0)
                    {
                        return Json(new
                        {
                            redirect = (sum - dicPoints).ToString()
                        });
                        payModel1 = new payModel() { pay = sum - dicPoints };

                    }
                    else
                    {
                        return Json(new
                        {
                            redirect = 0.ToString()
                        });
                        payModel1 = new payModel() { pay = Decimal.Zero };
                    }
                    return PartialView("_sumToPay", payModel1);
                }



                catch (Exception e)
                {
                    return PartialView("_error");
                }


            }
            else
            {
                return PartialView("_error");

            }


        }
        //[HttpPost]
        //public ActionResult resetPoints(string phone, int dec)
        //{
        //    var sub = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(phone,);
        //    if (sub == null || (sub.Active == false))
        //    {
        //        return PartialView("none");
        //    }
        //  else {
        //        _webStoreContext.CurrentStore.visits += 1;
        //        _webStoreContext.CurrentStore.weeklyVisits += 1;
        //        _webStoreContext.CurrentStore.monthlyVisits += 1;
        //        _webStoreContext.CurrentStore.pointImpsVisits += 1;
        //        _webStoreContext.CurrentStore.pointImpsSum += sub.points;
        //        sub.points = 0;
        //        return PartialView("pointsDownSuccess");
        //    }



        //}

        [HttpPost]
        public ActionResult nop(int nop)
        {

            return Json(new
            {
                redirect = "nop"
            });



        }
        [Route("{id}/register")]
        public ActionResult Register()
        {



            var rawUrl = this.HttpContext.Request.RawUrl;
            string[] r2 = rawUrl.Split('/');


            string r1 = r2[1];
            int r3 = Int32.Parse(r1);
            Store currentStore = _webStoreContext.CurrentStoreCustomer(r3);
            if (!(_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator()))
            {
                if (_workContext.CurrentCustomer.StoreId != currentStore.Id)
                    return RedirectToRoute("login");
            }
            //check whether registration is allowed


            var model = new RegisterModel();
            PrepareCustomerRegisterModel(model, currentStore);
            //enable newsletter by default
            model.Newsletter = true;
            model.rgb = currentStore.rgb;
            model.DateOfBirthRequired = currentStore.DateOfBirthRequired;
            model.AllowTZ = currentStore.AllowTZ;
            model.AllowRealEmail = currentStore.AllowRealEmail;
            model.check1 = currentStore.check1;


            return View(model);
        }

        [HttpPost]
        //[CaptchaValidator]
        //[HoneypotValidator]
        [PublicAntiForgery]
        [ValidateInput(false)]

        [PublicStoreAllowNavigation(false)]
        [Route("{id}/register")]
        public ActionResult Register(RegisterModel model, string returnUrl, FormCollection form)
        {



            var rawUrl = this.HttpContext.Request.RawUrl;
            string[] r2 = rawUrl.Split('/');


            string r1 = r2[1];
            int r3 = Int32.Parse(r1);
            Store currentStore = _webStoreContext.CurrentStoreCustomer(r3);
            if (!(_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator()))
            {
                if (_workContext.CurrentCustomer.StoreId != currentStore.Id)
                    return RedirectToRoute("login");
            }



            //if (!CommonHelper.IsValidPhone(model.Email))
            //{
            //    var result =   _localizationService.GetResource("Newsletter.phone.Wrong");
            //}
            //check whether registration is allowed

            //if (_newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(model.Email, currentStore.Id) != null)
            //{

            //    //  return RedirectToRoute("RegisterResult", new { resultId = (int)UserRegistrationType.numberExists });
            //    var redirectUrl2 = "/" + currentStore.Id.ToString() + "/" + "RegisterResult/" + ((int)UserRegistrationType.numberExists).ToString();
            //    return Redirect(redirectUrl2);
            //}
            var customer = _workContext.CurrentCustomer;



            IList<CustomerAttribute> caForForm = _customerAttributeService.GetAllCustomerAttributesByStoreID(r3);
            string[] drops = new string[2];
            string[] strings = new string[2];
            string[] checks = new string[5];
            //DateTime?[] dates = new DateTime?[2];
            int cdrops = 0;
            int cstrings = 0;
            //int cdates = 0;
            for (int i = 0; i < caForForm.Count; i++)
            {
                if (caForForm[i].DisplayOrder > 100) continue;
                string ccc;

                string s1 = string.Format("customer_attribute_{0}", caForForm[i].Id);

                ValueProviderResult x;
                string y;
                if (caForForm[i].AttributeControlTypeId == 20) break;
                if (caForForm[i].AttributeControlTypeId == 1)
                {
                    x = form.GetValue(s1);
                    y = x.AttemptedValue;
                    if (y == "0") ccc = null;
                    else
                    {
                        //ccc = _customerAttributeService.GetCustomerAttributeValueById(Int32.Parse(form.GetValue(s1).AttemptedValue.ToString())).Name;
                        ccc = y;
                    }
                }
                else { ccc = form.GetValue(s1).AttemptedValue.ToString(); }



                if (_customerAttributeService.GetCustomerAttributeById(caForForm[i].Id).AttributeControlTypeId == 1) { drops[cdrops] = ccc; cdrops++; }
                if (_customerAttributeService.GetCustomerAttributeById(caForForm[i].Id).AttributeControlTypeId == 4) { strings[cstrings] = ccc; cstrings++; }

                if (i >= 6) break;
            }
            //for (int j = 0; j < 6; j++) {
            //   if( model.CustomerAttributes[j].AttributeControlType == AttributeControlType.Datepicker)
            //    {
            //        dates[cdates] = model.CustomerAttributes[j].ParseDate();
            //        cdates++;
            //        if (cdates > 2) break;
            //    }

            //}
            cdrops = 0;
            cstrings = 0;
            //cdates = 0;





            //validate CAPTCHA



            NewsLetterSubscription nls = new NewsLetterSubscription
            {
                StoreId = currentStore.Id,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                CreatedOnUtc = DateTime.Now,
                LastIpAddress = _webHelper.GetCurrentIpAddress(),
                Active = true,
                points = currentStore.pointsOnReg,
                RealEmail = model.RealEmail

            };


            //properties


            //form fields
            //if (currentStore.GenderEnabled)
            nls.Gender = model.Gender;

            if (currentStore.DateOfBirthEnabled)
            {
                nls.DateOfBirth = model.ParseDateOfBirth();
                nls.DateOfBirthWithYear = model.ParseDateOfBirthWithYear();

            }

            if (currentStore.date2Enabled)
            {
                nls.Date2 = model.ParseDate2();

            }

            if (currentStore.date3Enabled)
            {
                nls.Date3 = model.ParseDate3();

            }

            if (model.check1Val)
            {
                nls.checkbox1 = currentStore.check1String;
            }
            if (model.check2Val)
            {
                nls.checkbox2 = currentStore.check2String;
            }
            if (model.check3Val)
            {
                nls.checkbox3 = currentStore.check3String;
            }
            if (model.check4Val)
            {
                nls.checkbox4 = currentStore.check4String;
            }
            if (model.check5Val)
            {
                nls.checkbox5 = currentStore.check5String;
            }
            if (!model.check1Val && !model.check2Val && !model.check3Val && !model.check4Val && !model.check5Val)
            {
                nls.checkbox1 = currentStore.check1String;
                nls.checkbox2 = currentStore.check2String;
                nls.checkbox3 = currentStore.check3String;
                nls.checkbox4 = currentStore.check4String;
                nls.checkbox5 = currentStore.check5String;
            }
            //if (_customerSettings.CityEnabled)
            nls.City = strings[0];
            // else { nls.City = "NaV"; }
            //if (_customerSettings.CompanyEnabled)
            nls.company = strings[1];
            // else { nls.company = "NaV"; }
            //if (_customerSettings.StreetAddressEnabled)
            nls.StreetAddress = drops[0];
            //  else { nls.StreetAddress = "NaV"; }
            //if (_customerSettings.StreetAddress2Enabled)
            nls.StreetAddress2 = drops[1];
            // else { nls.StreetAddress2 = "NaV"; }

            //nls.Date3 = dates[1].HasValue ?
            //(DateTime?)_dateTimeHelper.ConvertToUtcTime(dates[0].Value) : null;
            //if (dates[0].Year > 2016)
            // nls.Date2 = dates[0];
            //if (dates[0].Year > 2016)
            //    nls.Date3 = dates[1];


            /////////29.10.17 - start//////////////////////////////////


            //   nls.gift1 = currentStore.gift1OnReg;
            //if (currentStore.gift1OnReg)
            //{
            //    nls.gift1EndDate = DateTime.Now.AddDays(currentStore.gift1ValidityDays);
            //}
            nls.points = currentStore.pointsOnReg;
            nls.gift2 = currentStore.gift2OnReg;

            if (currentStore.gift2OnReg)
            {
                nls.gift2EndDate = DateTime.Now.AddDays(currentStore.gift2ValidityDays);
                currentStore.gift2NotImp += 1;
            }

            /////////29.10.17 - end//////////////////////////////////
            nls.lastVisit = DateTime.UtcNow;
            nls.origin = 1;
            nls.Active = true;
            _newsLetterSubscriptionService.InsertNewsLetterSubscription(nls);
            currentStore.NumberOfCustomers += 1;
            currentStore.weeklyCustomerCountInt += 1;
            currentStore.monthlyCustomerCount += 1;
            _storeService.UpdateStore(currentStore);
            string welcomeMsg = null;

            /////////////////////   otech
            GetLastClubMemberQuery glcm = new GetLastClubMemberQuery();
            SaveClubMemberQuery scmq = new SaveClubMemberQuery();
            WSHttpBinding b = new WSHttpBinding();
            b.Security.Mode = SecurityMode.Transport;
            b.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
            b.MaxReceivedMessageSize = 2147483647;
            b.ReaderQuotas.MaxStringContentLength = 2147483647;
            b.ReaderQuotas.MaxArrayLength = 2147483647;
            System.ServiceModel.EndpointAddress endPoint =
                          new System.ServiceModel.EndpointAddress("https://api.otech.co.il:21443/service/otech.svc");
            OtechServiceClient client = new OtechServiceClient(b, endPoint);
            //OtechServiceClient client = new OtechServiceClient();


            //client.Endpoint.Address = "https://api.otech.co.il:21443/service/otech.svc";

          



            glcm.UserName = currentStore.Api_user; //"best";
            glcm.Password = currentStore.Api_pass; //"best2018";

            glcm.Date = new DateTime(2000, 1, 1);
            //glcm.Date = new DateTime();
            //glcm.Date = DateTime.Now;
            glcm.Filters = new ServiceFilter[2];
            //glcm.Filters = new ServiceFilter[1];
            glcm.Filters[0] = new ServiceFilter();
            glcm.Filters[1] = new ServiceFilter();
            if (currentStore.SearchByTZ)////////////////////  searches by "clb_num"
            {
                glcm.Filters[0].Field = "clb_num";
                glcm.Filters[0].Value = model.TZ;
                glcm.Filters[0].Type = FilterType.number;
            }
            else
            {
                glcm.Filters[0].Field = "clb_cell_pre";
                glcm.Filters[0].Value = model.Email.Substring(0, 3);// this is realy the mobile phone
                glcm.Filters[0].Type = FilterType.varchar;

                glcm.Filters[1].Field = "clb_cell";
                glcm.Filters[1].Value = model.Email.Substring(3, 7); // this is realy the mobile phone
                glcm.Filters[1].Type = FilterType.varchar;
            }
            //scmq.clubMember.
            scmq.UserName = currentStore.Api_user; //"best";
            scmq.Password = currentStore.Api_pass; //"best2018";
            scmq.IsNewClubMember = true;
            scmq.clubMember = new Nop.Web.ServiceReference1.lakclbST();
            scmq.clubMember.clb_f_name = model.FirstName;
            scmq.clubMember.clb_name = model.LastName;
            scmq.clubMember.clb_cell = model.Email.Substring(3, 7);
            scmq.clubMember.clb_cell_pre = model.Email.Substring(0, 3);
            //scmq.clubMember.clb_kvuza = "0";


            //scmq.clubMember.clb_from_mach = "37";
            scmq.clubMember.clb_from_date = DateTime.Today;
            scmq.clubMember.clb_email = nls.RealEmail;  ////////// this is Email

            try
            {
                scmq.clubMember.clb_num = Int32.Parse(model.TZ);  //Int32.Parse(currentStore.Api_Custom_1); //
            }
            catch
            {
                scmq.clubMember.clb_num = 0;
            }

            try
            {
                scmq.clubMember.clb_hnt_id = Int32.Parse(currentStore.Api_Custom_1);
            }
            catch
            {
                scmq.clubMember.clb_hnt_id = 0;
            }

            try
            {
                scmq.clubMember.clb_from_mach = currentStore.Api_Custom_2;
            }
            catch
            {
                scmq.clubMember.clb_from_mach = "0";

            }


            if (nls.DateOfBirthWithYear == null)
            {
                scmq.clubMember.clb_date_leda = nls.DateOfBirth;
            }
            else
            {
                scmq.clubMember.clb_date_leda = nls.DateOfBirthWithYear;
            }


            if (currentStore.AllowTZ)
            {

                scmq.clubMember.clb_tz = Int32.Parse(model.TZ);

            }

            try
            {

                scmq.Hatima = Convert.FromBase64String(model.signature.Split(',')[1]);
            }
            catch
            {

            }
            scmq.clubMember.clb_update = DateTime.Now;
            var s = client.GetLastClubMembers(glcm);

            if (s.ClubMembersk__BackingField.Length == 0)
            {
                var fuc = client.SaveClubMember(scmq);
                nls.phone = fuc.SucceededMessagek__BackingField;
                _newsLetterSubscriptionService.UpdateNewsLetterSubscription(nls);
            }
            if (!string.IsNullOrWhiteSpace(s.ErrorMessagek__BackingField))
            {
                _logger.Error(string.Format("Error posting data for phone number: {0} .error msg{1} ", scmq.clubMember.clb_cell, s.ErrorMessagek__BackingField));
                var redirectUrl2 = "/" + currentStore.Id.ToString() + "/" + "RegisterResult/" + ((int)UserRegistrationType.errorPosting).ToString();
                return Redirect(redirectUrl2);

            }


            /////////////////////////////
            //if (s.ClubMembersk__BackingField.Length != 0)
            //{
            //    var redirectUrl2 = "/" + currentStore.Id.ToString() + "/" + "RegisterResult/" + ((int)UserRegistrationType.numberExists).ToString();
            //    return Redirect(redirectUrl2);
            //}

            try
            {
                welcomeMsg = currentStore.welcomeMsg.Replace("$N$", nls.FirstName);
            }
            catch
            {

            }

            _emailSender.SendSmS(model.Email, welcomeMsg, currentStore.SmsUserName, currentStore.SmsPass, "", currentStore.preMsg, null);




            // }
            //}









            //notifications
            //if (_customerSettings.NotifyNewCustomerRegistration)
            //    _workflowMessageService.SendCustomerRegisteredNotificationMessage(customer, _localizationSettings.DefaultAdminLanguageId);

            //raise event       



            //send customer welcome message



            // var redirectUrl = Url.RouteUrl("RegisterResult", new { resultId = (int)UserRegistrationType.Standard });
            var redirectUrl = "/" + currentStore.Id.ToString() + "/" + "RegisterResult/" + ((int)UserRegistrationType.Standard).ToString();
            if (!String.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                redirectUrl = _webHelper.ModifyQueryString(redirectUrl, "returnurl=" + HttpUtility.UrlEncode(returnUrl), null);
            return Redirect(redirectUrl);
        }


        //errors







        [Route("{id}/RegisterResult/{resultId}")]
        [PublicStoreAllowNavigation(true)]
        public ActionResult RegisterResult(int id, int resultId)
        {
            var resultText = "";
            switch ((UserRegistrationType)resultId)
            {
                case UserRegistrationType.Disabled:
                    resultText = _localizationService.GetResource("Account.Register.Result.Disabled");
                    break;
                case UserRegistrationType.Standard:
                    resultText = _localizationService.GetResource("Account.Register.Result.Standard");
                    break;
                case UserRegistrationType.numberExists:
                    resultText = "מספר הטלפון כבר קיים במערכת";
                    break;
                case UserRegistrationType.EmailValidation:
                    resultText = _localizationService.GetResource("Account.Register.Result.EmailValidation");
                    break;
                case UserRegistrationType.ConfMsgSentToSubNumber:
                    // resultText = _localizationService.GetResource("Account.Register.Result.ConfMsgSentToSubNumber");
                    resultText = "הודעה נשלחה בהצלחה";
                    break;
                case UserRegistrationType.errorPosting:
                    resultText = "שגיאת מערכת";
                    break;
                default:
                    break;
            }

            var model = new RegisterResultModel
            {
                Result = resultText,
                storeID = _workContext.CurrentCustomer.StoreId
            };
            if (resultId == 99)
            {
                model.Result = "99"; // web registration
            }
            if (resultId == 199)
            {
                model.Result = "199"; // hacking
            }
            return View(model);
        }

        //available even when navigation is not allowed
        [PublicStoreAllowNavigation(true)]
        [HttpPost]
        public ActionResult RegisterResult(string returnUrl)
        {
            var storeID = _workContext.CurrentCustomer.StoreId;
            if (String.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
                return Redirect("/" + storeID + "/register");

            return Redirect(returnUrl);
        }


        [PublicStoreAllowNavigation(true)]
        [Route("{id}/{rand}/register/")]
        public ActionResult WebRegister()
        {
            var rawUrl = this.HttpContext.Request.RawUrl;
            string[] r2 = rawUrl.Split('/');


            string r1 = r2[1];
            int r3 = Int32.Parse(r1);

            string rand1 = r2[2];


            Store currentStore = _webStoreContext.CurrentStoreCustomer(r3);
            if (currentStore.CompanyPhoneNumber != rand1)
            {

                return Redirect("www.google.co.il");
            }

            //check whether registration is allowed


            var model = new RegisterModel();
            PrepareCustomerRegisterModel(model, currentStore);
            //enable newsletter by default
            model.Newsletter = true;
            model.rgb = currentStore.rgb;
            model.DateOfBirthRequired = currentStore.DateOfBirthRequired;
            model.AllowTZ = currentStore.AllowTZ;
            model.AllowRealEmail = currentStore.AllowRealEmail;
            model.check1 = currentStore.check1;
            return View(model);
        }







        [HttpPost]
        [PublicAntiForgery]
        [ValidateInput(false)]

        [PublicStoreAllowNavigation(true)]
        [Route("{id}/{rand}/register/")]
        public ActionResult WebRegister(RegisterModel model, string returnUrl, FormCollection form)
        {
            string redirectUrl = null;
            string contextIpAdress = _webHelper.GetCurrentIpAddress();
            var rawUrl = this.HttpContext.Request.RawUrl;
            string[] r2 = rawUrl.Split('/');


            string r1 = r2[1];
            int r3 = Int32.Parse(r1);

            string rand1 = r2[2];


            Store currentStore = _webStoreContext.CurrentStoreCustomer(r3);
            if (currentStore.CompanyPhoneNumber != rand1)
            {

                return Redirect("www.google.co.il");
            }

            //if (!CommonHelper.IsValidPhone(model.Email))
            //{
            //    var result =   _localizationService.GetResource("Newsletter.phone.Wrong");
            //}
            //check whether registration is allowed


            var tempNls = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByIpAdrressAndStoreId(contextIpAdress, currentStore.Id);
            if (tempNls != null)
            {
                int dateDIFF = DateTime.Now.DayOfYear - tempNls.CreatedOnUtc.Value.DayOfYear;

                if (dateDIFF <= 1)
                {
                    var redirectUrl2 = "/" + currentStore.Id.ToString() + "/" + "RegisterResult/" + (199).ToString(); // 
                    return Redirect(redirectUrl2);
                }
            }


            var customer = _workContext.CurrentCustomer;



            IList<CustomerAttribute> caForForm = _customerAttributeService.GetAllCustomerAttributesByStoreID(r3);
            string[] drops = new string[2];
            string[] strings = new string[2];
            string[] checks = new string[5];
            //DateTime?[] dates = new DateTime?[2];
            int cdrops = 0;
            int cstrings = 0;
            //int cdates = 0;
            for (int i = 0; i < caForForm.Count; i++)
            {
                if (caForForm[i].DisplayOrder > 100) continue;
                string ccc;

                string s1 = string.Format("customer_attribute_{0}", caForForm[i].Id);

                ValueProviderResult x;
                string y;
                if (caForForm[i].AttributeControlTypeId == 20) break;
                if (caForForm[i].AttributeControlTypeId == 1)
                {
                    x = form.GetValue(s1);
                    y = x.AttemptedValue;
                    if (y == "0") ccc = null;
                    else
                    {
                        //ccc = _customerAttributeService.GetCustomerAttributeValueById(Int32.Parse(form.GetValue(s1).AttemptedValue.ToString())).Name;
                        ccc = y;
                    }
                }
                else { ccc = form.GetValue(s1).AttemptedValue.ToString(); }



                if (_customerAttributeService.GetCustomerAttributeById(caForForm[i].Id).AttributeControlTypeId == 1) { drops[cdrops] = ccc; cdrops++; }
                if (_customerAttributeService.GetCustomerAttributeById(caForForm[i].Id).AttributeControlTypeId == 4) { strings[cstrings] = ccc; cstrings++; }

                if (i >= 6) break;
            }
            //for (int j = 0; j < 6; j++) {
            //   if( model.CustomerAttributes[j].AttributeControlType == AttributeControlType.Datepicker)
            //    {
            //        dates[cdates] = model.CustomerAttributes[j].ParseDate();
            //        cdates++;
            //        if (cdates > 2) break;
            //    }

            //}
            cdrops = 0;
            cstrings = 0;
            //cdates = 0;





            //validate CAPTCHA



            NewsLetterSubscription nls = new NewsLetterSubscription
            {
                StoreId = currentStore.Id,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                CreatedOnUtc = DateTime.Now,
                LastIpAddress = _webHelper.GetCurrentIpAddress(),
                Active = true,
                points = currentStore.pointsOnReg,
                gift1 = currentStore.gift2OnReg,
                RealEmail = model.RealEmail
            };


            //properties


            //form fields
            //if (currentStore.GenderEnabled)
            nls.Gender = model.Gender;

            if (currentStore.DateOfBirthEnabled)
            {
                nls.DateOfBirth = model.ParseDateOfBirth();
                nls.DateOfBirthWithYear = model.ParseDateOfBirthWithYear();

            }

            if (currentStore.date2Enabled)
            {
                nls.Date2 = model.ParseDate2();

            }

            if (currentStore.date3Enabled)
            {
                nls.Date3 = model.ParseDate3();

            }

            if (model.check1Val)
            {
                nls.checkbox1 = currentStore.check1String;
            }
            if (model.check2Val)
            {
                nls.checkbox2 = currentStore.check2String;
            }
            if (model.check3Val)
            {
                nls.checkbox3 = currentStore.check3String;
            }
            if (model.check4Val)
            {
                nls.checkbox4 = currentStore.check4String;
            }
            if (model.check5Val)
            {
                nls.checkbox5 = currentStore.check5String;
            }
            if (!model.check1Val && !model.check2Val && !model.check3Val && !model.check4Val && !model.check5Val)
            {
                nls.checkbox1 = currentStore.check1String;
                nls.checkbox2 = currentStore.check2String;
                nls.checkbox3 = currentStore.check3String;
                nls.checkbox4 = currentStore.check4String;
                nls.checkbox5 = currentStore.check5String;
            }
            //if (_customerSettings.CityEnabled)
            nls.City = strings[0];
            // else { nls.City = "NaV"; }
            //if (_customerSettings.CompanyEnabled)
            nls.company = strings[1];
            // else { nls.company = "NaV"; }
            //if (_customerSettings.StreetAddressEnabled)
            nls.StreetAddress = drops[0];
            //  else { nls.StreetAddress = "NaV"; }
            //if (_customerSettings.StreetAddress2Enabled)
            nls.StreetAddress2 = drops[1];
            // else { nls.StreetAddress2 = "NaV"; }

            //nls.Date3 = dates[1].HasValue ?
            //(DateTime?)_dateTimeHelper.ConvertToUtcTime(dates[0].Value) : null;
            //if (dates[0].Year > 2016)
            // nls.Date2 = dates[0];
            //if (dates[0].Year > 2016)
            //    nls.Date3 = dates[1];

            nls.points = currentStore.pointsOnReg;
            /////////29.10.17 - start//////////////////////////////////


            //   nls.gift1 = currentStore.gift1OnReg;
            //if (currentStore.gift1OnReg)
            //{
            //    nls.gift1EndDate = DateTime.Now.AddDays(currentStore.gift1ValidityDays);
            //}
            nls.gift2 = currentStore.gift2OnReg;

            if (currentStore.gift2OnReg)
            {
                nls.gift2EndDate = DateTime.Now.AddDays(currentStore.gift2ValidityDays);
                currentStore.gift2NotImp += 1;
            }

            /////////29.10.17 - end//////////////////////////////////
            nls.lastVisit = DateTime.UtcNow;
            nls.origin = 5;
            nls.Active = true;
            nls.LastIpAddress = contextIpAdress;
            _newsLetterSubscriptionService.InsertNewsLetterSubscription(nls);
            currentStore.NumberOfCustomers += 1;
            currentStore.weeklyCustomerCountInt += 1;
            currentStore.monthlyCustomerCount += 1;
            _storeService.UpdateStore(currentStore);
            string welcomeMsg = null;
            try
            {
                welcomeMsg = currentStore.welcomeMsg.Replace("$N$", nls.FirstName);
            }
            catch
            {

            }

            GetLastClubMemberQuery glcm = new GetLastClubMemberQuery();
            SaveClubMemberQuery scmq = new SaveClubMemberQuery();
            WSHttpBinding b = new WSHttpBinding();
            b.Security.Mode = SecurityMode.Transport;
            b.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
            b.MaxReceivedMessageSize = 2147483647;
            b.ReaderQuotas.MaxStringContentLength = 2147483647;
            b.ReaderQuotas.MaxArrayLength = 2147483647;
            System.ServiceModel.EndpointAddress endPoint =
                          new System.ServiceModel.EndpointAddress("https://api.otech.co.il:21443/service/otech.svc");
            OtechServiceClient client = new OtechServiceClient(b, endPoint);
            //OtechServiceClient client = new OtechServiceClient();


            //client.Endpoint.Address = "https://api.otech.co.il:21443/service/otech.svc";





            glcm.UserName = currentStore.Api_user; //"best";
            glcm.Password = currentStore.Api_pass; //"best2018";

            glcm.Date = new DateTime(2000, 1, 1);
            //glcm.Date = new DateTime();
            //glcm.Date = DateTime.Now;
            glcm.Filters = new ServiceFilter[2];
            //glcm.Filters = new ServiceFilter[1];
            glcm.Filters[0] = new ServiceFilter();
            glcm.Filters[1] = new ServiceFilter();
            if (currentStore.SearchByTZ)////////////////////  searches by "clb_num"
            {
                glcm.Filters[0].Field = "clb_num";
                glcm.Filters[0].Value = model.TZ;
                glcm.Filters[0].Type = FilterType.number;
            }
            else
            {
                glcm.Filters[0].Field = "clb_cell_pre";
                glcm.Filters[0].Value = model.Email.Substring(0, 3);// this is realy the mobile phone
                glcm.Filters[0].Type = FilterType.varchar;

                glcm.Filters[1].Field = "clb_cell";
                glcm.Filters[1].Value = model.Email.Substring(3, 7); // this is realy the mobile phone
                glcm.Filters[1].Type = FilterType.varchar;
            }
            //scmq.clubMember.
            scmq.UserName = currentStore.Api_user; //"best";
            scmq.Password = currentStore.Api_pass; //"best2018";
            scmq.IsNewClubMember = true;
            scmq.clubMember = new Nop.Web.ServiceReference1.lakclbST();
            scmq.clubMember.clb_f_name = model.FirstName;
            scmq.clubMember.clb_name = model.LastName;
            scmq.clubMember.clb_cell = model.Email.Substring(3, 7);
            scmq.clubMember.clb_cell_pre = model.Email.Substring(0, 3);
            //scmq.clubMember.clb_kvuza = "0";


            //scmq.clubMember.clb_from_mach = "37";
            scmq.clubMember.clb_from_date = DateTime.Now;
            scmq.clubMember.clb_email = model.RealEmail;  ////////// this is Email

            try
            {
                scmq.clubMember.clb_num = Int32.Parse(model.TZ);  //Int32.Parse(currentStore.Api_Custom_1); //
            }
            catch
            {
                scmq.clubMember.clb_num = 0;
            }

            try
            {
                scmq.clubMember.clb_hnt_id = Int32.Parse(currentStore.Api_Custom_1);
            }
            catch
            {
                scmq.clubMember.clb_hnt_id = 0;
            }

            try
            {
                scmq.clubMember.clb_from_mach = currentStore.Api_Custom_2;
            }
            catch
            {
                scmq.clubMember.clb_from_mach = "0";

            }


            if (nls.DateOfBirthWithYear == null)
            {
                scmq.clubMember.clb_date_leda = nls.DateOfBirth;
            }
            else
            {
                scmq.clubMember.clb_date_leda = nls.DateOfBirthWithYear;
            }


            if (currentStore.AllowTZ)
            {

                scmq.clubMember.clb_tz = Int32.Parse(model.TZ);

            }

            try
            {

                scmq.Hatima = Convert.FromBase64String(model.signature.Split(',')[1]);
            }
            catch
            {

            }
            scmq.clubMember.clb_update = DateTime.Now;
            var s = client.GetLastClubMembers(glcm);

            if (s.ClubMembersk__BackingField.Length == 0)
            {
                var fuc = client.SaveClubMember(scmq);
                nls.phone = fuc.SucceededMessagek__BackingField;
                _newsLetterSubscriptionService.UpdateNewsLetterSubscription(nls);
            }
            if (!string.IsNullOrWhiteSpace(s.ErrorMessagek__BackingField))
            {
                _logger.Error(string.Format("Error posting data for phone number: {0} .error msg{1} ", scmq.clubMember.clb_cell, s.ErrorMessagek__BackingField));
                var redirectUrl2 = "/" + currentStore.Id.ToString() + "/" + "RegisterResult/" + ((int)UserRegistrationType.errorPosting).ToString();
                return Redirect(redirectUrl2);

            }


            ///////////////////////////
            if (s.ClubMembersk__BackingField.Length != 0)
            {
                var redirectUrl2 = "/" + currentStore.Id.ToString() + "/" + "RegisterResult/" + ((int)UserRegistrationType.numberExists).ToString();
                return Redirect(redirectUrl2);
            }

            try
            {
                welcomeMsg = currentStore.welcomeMsg.Replace("$N$", nls.FirstName);
            }
            catch
            {

            }

            _emailSender.SendSmS(model.Email, welcomeMsg, currentStore.SmsUserName, currentStore.SmsPass, "", currentStore.preMsg, null);







            // }
            //}









            //notifications
            //if (_customerSettings.NotifyNewCustomerRegistration)
            //    _workflowMessageService.SendCustomerRegisteredNotificationMessage(customer, _localizationSettings.DefaultAdminLanguageId);

            //raise event       



            //send customer welcome message



            // var redirectUrl = Url.RouteUrl("RegisterResult", new { resultId = (int)UserRegistrationType.Standard });

            redirectUrl = "/" + currentStore.Id.ToString() + "/" + "RegisterResult/" + ((int)UserRegistrationType.Standard).ToString();
            if (!String.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                redirectUrl = _webHelper.ModifyQueryString(redirectUrl, "returnurl=" + HttpUtility.UrlEncode(returnUrl), null);
            return Redirect(redirectUrl);
        }
   






        [Route("{id}/SendToSubscription")]
        public ActionResult SendToSubscription(int id)
        {
            int storeID = id;
            if (!(_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator()))
            {
                if (_workContext.CurrentCustomer.StoreId != storeID)
                    return RedirectToRoute("login");
            }
            var currentStore = _webStoreContext.CurrentStoreCustomer(storeID);
            var model = new findSubModel();
            model.rgb = currentStore.rgb;


            return View(model);



        }
        //[Route("{id}/SendToSubscription")]
        //[HttpPost]
        //public ActionResult SendToSubscription(findSubModel findSubmodel, int id)
        //{
        //    int storeID = id;
        //    Store CurrentStore = _webStoreContext.CurrentStoreCustomer(id);
        //    if (!(_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator()))
        //    {
        //        if (_workContext.CurrentCustomer.StoreId != storeID)
        //            return RedirectToRoute("login");
        //    }
        //    //var sub = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(findSubmodel.phone, storeID);

        //    //if (sub==null || !sub.Active)
        //    //{
        //        //try
        //        //{
        //        //    _newsLetterSubscriptionService.DeleteNewsLetterSubscription(sub);
        //        //}
        //        //catch
        //        //{

        //        //}

        //        var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        //        var stringChars = new char[10];
        //        var random = new Random();

        //        for (int i = 0; i < stringChars.Length; i++)
        //        {
        //            stringChars[i] = chars[random.Next(chars.Length)];
        //        }

        //        var randString = new String(stringChars);

        //        NewsLetterSubscription awaitingConformation = new NewsLetterSubscription
        //        {
        //            StoreId = id,
        //            Email = findSubmodel.phone,
        //            CreatedOnUtc = DateTime.Now,
        //            ownerIpAddress = _webHelper.GetCurrentIpAddress(),
        //            Active = false,
        //            NewsLetterSubscriptionGuid = Guid.NewGuid(),
        //            origin = 10,
        //            FirstName = randString



        //        };


        //        string tempUrl = "להרשמה למועדון הלקוחות יש להיכנס ללינק הבא " + CurrentStore.Url + storeID.ToString() + "/" + awaitingConformation.NewsLetterSubscriptionGuid.ToString() + "/" + awaitingConformation.Email + "/" + "SubDeviceRegister";
        //        _newsLetterSubscriptionService.InsertNewsLetterSubscription(awaitingConformation);
        //        _emailSender.SendSmS(awaitingConformation.Email, tempUrl, CurrentStore.SmsUserName, CurrentStore.SmsPass, "", CurrentStore.preMsg, CurrentStore.smsRemovalLink);
        //        var redirectUrl = "/" + CurrentStore.Id.ToString() + "/" + "RegisterResult/" + "100".ToString();
        //        return Redirect(redirectUrl);


        //    //}
        //    //else
        //    //{
        //    //    var redirectUrl = "/" + CurrentStore.Id.ToString() + "/" + "RegisterResult/" + "5";
        //    //    return Redirect(redirectUrl);
        //    //}
        //}


        [Route("{id}/SendToSubscription")]
        [HttpPost]
        public ActionResult SendToSubscription(findSubModel findSubmodel, int id)
        {
            int storeID = id;
            Store CurrentStore = _webStoreContext.CurrentStoreCustomer(id);
         

            _emailSender.SendSmS(findSubmodel.phone, CurrentStore.welcomeMsg, CurrentStore.SmsUserName, CurrentStore.SmsPass, "", CurrentStore.preMsg, CurrentStore.smsRemovalLink);
            var redirectUrl = "/" + CurrentStore.Id.ToString() + "/" + "RegisterResult/" + 100.ToString();
            return Redirect(redirectUrl);


        }





        [Route("{id}/{rand}/{num}/SubDeviceRegister/")]
        public ActionResult SubDeviceRegister()
          {
            var rawUrl = this.HttpContext.Request.RawUrl;
            string[] r2 = rawUrl.Split('/');


            string idFromUrl = r2[1];
            int r3 = Int32.Parse(idFromUrl);

            string randFromUrl = r2[2];
            string numberFromUrl = r2[3];


            Store currentStore = _webStoreContext.CurrentStoreCustomer(r3);
           NewsLetterSubscription subAwaiting =  _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(numberFromUrl, Int32.Parse(idFromUrl));
            if(subAwaiting==null || subAwaiting.NewsLetterSubscriptionGuid.ToString() != randFromUrl)// rand is wrong or sub already confirmed
            {
              return  Redirect("/ErrorPage.htm");
            }
            if (subAwaiting.ConfirmedOnUtc.HasValue)
            {
                var redirectUrl = "/" + idFromUrl.ToString() + "/" + "RegisterResult/" + ((int)UserRegistrationType.numberExists).ToString();
                return Redirect(redirectUrl);
            }

            //check whether registration is allowed


            var model = new RegisterModel();
            PrepareCustomerRegisterModel(model, currentStore);
            //enable newsletter by default
            model.Newsletter = true;
            model.rgb = currentStore.rgb;
            model.DateOfBirthRequired = currentStore.DateOfBirthRequired;
            model.Email = numberFromUrl;
            model.AllowRealEmail = currentStore.AllowRealEmail;
            model.AllowTZ = currentStore.AllowTZ;

            return View(model);
        }








        [HttpPost]
        [PublicAntiForgery]
        [ValidateInput(false)]

       
        [Route("{id}/{rand}/{num}/SubDeviceRegister/")]
        public ActionResult SubDeviceRegister(RegisterModel model, string returnUrl, FormCollection form)
        {
            var rawUrl = this.HttpContext.Request.RawUrl;
            string[] r2 = rawUrl.Split('/');


            string idFromUrl = r2[1];
            int r3 = Int32.Parse(idFromUrl);

            string randFromUrl = r2[2];
            string numberFromUrl = r2[3];


            Store currentStore = _webStoreContext.CurrentStoreCustomer(r3);
            NewsLetterSubscription subAwaiting = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByGuid(System.Guid.Parse(randFromUrl));
            if (subAwaiting == null || subAwaiting.NewsLetterSubscriptionGuid.ToString() != randFromUrl)// rand is wrong or sub already confirmed
            {
                return Redirect("/ErrorPage.htm");
            }
            if (subAwaiting.ConfirmedOnUtc.HasValue)
            {
                var redirectUrlError = "/" + idFromUrl.ToString() + "/" + "RegisterResult/" + "100";
                return Redirect(redirectUrlError);
            }
            var customer = _workContext.CurrentCustomer;



            IList<CustomerAttribute> caForForm = _customerAttributeService.GetAllCustomerAttributesByStoreID(r3);
            string[] drops = new string[2];
            string[] strings = new string[2];
            string[] checks = new string[5];
            //DateTime?[] dates = new DateTime?[2];
            int cdrops = 0;
            int cstrings = 0;
            //int cdates = 0;
            for (int i = 0; i < caForForm.Count; i++)
            {
                if (caForForm[i].DisplayOrder > 100) continue;
                string ccc;

                string s1 = string.Format("customer_attribute_{0}", caForForm[i].Id);

                ValueProviderResult x;
                string y;
                if (caForForm[i].AttributeControlTypeId == 20) break;
                if (caForForm[i].AttributeControlTypeId == 1)
                {
                    x = form.GetValue(s1);
                    y = x.AttemptedValue;
                    if (y == "0") ccc = null;
                    else
                    {
                        //ccc = _customerAttributeService.GetCustomerAttributeValueById(Int32.Parse(form.GetValue(s1).AttemptedValue.ToString())).Name;
                        ccc = y;
                    }
                }
                else { ccc = form.GetValue(s1).AttemptedValue.ToString(); }



                if (_customerAttributeService.GetCustomerAttributeById(caForForm[i].Id).AttributeControlTypeId == 1) { drops[cdrops] = ccc; cdrops++; }
                if (_customerAttributeService.GetCustomerAttributeById(caForForm[i].Id).AttributeControlTypeId == 4) { strings[cstrings] = ccc; cstrings++; }

                if (i >= 6) break;
            }
            //for (int j = 0; j < 6; j++) {
            //   if( model.CustomerAttributes[j].AttributeControlType == AttributeControlType.Datepicker)
            //    {
            //        dates[cdates] = model.CustomerAttributes[j].ParseDate();
            //        cdates++;
            //        if (cdates > 2) break;
            //    }

            //}
            cdrops = 0;
            cstrings = 0;
            //cdates = 0;





            //validate CAPTCHA



            subAwaiting.FirstName = model.FirstName;
            subAwaiting.LastName = model.LastName;
            subAwaiting.ConfirmedOnUtc = DateTime.Now;
            subAwaiting.confirmIpAddress = _webHelper.GetCurrentIpAddress();
            subAwaiting.Active = true;
            subAwaiting.points = currentStore.pointsOnReg;
            subAwaiting.gift1 = currentStore.gift2OnReg;
            subAwaiting.RealEmail = model.RealEmail;


            //properties


            //form fields
            //if (currentStore.GenderEnabled)
            subAwaiting.Gender = model.Gender;

            if (currentStore.DateOfBirthEnabled)
            {
                subAwaiting.DateOfBirth = model.ParseDateOfBirth();
                subAwaiting.DateOfBirthWithYear = model.ParseDateOfBirthWithYear();

            }

            if (currentStore.date2Enabled)
            {
                subAwaiting.Date2 = model.ParseDate2();

            }

            if (currentStore.date3Enabled)
            {
                subAwaiting.Date3 = model.ParseDate3();

            }

            if (model.check1Val)
            {
                subAwaiting.checkbox1 = currentStore.check1String;
            }
            if (model.check2Val)
            {
                subAwaiting.checkbox2 = currentStore.check2String;
            }
            if (model.check3Val)
            {
                subAwaiting.checkbox3 = currentStore.check3String;
            }
            if (model.check4Val)
            {
                subAwaiting.checkbox4 = currentStore.check4String;
            }
            if (model.check5Val)
            {
                subAwaiting.checkbox5 = currentStore.check5String;
            }
            if (!model.check1Val && !model.check2Val && !model.check3Val && !model.check4Val && !model.check5Val)
            {
                subAwaiting.checkbox1 = currentStore.check1String;
                subAwaiting.checkbox2 = currentStore.check2String;
                subAwaiting.checkbox3 = currentStore.check3String;
                subAwaiting.checkbox4 = currentStore.check4String;
                subAwaiting.checkbox5 = currentStore.check5String;
            }
            //if (_customerSettings.CityEnabled)
            subAwaiting.City = strings[0];
            // else { subAwaiting.City = "NaV"; }
            //if (_customerSettings.CompanyEnabled)
            subAwaiting.company = strings[1];
            // else { subAwaiting.company = "NaV"; }
            //if (_customerSettings.StreetAddressEnabled)
            subAwaiting.StreetAddress = drops[0];
            //  else { subAwaiting.StreetAddress = "NaV"; }
            //if (_customerSettings.StreetAddress2Enabled)
            subAwaiting.StreetAddress2 = drops[1];
            // else { subAwaiting.StreetAddress2 = "NaV"; }

            //subAwaiting.Date3 = dates[1].HasValue ?
            //(DateTime?)_dateTimeHelper.ConvertToUtcTime(dates[0].Value) : null;
            //if (dates[0].Year > 2016)
            // subAwaiting.Date2 = dates[0];
            //if (dates[0].Year > 2016)
            //    subAwaiting.Date3 = dates[1];

            subAwaiting.points = currentStore.pointsOnReg;
            /////////29.10.17 - start//////////////////////////////////


            //   subAwaiting.gift1 = currentStore.gift1OnReg;
            //if (currentStore.gift1OnReg)
            //{
            //    subAwaiting.gift1EndDate = DateTime.Now.AddDays(currentStore.gift1ValidityDays);
            //}
            subAwaiting.gift2 = currentStore.gift2OnReg;

            if (currentStore.gift2OnReg)
            {
                subAwaiting.gift2EndDate = DateTime.Now.AddDays(currentStore.gift2ValidityDays);
                currentStore.gift2NotImp += 1;
            }

            /////////29.10.17 - end//////////////////////////////////
          
            subAwaiting.origin = 10;
            subAwaiting.Active = true;
            _newsLetterSubscriptionService.UpdateNewsLetterSubscription(subAwaiting);
            currentStore.NumberOfCustomers += 1;
            currentStore.weeklyCustomerCountInt += 1;
            currentStore.monthlyCustomerCount += 1;
            _storeService.UpdateStore(currentStore);
            string welcomeMsg = null;
            try
            {
                welcomeMsg = currentStore.welcomeMsg.Replace("$N$", subAwaiting.FirstName);
            }
            catch
            {

            }

            GetLastClubMemberQuery glcm = new GetLastClubMemberQuery();
            SaveClubMemberQuery scmq = new SaveClubMemberQuery();
            OtechServiceClient client = new OtechServiceClient();
            glcm.UserName = currentStore.Api_user; //"best";
            glcm.Password = currentStore.Api_pass; //"best2018";

            glcm.Date = new DateTime(2000, 1, 1);
            glcm.Filters = new ServiceFilter[1];
            if (currentStore.SearchByTZ)
            {
                glcm.Filters[0].Field = "clb_tz";
                glcm.Filters[0].Value = model.TZ;
                glcm.Filters[0].Type = FilterType.number;
            }
            else
            {
                glcm.Filters[0].Field = "clb_cell";
                glcm.Filters[0].Value = model.Email; // this is realy the mobile phone
                glcm.Filters[0].Type = FilterType.varchar;
            }
            scmq.UserName = currentStore.Api_user; //"best";
            scmq.Password = currentStore.Api_pass; //"best2018";
            scmq.IsNewClubMember = true;
            scmq.clubMember = new Nop.Web.ServiceReference1.lakclbST();

            try
            {
                scmq.clubMember.clb_num = Int32.Parse(currentStore.Api_Custom_1);
            }
            catch
            {
                scmq.clubMember.clb_num = 0;
            }

            scmq.clubMember.clb_f_name = model.FirstName + " " + model.LastName;


            scmq.clubMember.clb_cell = model.Email;
            scmq.clubMember.clb_cell_pre = model.Email.Substring(0, 3);
            if (subAwaiting.DateOfBirthWithYear == null)
            {
                scmq.clubMember.clb_date_leda = subAwaiting.DateOfBirth;
            }
            else
            {
                scmq.clubMember.clb_date_leda = subAwaiting.DateOfBirthWithYear;
            }
            scmq.clubMember.clb_email = subAwaiting.RealEmail;  ////////// this is Email
            if (currentStore.AllowTZ)
            {

                scmq.clubMember.clb_tz = Int32.Parse(model.TZ);
            }
            scmq.clubMember.clb_update = DateTime.Now;
            var s = client.GetLastClubMembers(glcm);

            if (s.ClubMembersk__BackingField.Length == 0)
            {
                var fuc = client.SaveClubMember(scmq);
                subAwaiting.phone = fuc.SucceededMessagek__BackingField;
                _newsLetterSubscriptionService.UpdateNewsLetterSubscription(subAwaiting);
            }
            if (!string.IsNullOrWhiteSpace(s.ErrorMessagek__BackingField))
            {
                _logger.Error(string.Format("Error posting data for phone number: {0} .error msg{1} ", scmq.clubMember.clb_cell, s.ErrorMessagek__BackingField));
                var redirectUrl2 = "/" + currentStore.Id.ToString() + "/" + "RegisterResult/" + ((int)UserRegistrationType.errorPosting).ToString();
                return Redirect(redirectUrl2);

            }


            ///////////////////////////
            if (s.ClubMembersk__BackingField.Length != 0)
            {
                var redirectUrl2 = "/" + currentStore.Id.ToString() + "/" + "RegisterResult/" + ((int)UserRegistrationType.numberExists).ToString();
                return Redirect(redirectUrl2);
            }

            _emailSender.SendSmS(model.Email, welcomeMsg, currentStore.SmsUserName, currentStore.SmsPass, "", currentStore.preMsg, null);

            // }
            //}












            // var redirectUrl = Url.RouteUrl("RegisterResult", new { resultId = (int)UserRegistrationType.Standard });
            var redirectUrl = "/" + currentStore.Id.ToString() + "/" + "RegisterResult/" + 99.ToString();

            return Redirect(redirectUrl);
        }





        //[Route("{id}/stat")]
        //public ActionResult stat()
        //{
        //    if (!(_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator()))
        //    {
        //        if (_workContext.CurrentCustomer.StoreId != id)
        //            return RedirectToRoute("RegisterResult", new { resultId = (int)UserRegistrationType.AdminApproval });
        //    }
        //    var model = new statModel();

        //    return View(model);

        //}








    }
}
