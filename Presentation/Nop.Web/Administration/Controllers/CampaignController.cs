﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Nop.Admin.Extensions;
using Nop.Admin.Models.Messages;
using Nop.Core;
using Nop.Core.Domain.Messages;
using Nop.Services.Customers;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Admin.Models.Settings;
using Nop.Core.Domain.Customers;
using Nop.Admin.Models.Stores;
using Nop.Services.Common;

using Nop.Core.Domain.Catalog;
using Nop.Web.Models.Customer;
using Nop.Core.Domain.Stores;
using System.Threading.Tasks;

namespace Nop.Admin.Controllers
{
	public partial class CampaignController : BaseAdminController
	{
        private readonly ICampaignService _campaignService;
        private readonly IDateTimeHelper _DateTimeHelper;
        private readonly IEmailAccountService _emailAccountService;
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
        private readonly ILocalizationService _localizationService;
        private readonly IMessageTokenProvider _messageTokenProvider;
        private readonly IStoreContext _storeContext;
        private readonly IStoreService _storeService;
        private readonly IPermissionService _permissionService;
	    private readonly ICustomerService _customerService;
        private readonly IWorkContext _workContext;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ICustomerAttributeService _customerAttributeService;
        private readonly ICustomerAttributeParser _customerAttributeParser;
        private readonly CustomerSettings _customerSettings;
        private readonly IEmailSender _emailSender;
        static volatile int campId;
        private Object locker = new Object();

        public CampaignController(ICampaignService campaignService,
            IDateTimeHelper DateTimeHelper, 
            IEmailAccountService emailAccountService,
            EmailAccountSettings emailAccountSettings,
            INewsLetterSubscriptionService newsLetterSubscriptionService,
            ILocalizationService localizationService, 
            IMessageTokenProvider messageTokenProvider,
            IStoreContext storeContext,
            IStoreService storeService,
            IPermissionService permissionService, 
            ICustomerService customerService,
              IGenericAttributeService genericAttributeService,
             IWorkContext workContext,
              ICustomerAttributeService customerAttributeService,
              ICustomerAttributeParser customerAttributeParser,
               IEmailSender emailSender,
              CustomerSettings customerSettings
            )
		{
            this._campaignService = campaignService;
            this._DateTimeHelper = DateTimeHelper;
            this._emailAccountService = emailAccountService;
            this._emailAccountSettings = emailAccountSettings;
            this._newsLetterSubscriptionService = newsLetterSubscriptionService;
            this._localizationService = localizationService;
            this._messageTokenProvider = messageTokenProvider;
            this._storeContext = storeContext;
            this._storeService = storeService;
            this._permissionService = permissionService;
            this._customerService = customerService;
            this._workContext = workContext;
            this._genericAttributeService = genericAttributeService;
            this._customerAttributeService = customerAttributeService;
            this._customerAttributeParser = customerAttributeParser;
            this._customerSettings = customerSettings;
            this._emailSender = emailSender;

        }


        [ChildActionOnly]
        public ActionResult StoreScopeConfiguration()
        {
            var model = new StoreScopeConfigurationModel();
            if (_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator())
            {
                var allStores = _storeService.GetAllStores();
                if (allStores.Count < 2)
                    return Content("");

                
                foreach (var s in allStores)
                {
                    model.Stores.Add(new StoreModel
                    {
                        Id = s.Id,
                        Name = s.Name
                    });
                }

                model.StoreId = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            }
            if(_workContext.CurrentCustomer.IsRegistered())
            {
                var allStores = _storeService.GetStoreById(_workContext.CurrentCustomer.StoreId);
               
               
                    model.Stores.Add(new StoreModel
                    {
                        Id = allStores.Id,
                        Name = allStores.Name
                    });
               

            }
            
            return PartialView(model);
        }
        public ActionResult ChangeStoreScopeConfiguration(int storeid, string returnUrl = "")
        {
            var store = _storeService.GetStoreById(storeid);
            if (store != null || storeid == 0)
            {
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.AdminAreaStoreScopeConfiguration, storeid);
            }

            //home page
            if (String.IsNullOrEmpty(returnUrl))
                returnUrl = Url.Action("Index", "Home", new { area = "Admin" });
            //prevent open redirection attack
            if (!Url.IsLocalUrl(returnUrl))
                return RedirectToAction("Index", "Home", new { area = "Admin" });
            return Redirect(returnUrl);
        }

        [NonAction]
        protected virtual void PrepareCustomCustomerAttributes(CampaignModel campaign)
        {
            Store currentStore;
            if (_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator())
            {
                var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
                //currentStore = _storeService.GetStoreById(campaign.StoreId);
                if (storeScope == 0)
                    storeScope = 1;
                currentStore = _storeService.GetStoreById(storeScope);
            }
            else { currentStore = _storeService.GetStoreById(_workContext.CurrentCustomer.StoreId); }

            campaign.GenderEnabled = currentStore.GenderEnabled;
            campaign.DateOfBirthEnabled = currentStore.DateOfBirthEnabled;
            campaign.Date2Enabled = currentStore.date2Enabled;
            campaign.Date2Name = currentStore.date2Name;
            campaign.Date3Enabled = currentStore.date3Enabled;
            campaign.Date3Name = currentStore.date3Name;
            if (currentStore.DateOfBirthEnabled)
            {
                //campaign.DateOfBirth = DateTime.Now;
                //campaign.DateOfBirthTo = DateTime.Now;
            }
            var customerAttributes = _customerAttributeService.GetAllCustomerAttributesByStoreID(currentStore.Id);
            foreach (var attribute in customerAttributes)
            {
                var attributeModel = new CustomerAttributeModel
                {
                    Id = attribute.Id,
                    Name = attribute.GetLocalized(x => x.Name),
                    IsRequired = false,
                    AttributeControlType = attribute.AttributeControlType,
                    DisplayOrder = attribute.DisplayOrder
                };

                if (attribute.ShouldHaveValues())
                {
                    //values
                    var attributeValues = _customerAttributeService.GetCustomerAttributeValues(attribute.Id);
                    int counterForEdit = 0;// if we are here we are sure in dropdown att
                    bool preSelectedFlag = false;
                    foreach (var attributeValue in attributeValues)
                    {
                        var valueModel = new CustomerAttributeValueModel
                        {
                            Id = attributeValue.Id,
                            Name = attributeValue.GetLocalized(x => x.Name),
                            
                        };
                        if(counterForEdit == 0)
                        {
                            if(campaign.StreetAddress == attributeValue.Name)
                            {
                                valueModel.IsPreSelected = true;
                                preSelectedFlag = true;
                            }
                        }
                        if (counterForEdit == 1)
                        {
                            if (campaign.StreetAddress2 == attributeValue.Name)
                            {
                                valueModel.IsPreSelected = true;
                                preSelectedFlag = true;
                            }
                        }
                        if (!preSelectedFlag)
                        {
                            valueModel.IsPreSelected = attributeValue.IsPreSelected;
                        }
                        attributeModel.Values.Add(valueModel);
                    }
                }

                //set already selected attributes
               
                //switch (attribute.AttributeControlType)
                //{
                //    case AttributeControlType.DropdownList:
                //    case AttributeControlType.RadioList:
                //    case AttributeControlType.Checkboxes:
                //        {
                            
                //        }
                //        break;
                //    case AttributeControlType.ReadonlyCheckboxes:
                //        {
                //            //do nothing
                //            //values are already pre-set
                //        }
                //        break;
                //    case AttributeControlType.TextBox:
                //    case AttributeControlType.MultilineTextbox:
                //        {
                            
                //        }
                //        break;
                //    case AttributeControlType.ColorSquares:
                //    case AttributeControlType.ImageSquares:
                //    case AttributeControlType.Datepicker:
                //    case AttributeControlType.FileUpload:
                //    default:
                //        //not supported attribute control types
                //        break;
                //}

                campaign.Cabm.Add(attributeModel);
            }


            
        }

        [NonAction]
        protected virtual string FormatTokens(string[] tokens)
        {
            var sb = new StringBuilder();
            for (int i = 0; i < tokens.Length; i++)
            {
                string token = tokens[i];
                sb.Append(token);
                if (i != tokens.Length - 1)
                    sb.Append(", ");
            }

            return sb.ToString();
        }

        protected virtual void PrepareStoresModel(CampaignModel model)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            
            //if (_workContext.CurrentCustomer.IsAdmin())
            //{
            //    var stores = _storeService.GetAllStores();
            //    foreach (var store in stores)
            //    {
            //        model.AvailableStores.Add(new SelectListItem
            //        {
            //            Text = store.Name,
            //            Value = store.Id.ToString()
            //        });
            //    }
            //}
            //else
            //{
            //    var store = _storeService.GetStoreById(_workContext.CurrentCustomer.StoreId);
            //    model.AvailableStores.Add(new SelectListItem
            //    {
            //        Text = store.Name,
            //        Value = store.Id.ToString()
            //    });

            //}
        }

        [NonAction]
        protected virtual void PrepareAttModel(CampaignModel model)
        {
            if (model == null)
                throw new ArgumentNullException("model");

           
        }
        
        [NonAction]
        protected virtual void PrepareCustomerRolesModel(CampaignModel model)
	    {
            //if (model == null)
            //    throw new ArgumentNullException("model");

            //model.AvailableCustomerRoles.Add(new SelectListItem
            //{
            //    Text = _localizationService.GetResource("Admin.Common.All"),
            //    Value = "0"
            //});
            //var roles = _customerService.GetAllCustomerRoles();
            //foreach (var customerRole in roles)
            //{
            //    model.AvailableCustomerRoles.Add(new SelectListItem
            //    {
            //        Text = customerRole.Name,
            //        Value = customerRole.Id.ToString()
            //    });
            //}
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

		public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
                return AccessDeniedView();
            var model = new CampaignListModel();
            if (_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator())
            {
                var stores = _storeService.GetAllStores();
              



                foreach (var store in stores)
                {
                    model.AvailableStores.Add(new SelectListItem
                    {
                        Text = store.Name,
                        Value = store.Id.ToString()
                    });
                }
            }
            else
            {
                var store = _storeService.GetStoreById(_workContext.CurrentCustomer.StoreId);
                model.AvailableStores.Add(new SelectListItem
                {
                    Text = store.Name,
                    Value = store.Id.ToString()
                });
            }


            return View(model);
		}

        [HttpPost]
        public ActionResult List(DataSourceRequest command, CampaignListModel searchModel)
        {
            IList<Campaign> campaigns = null;
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
                return AccessDeniedView();
            if (_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator())
            {
                campaigns = _campaignService.GetAllCampaigns(searchModel.StoreId);
            }
            else
            {
                 campaigns = _campaignService.GetAllCampaigns(_workContext.CurrentCustomer.StoreId);
            }

               
            int xy = 4;
            var gridModel = new DataSourceResult
            {
                Data = campaigns.Select(x =>
                {
                    var model = x.ToModel();
                   // model.CreatedOnUtc = _DateTimeHelper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc);
                    if (x.DontSendBeforeDateUtc.HasValue)
                        model.DontSendBeforeDate = _DateTimeHelper.ConvertToUserTime(x.DontSendBeforeDateUtc.Value, DateTimeKind.Utc);

                    return model;
                    
                }).OrderByDescending(model => model.CreatedOnUtc),
                Total = campaigns.Count
            };
            return Json(gridModel);
        }

        public ActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
                return AccessDeniedView();
            var model = new CampaignModel();
            int storeScope;
            Store currentStore;
            if (_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator())
            {
                 storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
                //currentStore = _storeService.GetStoreById(campaign.StoreId);
                // currentStore = _storeService.GetStoreById(storeScope);
                model.AdminOrVendor = true;
            }
            else {
                storeScope =(_workContext.CurrentCustomer.StoreId);
                model.AdminOrVendor = false;
            }
            if (storeScope == 0) storeScope = 1;
            currentStore = _storeService.GetStoreById(storeScope);
           
            model.AllowedTokens = FormatTokens(_messageTokenProvider.GetListOfCampaignAllowedTokens());
            //stores
            model.StoreId = storeScope;
            PrepareStoresModel(model);
            //customer roles
            PrepareCustomerRolesModel(model);
            PrepareCustomCustomerAttributes(model);
            model.gift1Enabled = currentStore.gift1;
            model.gift1Name = currentStore.gift1Name;
            model.gift2Enabled = currentStore.gift2;
            model.gift2Name = currentStore.gift2Name;
            model.gift3Enabled = currentStore.gift3;
            model.gift3Name = currentStore.gift3Name;
            model.gift4Enabled = currentStore.gift4;
            model.gift4Name = currentStore.gift4Name;
            model.isPoints = currentStore.isPoints;
            model.NumOfSent = 0;
            model.MinPoints = 0;
            model.MaxPoints = 0;
            model.Date2Enabled = currentStore.date2Enabled;
            model.Date2Required = currentStore.date2Required;
            model.Date2Name = currentStore.date2Name;
            model.Date3Enabled = currentStore.date3Enabled;
            model.Date3Required = currentStore.date3Required;
            model.Date3Name = currentStore.date3Name;
            model.check1 = currentStore.check1;
            model.check1String = currentStore.check1String;
            model.check2 = currentStore.check2;
            model.check2String = currentStore.check2String;
            model.check3 = currentStore.check3;
            model.check3String = currentStore.check3String;
            model.check4 = currentStore.check4;
            model.check4String = currentStore.check4String;
            model.check5 = currentStore.check5;
            model.check5String = currentStore.check5String;
            model.multiField = currentStore.multiField;
            model.multiFieldName = currentStore.multiFieldName;
            model.isSent = 0;


            model.origin = 0;


            return View(model);
        }


        
        public ActionResult send(int id)
        {
            lock (locker)
            {
                if (id == campId)  // locking agaist double clicking on frontEnd
                {
                    return Json(new
                    {
                        redirect = -1
                    });
                }
                else
                {
                    campId = id;
                }
            }
            int storeScope2;
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
                return AccessDeniedView();

            var campaign = _campaignService.GetCampaignById(id);
            if (campaign == null)
                //No campaign found with the specified id
                return RedirectToAction("List");
            if (_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator())
                storeScope2 = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            else
            {
                storeScope2 = _workContext.CurrentCustomer.StoreId;
            }
            Store currentStore = _storeService.GetStoreById(storeScope2);
            if(campaign.StoreId!= storeScope2)
                return AccessDeniedView();

            //var lastCamp = _campaignService.GetLastCampaign(storeScope2);
            //if (lastCamp != null && (DateTime.Now - lastCamp.SentOn).Value.Days == 0 && (DateTime.Now - lastCamp.CreatedOnUtc).Value.Minutes <= 5)
            //{
            //    return RedirectToAction("List");
            //}
            try
            {
                campaign.NumOfSent = SendMassEmail(campaign);
                campaign.isSent = 1;
                campaign.SentOn = DateTime.Now;
                _campaignService.UpdateCampaign(campaign);
            }
            catch
            {
                return Json(new
                {
                    redirect = "error"
                });

            }
            
            return Json(new
            {
                redirect = campaign.NumOfSent.ToString()
             });

            
        }






        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create(CampaignModel model, FormCollection form, bool continueEditing)
        {
            int storeScope2;
           // DateTime timeStamp;
            
            /////////////////////////////////////////////////////////////////
            if (_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator())
                storeScope2 = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            else
            {
                storeScope2 = _workContext.CurrentCustomer.StoreId;
            }
            Store currentStore = _storeService.GetStoreById(storeScope2);
            //var lastCamp = _campaignService.GetLastCampaign(storeScope2);
            //if(lastCamp!=null &&(DateTime.Now - lastCamp.CreatedOnUtc ).Value.Days == 0 && (DateTime.Now - lastCamp.CreatedOnUtc).Value.Minutes <= 5)
            //{
            //    return RedirectToAction("List");
            //}


            Campaign campaign = model.ToEntity();
            IList<CustomerAttribute> caForForm = _customerAttributeService.GetAllCustomerAttributesByStoreID(storeScope2);
            string[] drops = new string[2];
            string[] strings = new string[2];
            DateTime[] dates = new DateTime[2];
            int cdrops = 0;
            int cstrings = 0;
            int storeScope = 0;
            //int cdates = 0;
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
                return AccessDeniedView();
           
          
               
               


                campaign.CreatedOnUtc = DateTime.Now;
               
               

              campaign.DontSendBeforeDateUtc = model.DontSendBeforeDate.HasValue ?
                  (DateTime?)model.DontSendBeforeDate.Value: null;

                campaign.DateOfBirth = model.ParseDateOfBirth();
                campaign.DateOfBirthTo = model.ParseDateOfBirthTo();
                campaign.Date2 = model.ParseDate2();
                campaign.Date2To = model.ParseDate2To();
                campaign.Date3 = model.ParseDate3();
                campaign.Date3To = model.ParseDate3To();


                //campaign.DateOfBirth = model.DateOfBirth.HasValue ?
                //  (DateTime?)model.DateOfBirth.Value : null;
                //campaign.DateOfBirthTo = model.DateOfBirthTo.HasValue ?
                // (DateTime?)model.DateOfBirthTo.Value: null;
                //campaign.Date2 = model.Date2.HasValue ?
                //  (DateTime?)model.Date2.Value: null;
                //campaign.Date2To = model.Date2To.HasValue ?
                //  (DateTime?)model.Date2To.Value : null;
                //campaign.Date3= model.Date3.HasValue ?
                // (DateTime?)model.Date3.Value : null;
                //campaign.Date3To = model.Date3To.HasValue ?
                // (DateTime?)model.Date3To.Value : null;
                /////////////////////////////////////////////////////////////////////////////////




                //campaign.Date2.Value. = DateTime.MinValue;
                //campaign.Date2To = DateTime.MinValue;
                //campaign.Date3 = DateTime.MinValue;
                //campaign.Date3To = DateTime.MinValue;
                //campaign.DateOfBirth = DateTime.MinValue;
                //campaign.DontSendBeforeDateUtc = DateTime.MinValue;






                for (int i = 0; i < caForForm.Count; i++)
                {
                    if (caForForm[i].DisplayOrder > 100) continue;
                    string ccc;

                    string s1 = string.Format("customer_attribute_{0}", caForForm[i].Id);

                    ValueProviderResult x;
                    string y;
                    if (caForForm[i].AttributeControlTypeId == 20) break;
                    if (caForForm[i].AttributeControlTypeId == 1)
                    {
                        x = form.GetValue(s1);
                        y = x.AttemptedValue;
                        if (y == "0") ccc = null;
                        else
                        {
                            ccc = _customerAttributeService.GetCustomerAttributeValueById(Int32.Parse(form.GetValue(s1).AttemptedValue.ToString())).Name;
                        }
                    }
                    else { ccc = form.GetValue(s1).AttemptedValue.ToString(); }
                    if (_customerAttributeService.GetCustomerAttributeById(caForForm[i].Id).AttributeControlTypeId == 1) { drops[cdrops] = ccc; cdrops++; }
                    if (_customerAttributeService.GetCustomerAttributeById(caForForm[i].Id).AttributeControlTypeId == 4) { strings[cstrings] = ccc; cstrings++; }

                    if (i >= 6) break;
                }
                cdrops = 0;
                cstrings = 0;
                //cdates = 0;
                //if(_customerSettings.StreetAddressEnabled)
                   campaign.StreetAddress = drops[0];
                //if (_customerSettings.StreetAddress2Enabled)
                    campaign.StreetAddress2 = drops[1];
                //if (_customerSettings.CityEnabled)
                    campaign.city = strings[0];
                //if (_customerSettings.CompanyEnabled)
                    campaign.company = strings[1];
               
                campaign.Gender = model.Gender;
                campaign.origin = model.origin;// depricated
            if (model.check1Val)
            {
                campaign.checkbox1 = currentStore.check1String;
            }
            if (model.check2Val)
            {
                campaign.checkbox2 = currentStore.check2String;
            }
            if (model.check3Val)
            {
                campaign.checkbox3 = currentStore.check3String;
            }
            if (model.check4Val)
            {
                campaign.checkbox4 = currentStore.check4String;
            }
            if (model.check5Val)
            {
                campaign.checkbox5 = currentStore.check5String;
            }




            //if(_customerSettings)




            campaign.StoreId = storeScope2;

               
                if(model.withName)
                {
                    string b1 = currentStore.preFirstNameMsg + " $N$" + "\n";
                        campaign.Body = b1 +campaign.Body;

                }
                //campaign.NumOfSent = SendMassEmail(campaign);
            campaign.Subject = "";
                _campaignService.InsertCampaign(campaign);

                SuccessNotification(_localizationService.GetResource("Admin.Promotions.Campaigns.Added"));
               // return continueEditing ? RedirectToAction("Edit", new { id = campaign.Id }) : RedirectToAction("List");
           

            var model2 = campaign.ToModel();
            


            //If we got this far, something failed, redisplay form
           
            //stores
            PrepareStoresModel(model2);
            //customer roles
            PrepareCustomerRolesModel(model2);

            return RedirectToAction("Edit", new { id = campaign.Id });
        }

		public ActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
                return AccessDeniedView();
            int storeScope2;
            // DateTime timeStamp;

            /////////////////////////////////////////////////////////////////
            if (_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator())
                storeScope2 = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            else
            {
                storeScope2 = _workContext.CurrentCustomer.StoreId;
            }
            Store currentStore = _storeService.GetStoreById(storeScope2);
            var campaign = _campaignService.GetCampaignById(id);
            if (campaign == null)
                //No campaign found with the specified id
                return RedirectToAction("List");

            CampaignModel model = campaign.ToModel();
            PrepareStoresModel(model);
            //customer roles
            PrepareCustomerRolesModel(model);
           
            PrepareCustomCustomerAttributes(model);
            model.gift1Enabled = currentStore.gift1;
            model.gift1Name = currentStore.gift1Name;
            model.gift2Enabled = currentStore.gift2;
            model.gift2Name = currentStore.gift2Name;
            model.gift3Enabled = currentStore.gift3;
            model.gift3Name = currentStore.gift3Name;
            model.gift4Enabled = currentStore.gift4;
            model.gift4Name = currentStore.gift4Name;
            model.isPoints = currentStore.isPoints;
           
            model.Date2Enabled = currentStore.date2Enabled;
           
            model.Date2Name = currentStore.date2Name;
            model.Date3Enabled = currentStore.date3Enabled;
           
            model.Date3Name = currentStore.date3Name;
            model.check1 = currentStore.check1;
            model.check1String = currentStore.check1String;
            if(campaign.checkbox1 != null)
            {
                model.check1Val = true;
            }
            else
            {
                model.check1Val = false;
            }
            model.check2 = currentStore.check2;
            model.check2String = currentStore.check2String;
            model.check3 = currentStore.check3;
            model.check3String = currentStore.check3String;
            model.check4 = currentStore.check4;
            model.check4String = currentStore.check4String;
            model.check5 = currentStore.check5;
            model.check5String = currentStore.check5String;
            model.multiField = currentStore.multiField;
            model.multiFieldName = currentStore.multiFieldName;

            if (campaign.checkbox2 != null)
            {
                model.check2Val = true;
            }
            else
            {
                model.check2Val = false;
            }
            if (campaign.checkbox3 != null)
            {
                model.check3Val = true;
            }
            else
            {
                model.check3Val = false;
            }
            if (campaign.checkbox4 != null)
            {
                model.check4Val = true;
            }
            else
            {
                model.check4Val = false;
            }
            if (campaign.checkbox5 != null)
            {
                model.check5Val = true;
            }
            else
            {
                model.check5Val = false;
            }

            return View(model);
		}

        [HttpPost]
        [ParameterBasedOnFormName("save-continue", "continueEditing")]
        [FormValueRequired("save", "save-continue")]
        public ActionResult Edit(CampaignModel model, FormCollection form, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
                return AccessDeniedView();

            var campaign = _campaignService.GetCampaignById(model.Id);
            if (campaign == null)
                //No campaign found with the specified id
                return RedirectToAction("List");

            //if (ModelState.IsValid)
            //{
                campaign = model.ToEntity(campaign);
                //campaign.DontSendBeforeDateUtc = model.DontSendBeforeDate.HasValue ?
                //    (DateTime?)_DateTimeHelper.ConvertToUtcTime(model.DontSendBeforeDate.Value) : null;
                int storeScope2;
                /////////////////////////////////////////////////////////////////
                if (_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator())
                    storeScope2 = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
                else
                {
                    storeScope2 = _workContext.CurrentCustomer.StoreId;
                }
                campaign.StoreId = storeScope2;
            campaign.Subject = "";
            IList<CustomerAttribute> caForForm = _customerAttributeService.GetAllCustomerAttributesByStoreID(campaign.StoreId);
            string[] drops = new string[2];
            string[] strings = new string[2];
            DateTime[] dates = new DateTime[2];
            int cdrops = 0;
            int cstrings = 0;

            for (int i = 0; i < caForForm.Count; i++)
            {
                if (caForForm[i].DisplayOrder > 100) continue;
                string ccc;

                string s1 = string.Format("customer_attribute_{0}", caForForm[i].Id);

                ValueProviderResult x;
                string y;
                if (caForForm[i].AttributeControlTypeId == 20) break;
                if (caForForm[i].AttributeControlTypeId == 1)
                {
                    x = form.GetValue(s1);
                    y = x.AttemptedValue;
                    if (y == "0") ccc = null;
                    else
                    {
                        ccc = _customerAttributeService.GetCustomerAttributeValueById(Int32.Parse(form.GetValue(s1).AttemptedValue.ToString())).Name;
                    }
                }
                else { ccc = form.GetValue(s1).AttemptedValue.ToString(); }
                if (_customerAttributeService.GetCustomerAttributeById(caForForm[i].Id).AttributeControlTypeId == 1) { drops[cdrops] = ccc; cdrops++; }
                if (_customerAttributeService.GetCustomerAttributeById(caForForm[i].Id).AttributeControlTypeId == 4) { strings[cstrings] = ccc; cstrings++; }

                if (i >= 6) break;
            }
            cdrops = 0;
            cstrings = 0;
            //cdates = 0;
            //if(_customerSettings.StreetAddressEnabled)
            campaign.StreetAddress = drops[0];
            //if (_customerSettings.StreetAddress2Enabled)
            campaign.StreetAddress2 = drops[1];
            //if (_customerSettings.CityEnabled)
            campaign.city = strings[0];
            //if (_customerSettings.CompanyEnabled)
            campaign.company = strings[1];

            campaign.Gender = model.Gender;


            _campaignService.UpdateCampaign(campaign);

                SuccessNotification(_localizationService.GetResource("Admin.Promotions.Campaigns.Updated"));
                return continueEditing ? RedirectToAction("Edit", new { id = campaign.Id }) : RedirectToAction("List");
            //}

            //If we got this far, something failed, redisplay form
            model.AllowedTokens = FormatTokens(_messageTokenProvider.GetListOfCampaignAllowedTokens());
            //stores
            PrepareStoresModel(model);
            //customer roles
            PrepareCustomerRolesModel(model);
            return View(model);
		}

       
        
        public ActionResult SendTestEmail(string email,string body, string withName, string subject= null)
        {
            string b1;
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
                return AccessDeniedView();
            int storeScope;
            Store currentStore;
            if (_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator())
            {
                storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
                //currentStore = _storeService.GetStoreById(campaign.StoreId);
                // currentStore = _storeService.GetStoreById(storeScope);
            }
            else { storeScope = (_workContext.CurrentCustomer.StoreId); }
            if (storeScope == 0) storeScope = 1;
            currentStore = _storeService.GetStoreById(storeScope);

            string nameMsg = body.Replace("[לקוח]", "טל");
             nameMsg = nameMsg.Replace("[נקודות]", "100");




            _emailSender.SendSmS(email, nameMsg, currentStore.SmsUserName, currentStore.SmsPass, "", currentStore.preMsg,currentStore.smsRemovalLink, currentStore.Id.ToString());
            return Json(new
            {
                redirect = ""
            });
        }

            //If we got this far, something failed, redisplay form
         
      

        //[HttpPost, ActionName("Create")]
        //[FormValueRequired("send-mass-email")]
        public int SendMassEmail(Campaign campaign) 
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
                return 0;

            //Campaign campaign = _campaignService.GetCampaignById(model.Id);
            //if (campaign == null)
            //    //No campaign found with the specified id
            //    return 0;


            //model.AllowedTokens = FormatTokens(_messageTokenProvider.GetListOfCampaignAllowedTokens());
            //stores
            //PrepareStoresModel(model);
            //customer roles
           /* PrepareCustomerRolesModel(model)*/;

            try
            {
                //var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
                //if (emailAccount == null)
                //    throw new NopException("Email account could not be loaded");

                //subscribers of certain store?
                //var store = _storeService.GetStoreById(campaign.StoreId);
                //var storeId = store != null ? store.Id : 0;
                bool? g1;
                bool? g2;
                bool? g3;
                bool? g4;
                if (campaign.gift1 == 0)
                { g1 = null; }
                else if (campaign.gift1 == 1)
                { g1 = true; }
                else { g1 = false; }

                if (campaign.gift2 == 0)
                { g2 = null; }
                else if (campaign.gift2 == 1)
                { g2 = true; }

                else { g2 = false; }

                if (campaign.gift3 == 0)
                { g3 = null; }
                else if (campaign.gift3 == 1)
                { g3 = true; }
                else { g3 = false; }


                if (campaign.gift4 == 0)
                { g4 = null; }
                else if (campaign.gift4 == 1)
                { g4 = true; }
                else { g4 = false; }

                int? maxPointsTemp;
                int minPointsTemp;
               
                var subscriptions = _newsLetterSubscriptionService.GetNewsLetterSubscriptions(
                    storeId: campaign.StoreId, 
                  
                    Gender: campaign.Gender,
                    
                    company: campaign.company,
                    city: campaign.city,
                    StreetAddress: campaign.StreetAddress,
                    StreetAddress2: campaign.StreetAddress2,
                    origin: campaign.origin,
                    minPoints: campaign.MinPoints,
                    maxPoints: campaign.MaxPoints,
                    gift1:g1,
                    gift2: g2,
                    gift3: g3,
                    gift4: g4,
                    createdFromUtc: campaign.CreatedOn,
                    createdToUtc: campaign.CreatedTo,
                    DateOfBirthFrom: campaign.DateOfBirth,
                    DateOfBirthTo: campaign.DateOfBirthTo,
                    date2From: campaign.Date2,
                    date2To: campaign.Date2To,
                    date3From: campaign.Date3,
                    date3To: campaign.Date3To,
                    multiOrigin : campaign.multiOrigin,
                     ezDevice : campaign.ezDevice,
                     facebook : campaign.facebook,
                     instagram : campaign.instagram,
                     website : campaign.website,
                     internet : campaign.internet,
                     legacyDb : campaign.legacyDb,
                     other : campaign.other,
                     check1: campaign.checkbox1,
                     check2: campaign.checkbox2,
                     check3: campaign.checkbox3,
                     check4: campaign.checkbox4,
                     check5: campaign.checkbox5 );

                Dictionary<string, string> phonesList = new Dictionary<string, string>();
                Dictionary < string, string> pointsNlsList = new Dictionary<string, string>();

                foreach (var nlsub in subscriptions)
                {
                    try
                    {
                        phonesList.Add(nlsub.Email, nlsub.FirstName);
                        pointsNlsList.Add(nlsub.Email, nlsub.points.ToString());
                    }
                    catch(Exception e)
                    {
                        var x = e.Data;
                        var y = e.Message;
                    }
                }
                Store store = _storeService.GetStoreById(campaign.StoreId);
                string nameMsg = campaign.Body.Replace("[לקוח]", "$N$");
                 nameMsg = nameMsg.Replace("[נקודות]", "$P$");
                _emailSender.SendMasSmS(phonesList, nameMsg, store.SmsUserName, store.SmsPass, campaign.DontSendBeforeDateUtc 
                ,store.preMsg, store.smsRemovalLink,store.Id.ToString(),pointsNlsList:pointsNlsList);

                return phonesList.Count;

                
                //SuccessNotification(string.Format(_localizationService.GetResource("Admin.Promotions.Campaigns.MassEmailSentToCustomers"), totalEmailsSent), false);
               
            }
            catch (Exception exc)
            {
                ErrorNotification(exc, false);
            }

            //If we got this far, something failed, redisplay form
            return 0;
        }



        public async Task<int> SendMassEmailAsync(Campaign campaign)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
                return 0;

            //Campaign campaign = _campaignService.GetCampaignById(model.Id);
            //if (campaign == null)
            //    //No campaign found with the specified id
            //    return 0;


            //model.AllowedTokens = FormatTokens(_messageTokenProvider.GetListOfCampaignAllowedTokens());
            //stores
            //PrepareStoresModel(model);
            //customer roles
            /* PrepareCustomerRolesModel(model)*/
            ;

            try
            {
                //var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
                //if (emailAccount == null)
                //    throw new NopException("Email account could not be loaded");

                //subscribers of certain store?
                //var store = _storeService.GetStoreById(campaign.StoreId);
                //var storeId = store != null ? store.Id : 0;
                bool? g1;
                bool? g2;
                bool? g3;
                bool? g4;
                if (campaign.gift1 == 0)
                { g1 = null; }
                else if (campaign.gift1 == 1)
                { g1 = true; }
                else { g1 = false; }

                if (campaign.gift2 == 0)
                { g2 = null; }
                else if (campaign.gift2 == 1)
                { g2 = true; }

                else { g2 = false; }

                if (campaign.gift3 == 0)
                { g3 = null; }
                else if (campaign.gift3 == 1)
                { g3 = true; }
                else { g3 = false; }


                if (campaign.gift4 == 0)
                { g4 = null; }
                else if (campaign.gift4 == 1)
                { g4 = true; }
                else { g4 = false; }


                var subscriptions = _newsLetterSubscriptionService.GetNewsLetterSubscriptions(
                    storeId: campaign.StoreId,

                    Gender: campaign.Gender,

                    company: campaign.company,
                    city: campaign.city,
                    StreetAddress: campaign.StreetAddress,
                    StreetAddress2: campaign.StreetAddress2,
                    origin: campaign.origin,
                    minPoints: campaign.MaxPoints,
                    maxPoints: campaign.MinPoints,
                    gift1: g1,
                    gift2: g2,
                    gift3: g3,
                    gift4: g4,
                    createdFromUtc: campaign.CreatedOn,
                    createdToUtc: campaign.CreatedTo,
                    DateOfBirthFrom: campaign.DateOfBirth,
                    DateOfBirthTo: campaign.DateOfBirthTo,
                    date2From: campaign.Date2,
                    date2To: campaign.Date2To,
                    date3From: campaign.Date3,
                    date3To: campaign.Date3To,
                    multiOrigin: campaign.multiOrigin,
                     ezDevice: campaign.ezDevice,
                     facebook: campaign.facebook,
                     instagram: campaign.instagram,
                     website: campaign.website,
                     internet: campaign.internet,
                     legacyDb: campaign.legacyDb,
                     other: campaign.other);

                Dictionary<string, string> phonesList = new Dictionary<string, string>();

                foreach (var nlsub in subscriptions)
                {
                    phonesList.Add(nlsub.Email, nlsub.FirstName);
                }
                Store store = _storeService.GetStoreById(campaign.StoreId);

                _emailSender.SendMasSmS(phonesList, campaign.Body, store.SmsUserName, store.SmsPass, campaign.DontSendBeforeDateUtc
                , store.preMsg, store.smsRemovalLink,store.Id.ToString());

                return phonesList.Count;


                //SuccessNotification(string.Format(_localizationService.GetResource("Admin.Promotions.Campaigns.MassEmailSentToCustomers"), totalEmailsSent), false);

            }
            catch (Exception exc)
            {
                ErrorNotification(exc, false);
            }

            //If we got this far, something failed, redisplay form
            return 0;
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
                return AccessDeniedView();

            var campaign = _campaignService.GetCampaignById(id);
            if (campaign == null)
                //No campaign found with the specified id
                return RedirectToAction("List");

            _campaignService.DeleteCampaign(campaign);

            SuccessNotification(_localizationService.GetResource("Admin.Promotions.Campaigns.Deleted"));
			return RedirectToAction("List");
		}
	}
}
