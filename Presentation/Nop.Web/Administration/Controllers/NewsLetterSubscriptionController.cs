﻿using System;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Nop.Admin.Extensions;
using Nop.Admin.Models.Messages;
using Nop.Core;
using Nop.Services.Customers;
using Nop.Services.ExportImport;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;
using System.Collections.Generic;
using Nop.Services.Common;
using Nop.Admin.Models.Settings;
using Nop.Core.Domain.Customers;
using Nop.Admin.Models.Stores;

namespace Nop.Admin.Controllers
{
	public partial class NewsLetterSubscriptionController : BaseAdminController
	{
		private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
		private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly IStoreService _storeService;
        private readonly ICustomerService _customerService;
        private readonly IExportManager _exportManager;
        private readonly IImportManager _importManager;
        private readonly IStoreContext _webStoreContext;
        private readonly IWorkContext _workContext;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly CustomerSettings _customerSettings;
        
        public NewsLetterSubscriptionController(INewsLetterSubscriptionService newsLetterSubscriptionService,
			IDateTimeHelper DateTimeHelper,
            ILocalizationService localizationService,
            IPermissionService permissionService,
            IStoreService storeService,
            ICustomerService customerService,
            IExportManager exportManager,
            IImportManager importManager,
             IWorkContext workContext,
             IStoreContext webStoreContext,
             IGenericAttributeService genericAttributeService,
             CustomerSettings customerSettings
            )
		{
			this._newsLetterSubscriptionService = newsLetterSubscriptionService;
			this._dateTimeHelper = DateTimeHelper;
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._storeService = storeService;
            this._customerService = customerService;
            this._exportManager = exportManager;
            this._importManager = importManager;
            this._workContext = workContext;
            this._webStoreContext = webStoreContext;
            this._genericAttributeService = genericAttributeService;
            this._customerSettings = customerSettings;
            
        }
        [ChildActionOnly]
        public ActionResult StoreScopeConfiguration()
        {
            var model = new StoreScopeConfigurationModel();
            if (_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator())
            {
                var allStores = _storeService.GetAllStores();
                if (allStores.Count < 2)
                    return Content("");


                foreach (var s in allStores)
                {
                    model.Stores.Add(new StoreModel
                    {
                        Id = s.Id,
                        Name = s.Name
                    });
                }

                model.StoreId = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            }
            if (_workContext.CurrentCustomer.IsRegistered())
            {
                var allStores = _storeService.GetStoreById(_workContext.CurrentCustomer.StoreId);


                model.Stores.Add(new StoreModel
                {
                    Id = allStores.Id,
                    Name = allStores.Name
                });


            }

            return PartialView(model);
        }
        public ActionResult ChangeStoreScopeConfiguration(int storeid, string returnUrl = "")
        {
            var store = _storeService.GetStoreById(storeid);
            if (store != null || storeid == 0)
            {
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.AdminAreaStoreScopeConfiguration, storeid);
            }

            //home page
            if (String.IsNullOrEmpty(returnUrl))
                returnUrl = Url.Action("Index", "Home", new { area = "Admin" });
            //prevent open redirection attack
            if (!Url.IsLocalUrl(returnUrl))
                return RedirectToAction("Index", "Home", new { area = "Admin" });
            return Redirect(returnUrl);
        }

        public ActionResult Index()
		{
			return RedirectToAction("List");
		}

		public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
                return AccessDeniedView();
            var model = new NewsLetterSubscriptionListModel();
            if (_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator())
            {
                var stores = _storeService.GetAllStores();




                foreach (var store in stores)
                {
                    model.AvailableStores.Add(new SelectListItem
                    {
                        Text = store.Name,
                        Value = store.Id.ToString()
                    });
                }
            }
            else
            {
                var store = _storeService.GetStoreById(_workContext.CurrentCustomer.StoreId);
                model.AvailableStores.Add(new SelectListItem
                {
                    Text = store.Name,
                    Value = store.Id.ToString()
                });
            }


            return View(model);
        }

		[HttpPost]
		public ActionResult SubscriptionList(DataSourceRequest command, NewsLetterSubscriptionListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNewsletterSubscribers))
                return AccessDeniedView();
            int storeId = 0;
            bool? isActive = null;
            if (model.ActiveId == 1)
                isActive = true;
            else if (model.ActiveId == 2)
                isActive = false;

            var startDateValue = (model.StartDate == null) ? null
                : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);
            var endDateValue = (model.EndDate == null) ? null
                : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);
            var Email = (model.SearchEmail == null) ? null
                : model.SearchEmail;
           
          

            if (_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator())
            {
               
                    storeId = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);


                
            }
            else
            {
                storeId = _workContext.CurrentCustomer.StoreId;
            }

                var newsletterSubscriptions = _newsLetterSubscriptionService.GetAllNewsLetterSubscriptions(model.SearchEmail,
                startDateValue, endDateValue, storeId, true, model.CustomerRoleId,
                command.Page - 1, command.PageSize, origin: model.origin);
            
            var gridModel = new DataSourceResult
            {
                Data = newsletterSubscriptions.Select(x =>
				{
                    var m = new NewsLetterSubscriptionLight();
                    m.Email = x.Email;
                    m.FirstName = x.FirstName;
                    m.LastName = x.LastName;
                  
			
				   
                    var z = x.CreatedOnUtc;
                    DateTime zz;
                    if (z == null)
                        zz = Convert.ToDateTime("2017-03-03 14:11:20.560");
                    else
                    {
                        zz = (DateTime)x.CreatedOnUtc;
                    }

                    m.CreatedOn = _dateTimeHelper.ConvertToUserTime(zz, DateTimeKind.Utc);
					return m;
				}).OrderByDescending(m =>m.CreatedOn),
                Total = newsletterSubscriptions.TotalCount
            };

            return Json(gridModel);
		}

        [HttpPost]
        public ActionResult SubscriptionUpdate([Bind(Exclude = "CreatedOn")] NewsLetterSubscriptionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNewsletterSubscribers))
                return AccessDeniedView();

            if (!ModelState.IsValid)
            {
                return Json(new DataSourceResult { Errors = ModelState.SerializeErrors() });
            }

            var subscription = _newsLetterSubscriptionService.GetNewsLetterSubscriptionById(model.Id);
            subscription.Email = model.Email;
            subscription.Active = model.Active;
            _newsLetterSubscriptionService.UpdateNewsLetterSubscription(subscription);

            return new NullJsonResult();
        }

        [HttpPost]
        public ActionResult SubscriptionDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNewsletterSubscribers))
                return AccessDeniedView();

            var subscription = _newsLetterSubscriptionService.GetNewsLetterSubscriptionById(id);
            if (subscription == null)
                throw new ArgumentException("No subscription found with the specified id");
            _newsLetterSubscriptionService.DeleteNewsLetterSubscription(subscription);

            return new NullJsonResult();
        }

        [HttpPost, ActionName("List")]
        [FormValueRequired("exportcsv")]
        public ActionResult ExportCsv()
        {
             int storeId = 0;
            if (_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator())
            {
               storeId = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
               

            }
            else
            {
                return AccessDeniedView();
            }
            //int storeId = 0;
            //bool? isActive = null;
            //if (model.ActiveId == 1)
            //    isActive = true;
            //else if (model.ActiveId == 2)
            //    isActive = false;
         
           

            var subscriptions = _newsLetterSubscriptionService.GetAllNewsLetterSubscriptions(null,
                null, null, storeId, true, 0);

		    string result = _exportManager.ExportNewsletterSubscribersToTxt(subscriptions);

            string fileName = String.Format("newsletter_emails_{0}_{1}.csv", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"), CommonHelper.GenerateRandomDigitCode(4));
			return File(Encoding.UTF8.GetBytes(result), MimeTypes.TextCsv, fileName);
		}

        [HttpPost]
        public ActionResult ImportCsv(FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNewsletterSubscribers))
                return AccessDeniedView();

            try
            {
                var file = Request.Files["importcsvfile"];
                if (file != null && file.ContentLength > 0)
                {
                    int count = _importManager.ImportNewsletterSubscribersFromTxt(file.InputStream);
                    //int count = _importManager.ImportRewardPointsFromTxt(file.InputStream);
                    //int count = _importManager.blat(file.InputStream);

                    SuccessNotification(String.Format(_localizationService.GetResource("Admin.Promotions.NewsLetterSubscriptions.ImportEmailsSuccess"), count));
                    return RedirectToAction("List");
                }
                ErrorNotification(_localizationService.GetResource("Admin.Common.UploadFile"));
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("List");
            }
        }


        public ActionResult Edit(string num)
        {
            
           var nls = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(num,_workContext.CurrentCustomer.StoreId);
            if (nls == null || nls.Active == false)
            {
                //No customer found with the specified id
                return RedirectToAction("List");
            }

            var model = new NewsLetterSubscriptionModel();
            model.FirstName = nls.FirstName;
            model.LastName = nls.LastName;
            model.DateOfBirth = nls.DateOfBirth;
            model.Date2 = nls.Date2;
            model.Date3 = nls.Date3;
            model.StreetAddress = nls.StreetAddress;
            model.StreetAddress2 = nls.StreetAddress2;
            return View(model);


        }





        [ChildActionOnly]
        public virtual ActionResult NlsStatistics()
        {
           

            return PartialView();
        }
        [ChildActionOnly]
        public virtual ActionResult NlsFacebookStatistics()
        {


            return PartialView();
        }

        public virtual ActionResult NlsFace2EZStatistics()
        {


            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public virtual ActionResult LoadNlsStatistics(string period,int origin = 0,int firstOrigin=0)
        {


            int storeId = _workContext.CurrentCustomer.StoreId;
            var result = new List<object>();

            var nowDt = _dateTimeHelper.ConvertToUserTime(DateTime.Now);
            var timeZone = _dateTimeHelper.CurrentTimeZone;
            //var searchCustomerRoleIds = new[] { _customerService.GetCustomerRoleBySystemName(SystemCustomerRoleNames.Registered).Id };

            var culture = new System.Globalization.CultureInfo(_workContext.WorkingLanguage.LanguageCulture);

            
                switch (period)
                {
                    case "year":
                        //year statistics
                        var yearAgoDt = nowDt.AddYears(-1).AddMonths(1);
                        var searchYearDateUser = new DateTime(yearAgoDt.Year, yearAgoDt.Month, 1);
                        if (!timeZone.IsInvalidTime(searchYearDateUser))
                        {
                            for (int i = 0; i <= 12; i++)
                            {
                                result.Add(new
                                {
                                    date = searchYearDateUser.Date.ToString("Y", culture),
                                    value = _newsLetterSubscriptionService.GetStats(
                                        createdFromUtc: _dateTimeHelper.ConvertToUtcTime(searchYearDateUser, timeZone),
                                        createdToUtc: _dateTimeHelper.ConvertToUtcTime(searchYearDateUser.AddMonths(1), timeZone),
                                        storeId: storeId,
                                        origin: origin,
                                        firstOrigin: firstOrigin,
                                        pageIndex: 0,
                                        pageSize: 1).ToString()
                                });

                                searchYearDateUser = searchYearDateUser.AddMonths(1);
                            }
                        }
                        break;

                    case "month":
                        //month statistics
                        var monthAgoDt = nowDt.AddDays(-30);
                        var searchMonthDateUser = new DateTime(monthAgoDt.Year, monthAgoDt.Month, monthAgoDt.Day);
                        if (!timeZone.IsInvalidTime(searchMonthDateUser))
                        {
                            for (int i = 0; i <= 30; i++)
                            {
                                result.Add(new
                                {
                                    date = searchMonthDateUser.Date.ToString("M", culture),
                                    value = _newsLetterSubscriptionService.GetStats(
                                        createdFromUtc: _dateTimeHelper.ConvertToUtcTime(searchMonthDateUser, timeZone),
                                        createdToUtc: _dateTimeHelper.ConvertToUtcTime(searchMonthDateUser.AddDays(1), timeZone),
                                        storeId: storeId,
                                        origin: origin,
                                        firstOrigin: firstOrigin,
                                        pageIndex: 0,
                                        pageSize: 1).ToString()
                                });

                                searchMonthDateUser = searchMonthDateUser.AddDays(1);
                            }
                        }
                        break;

                    case "week":
                    default:
                        //week statistics
                        var weekAgoDt = nowDt.AddDays(-7);
                        var searchWeekDateUser = new DateTime(weekAgoDt.Year, weekAgoDt.Month, weekAgoDt.Day);
                        if (!timeZone.IsInvalidTime(searchWeekDateUser))
                        {
                            for (int i = 0; i <= 7; i++)
                            {
                                result.Add(new
                                {
                                    date = searchWeekDateUser.Date.ToString("d dddd", culture),
                                    value = _newsLetterSubscriptionService.GetStats(
                                        createdFromUtc: _dateTimeHelper.ConvertToUtcTime(searchWeekDateUser, timeZone),
                                        createdToUtc: _dateTimeHelper.ConvertToUtcTime(searchWeekDateUser.AddDays(1), timeZone),
                                        storeId: storeId,
                                        origin: origin,
                                        firstOrigin: firstOrigin,
                                        pageIndex: 0,
                                        pageSize: 1).ToString()
                                });

                                searchWeekDateUser = searchWeekDateUser.AddDays(1);
                            }
                        }
                        break;
                }
            
          

            return Json(result, JsonRequestBehavior.AllowGet);
        }



        [AcceptVerbs(HttpVerbs.Get)]
        public virtual ActionResult LoadNlsFacebookStatistics(string period, int origin = 2, int firstOrigin = 0)
        {


            int storeId = _workContext.CurrentCustomer.StoreId;
            var result = new List<object>();

            var nowDt = _dateTimeHelper.ConvertToUserTime(DateTime.Now);
            var timeZone = _dateTimeHelper.CurrentTimeZone;
            //var searchCustomerRoleIds = new[] { _customerService.GetCustomerRoleBySystemName(SystemCustomerRoleNames.Registered).Id };

            var culture = new System.Globalization.CultureInfo(_workContext.WorkingLanguage.LanguageCulture);


            switch (period)
            {
                case "year":
                    //year statistics
                    var yearAgoDt = nowDt.AddYears(-1).AddMonths(1);
                    var searchYearDateUser = new DateTime(yearAgoDt.Year, yearAgoDt.Month, 1);
                    if (!timeZone.IsInvalidTime(searchYearDateUser))
                    {
                        for (int i = 0; i <= 12; i++)
                        {
                            result.Add(new
                            {
                                date = searchYearDateUser.Date.ToString("Y", culture),
                                value = _newsLetterSubscriptionService.GetStats(
                                    createdFromUtc: _dateTimeHelper.ConvertToUtcTime(searchYearDateUser, timeZone),
                                    createdToUtc: _dateTimeHelper.ConvertToUtcTime(searchYearDateUser.AddMonths(1), timeZone),
                                    storeId: storeId,
                                    origin: origin,
                                    firstOrigin: firstOrigin,
                                    pageIndex: 0,
                                    pageSize: 1).ToString()
                            });

                            searchYearDateUser = searchYearDateUser.AddMonths(1);
                        }
                    }
                    break;

                case "month":
                    //month statistics
                    var monthAgoDt = nowDt.AddDays(-30);
                    var searchMonthDateUser = new DateTime(monthAgoDt.Year, monthAgoDt.Month, monthAgoDt.Day);
                    if (!timeZone.IsInvalidTime(searchMonthDateUser))
                    {
                        for (int i = 0; i <= 30; i++)
                        {
                            result.Add(new
                            {
                                date = searchMonthDateUser.Date.ToString("M", culture),
                                value = _newsLetterSubscriptionService.GetStats(
                                    createdFromUtc: _dateTimeHelper.ConvertToUtcTime(searchMonthDateUser, timeZone),
                                    createdToUtc: _dateTimeHelper.ConvertToUtcTime(searchMonthDateUser.AddDays(1), timeZone),
                                    storeId: storeId,
                                    origin: origin,
                                    firstOrigin: firstOrigin,
                                    pageIndex: 0,
                                    pageSize: 1).ToString()
                            });

                            searchMonthDateUser = searchMonthDateUser.AddDays(1);
                        }
                    }
                    break;

                case "week":
                default:
                    //week statistics
                    var weekAgoDt = nowDt.AddDays(-7);
                    var searchWeekDateUser = new DateTime(weekAgoDt.Year, weekAgoDt.Month, weekAgoDt.Day);
                    if (!timeZone.IsInvalidTime(searchWeekDateUser))
                    {
                        for (int i = 0; i <= 7; i++)
                        {
                            result.Add(new
                            {
                                date = searchWeekDateUser.Date.ToString("d dddd", culture),
                                value = _newsLetterSubscriptionService.GetStats(
                                    createdFromUtc: _dateTimeHelper.ConvertToUtcTime(searchWeekDateUser, timeZone),
                                    createdToUtc: _dateTimeHelper.ConvertToUtcTime(searchWeekDateUser.AddDays(1), timeZone),
                                    storeId: storeId,
                                    origin: origin,
                                    firstOrigin: firstOrigin,
                                    pageIndex: 0,
                                    pageSize: 1).ToString()
                            });

                            searchWeekDateUser = searchWeekDateUser.AddDays(1);
                        }
                    }
                    break;
            }



            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        //[FormValueRequired("save", "save-continue")]
        //[ValidateInput(false)]
        //public ActionResult Edit(CustomerModel model, bool continueEditing, FormCollection form)
        //{
        //    if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
        //        return AccessDeniedView();

        //    var customer = _customerService.GetCustomerById(model.Id);
        //    if (customer == null || customer.Deleted)
        //        //No customer found with the specified id
        //        return RedirectToAction("List");

        //    //validate customer roles
        //    var allCustomerRoles = _customerService.GetAllCustomerRoles(true);
        //    var newCustomerRoles = new List<CustomerRole>();
        //    foreach (var customerRole in allCustomerRoles)
        //        if (model.SelectedCustomerRoleIds.Contains(customerRole.Id))
        //            newCustomerRoles.Add(customerRole);
        //    var customerRolesError = ValidateCustomerRoles(newCustomerRoles);
        //    if (!String.IsNullOrEmpty(customerRolesError))
        //    {
        //        ModelState.AddModelError("", customerRolesError);
        //        ErrorNotification(customerRolesError, false);
        //    }

        //    // Ensure that valid email address is entered if Registered role is checked to avoid registered customers with empty email address
        //    if (newCustomerRoles.Any() && newCustomerRoles.FirstOrDefault(c => c.SystemName == SystemCustomerRoleNames.Registered) != null && !CommonHelper.IsValidEmail(model.Email))
        //    {
        //        ModelState.AddModelError("", "Valid Email is required for customer to be in 'Registered' role");
        //        ErrorNotification("Valid Email is required for customer to be in 'Registered' role", false);
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            customer.AdminComment = model.AdminComment;
        //            customer.IsTaxExempt = model.IsTaxExempt;

        //            //prevent deactivation of the last active administrator
        //            if (!customer.IsAdmin() || model.Active || SecondAdminAccountExists(customer))
        //                customer.Active = model.Active;
        //            else
        //                ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.AdminAccountShouldExists.Deactivate"));



        //            //username

        //            //VAT number


        //            //vendor
        //            customer.VendorId = model.VendorId;

        //            //form fields
        //            //if (_DateTimeSettings.AllowCustomersToSetTimeZone)
        //            //    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.TimeZoneId, model.TimeZoneId);
        //            //if (_customerSettings.GenderEnabled)
        //            //    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Gender, model.Gender);
        //            //_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.FirstName, model.FirstName);
        //            //_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastName, model.LastName);
        //            //if (_customerSettings.DateOfBirthEnabled)
        //            //    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.DateOfBirth, model.DateOfBirth);
        //            //if (_customerSettings.CompanyEnabled)
        //            //    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Company, model.Company);
        //            //if (_customerSettings.StreetAddressEnabled)
        //            //    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress, model.StreetAddress);
        //            //if (_customerSettings.StreetAddress2Enabled)
        //            //    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress2, model.StreetAddress2);
        //            //if (_customerSettings.ZipPostalCodeEnabled)
        //            //    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.ZipPostalCode, model.ZipPostalCode);
        //            //if (_customerSettings.CityEnabled)
        //            //    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.City, model.City);
        //            //if (_customerSettings.CountryEnabled)
        //            //    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CountryId, model.CountryId);
        //            //if (_customerSettings.CountryEnabled && _customerSettings.StateProvinceEnabled)
        //            //    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StateProvinceId, model.StateProvinceId);
        //            //if (_customerSettings.PhoneEnabled)
        //            //    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Phone, model.Phone);
        //            //if (_customerSettings.FaxEnabled)
        //            //    _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Fax, model.Fax);

        //            //custom customer attributes
        //            var customerAttributes = ParseCustomCustomerAttributes(customer, form);
        //            _genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CustomCustomerAttributes, customerAttributes);

        //            //newsletter subscriptions
        //            if (!String.IsNullOrEmpty(customer.Email))
        //            {
        //                var allStores = _storeService.GetAllStores();
        //                foreach (var store in allStores)
        //                {
        //                    var newsletterSubscription = _newsLetterSubscriptionService
        //                        .GetNewsLetterSubscriptionByEmailAndStoreId(customer.Email, store.Id);
        //                    if (model.SelectedNewsletterSubscriptionStoreIds != null &&
        //                        model.SelectedNewsletterSubscriptionStoreIds.Contains(store.Id))
        //                    {
        //                        //subscribed
        //                        if (newsletterSubscription == null)
        //                        {
        //                            _newsLetterSubscriptionService.InsertNewsLetterSubscription(new NewsLetterSubscription
        //                            {
        //                                NewsLetterSubscriptionGuid = Guid.NewGuid(),
        //                                Email = customer.Email,
        //                                Active = true,
        //                                StoreId = store.Id,
        //                                CreatedOnUtc = DateTime.UtcNow
        //                            });
        //                        }
        //                    }
        //                    else
        //                    {
        //                        //not subscribed
        //                        if (newsletterSubscription != null)
        //                        {
        //                            _newsLetterSubscriptionService.DeleteNewsLetterSubscription(newsletterSubscription);
        //                        }
        //                    }
        //                }
        //            }


        //            //customer roles
        //            foreach (var customerRole in allCustomerRoles)
        //            {
        //                //ensure that the current customer cannot add/remove to/from "Administrators" system role
        //                //if he's not an admin himself
        //                if (customerRole.SystemName == SystemCustomerRoleNames.Administrators &&
        //                    !_workContext.CurrentCustomer.IsAdmin())
        //                    continue;

        //                if (model.SelectedCustomerRoleIds.Contains(customerRole.Id))
        //                {
        //                    //new role
        //                    if (customer.CustomerRoles.Count(cr => cr.Id == customerRole.Id) == 0)
        //                        customer.CustomerRoles.Add(customerRole);
        //                }
        //                else
        //                {
        //                    //prevent attempts to delete the administrator role from the user, if the user is the last active administrator
        //                    if (customerRole.SystemName == SystemCustomerRoleNames.Administrators && !SecondAdminAccountExists(customer))
        //                    {
        //                        ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.AdminAccountShouldExists.DeleteRole"));
        //                        continue;
        //                    }

        //                    //remove role
        //                    if (customer.CustomerRoles.Count(cr => cr.Id == customerRole.Id) > 0)
        //                        customer.CustomerRoles.Remove(customerRole);
        //                }
        //            }
        //            _customerService.UpdateCustomer(customer);


        //            //ensure that a customer with a vendor associated is not in "Administrators" role
        //            //otherwise, he won't have access to the other functionality in admin area
        //            if (customer.IsAdmin() && customer.VendorId > 0)
        //            {
        //                customer.VendorId = 0;
        //                _customerService.UpdateCustomer(customer);
        //                ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.AdminCouldNotbeVendor"));
        //            }

        //            //ensure that a customer in the Vendors role has a vendor account associated.
        //            //otherwise, he will have access to ALL products
        //            if (customer.IsVendor() && customer.VendorId == 0)
        //            {
        //                var vendorRole = customer
        //                    .CustomerRoles
        //                    .FirstOrDefault(x => x.SystemName == SystemCustomerRoleNames.Vendors);
        //                customer.CustomerRoles.Remove(vendorRole);
        //                _customerService.UpdateCustomer(customer);
        //                ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.CannotBeInVendoRoleWithoutVendorAssociated"));
        //            }


        //            //activity log
        //            _customerActivityService.InsertActivity("EditCustomer", _localizationService.GetResource("ActivityLog.EditCustomer"), customer.Id);

        //            SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.Updated"));
        //            if (continueEditing)
        //            {
        //                //selected tab
        //                SaveSelectedTabName();

        //                return RedirectToAction("Edit", new { id = customer.Id });
        //            }
        //            return RedirectToAction("List");
        //        }
        //        catch (Exception exc)
        //        {
        //            ErrorNotification(exc.Message, false);
        //        }
        //    }


        //    //If we got this far, something failed, redisplay form
        //    PrepareCustomerModel(model, customer, true);
        //    return View(model);
        //}

        //public ActionResult CustomerStatistics()
        //{
        //    if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
        //        return Content("");

        //    //a vendor doesn't have access to this report
        //    if (_workContext.CurrentVendor != null)
        //        return Content("");

        //    return PartialView();
        //}

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult LoadCustomerStatistics(string period)
        //{
        //    if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
        //        return Content("");

        //    var result = new List<object>();

        //    var nowDt = _dateTimeHelper.ConvertToUserTime(DateTime.Now);
        //    var timeZone = _dateTimeHelper.CurrentTimeZone;


        //    switch (period)
        //    {
        //        case "year":
        //            //year statistics
        //            var yearAgoRoundedDt = nowDt.AddYears(-1).AddMonths(1);
        //            var searchYearDateUser = new DateTime(yearAgoRoundedDt.Year, yearAgoRoundedDt.Month, 1);
        //            if (!timeZone.IsInvalidTime(searchYearDateUser))
        //            {
        //                DateTime searchYearDateUtc = _dateTimeHelper.ConvertToUtcTime(searchYearDateUser, timeZone);

        //                for (int i = 0; i <= 12; i++)
        //                {
        //                    result.Add(new
        //                    {
        //                        date = searchYearDateUser.Date.ToString("Y"),
        //                        value = _customerService.GetAllCustomers(
        //                            createdFromUtc: searchYearDateUtc,
        //                            createdToUtc: searchYearDateUtc.AddMonths(1),

        //                            pageIndex: 0,
        //                            pageSize: 1).TotalCount.ToString()
        //                    });

        //                    searchYearDateUtc = searchYearDateUtc.AddMonths(1);
        //                    searchYearDateUser = searchYearDateUser.AddMonths(1);
        //                }
        //            }
        //            break;

        //        case "month":
        //            //month statistics
        //            var searchMonthDateUser = new DateTime(nowDt.Year, nowDt.AddDays(-30).Month, nowDt.AddDays(-30).Day);
        //            if (!timeZone.IsInvalidTime(searchMonthDateUser))
        //            {
        //                DateTime searchMonthDateUtc = _dateTimeHelper.ConvertToUtcTime(searchMonthDateUser, timeZone);

        //                for (int i = 0; i <= 30; i++)
        //                {
        //                    result.Add(new
        //                    {
        //                        date = searchMonthDateUser.Date.ToString("M"),
        //                        value = _customerService.GetAllCustomers(
        //                            createdFromUtc: searchMonthDateUtc,
        //                            createdToUtc: searchMonthDateUtc.AddDays(1),

        //                            pageIndex: 0,
        //                            pageSize: 1).TotalCount.ToString()
        //                    });

        //                    searchMonthDateUtc = searchMonthDateUtc.AddDays(1);
        //                    searchMonthDateUser = searchMonthDateUser.AddDays(1);
        //                }
        //            }
        //            break;

        //        case "week":
        //        default:
        //            //week statistics
        //            var searchWeekDateUser = new DateTime(nowDt.Year, nowDt.AddDays(-7).Month, nowDt.AddDays(-7).Day);
        //            if (!timeZone.IsInvalidTime(searchWeekDateUser))
        //            {
        //                DateTime searchWeekDateUtc = _dateTimeHelper.ConvertToUtcTime(searchWeekDateUser, timeZone);

        //                for (int i = 0; i <= 7; i++)
        //                {
        //                    result.Add(new
        //                    {
        //                        date = searchWeekDateUser.Date.ToString("d dddd"),
        //                        value = _customerService.GetAllCustomers(
        //                            createdFromUtc: searchWeekDateUtc,
        //                            createdToUtc: searchWeekDateUtc.AddDays(1),

        //                            pageIndex: 0,
        //                            pageSize: 1).TotalCount.ToString()
        //                    });

        //                    searchWeekDateUtc = searchWeekDateUtc.AddDays(1);
        //                    searchWeekDateUser = searchWeekDateUser.AddDays(1);
        //                }
        //            }
        //            break;
        //    }

        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}
    }
}
