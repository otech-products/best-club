﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Nop.Admin.Extensions;
using Nop.Admin.Models.Messages;
using Nop.Core;
using Nop.Core.Domain.Messages;
using Nop.Services.Customers;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Admin.Models.Settings;
using Nop.Core.Domain.Customers;
using Nop.Admin.Models.Stores;
using Nop.Services.Common;

using Nop.Core.Domain.Catalog;
using Nop.Web.Models.Customer;
using Nop.Core.Domain.Stores;

namespace Nop.Admin.Controllers
{
    public partial class GiftController : BaseAdminController
    {

        private readonly ICampaignService _campaignService;
        private readonly IDateTimeHelper _DateTimeHelper;
        private readonly IEmailAccountService _emailAccountService;
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
        private readonly ILocalizationService _localizationService;
        private readonly IMessageTokenProvider _messageTokenProvider;
        private readonly IStoreContext _storeContext;
        private readonly IStoreService _storeService;
        private readonly IPermissionService _permissionService;
        private readonly ICustomerService _customerService;
        private readonly IWorkContext _workContext;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ICustomerAttributeService _customerAttributeService;
        private readonly ICustomerAttributeParser _customerAttributeParser;
        private readonly CustomerSettings _customerSettings;
        private readonly IEmailSender _emailSender;

        public GiftController(ICampaignService campaignService,
            IDateTimeHelper DateTimeHelper,
            IEmailAccountService emailAccountService,
            EmailAccountSettings emailAccountSettings,
            INewsLetterSubscriptionService newsLetterSubscriptionService,
            ILocalizationService localizationService,
            IMessageTokenProvider messageTokenProvider,
            IStoreContext storeContext,
            IStoreService storeService,
            IPermissionService permissionService,
            ICustomerService customerService,
              IGenericAttributeService genericAttributeService,
             IWorkContext workContext,
              ICustomerAttributeService customerAttributeService,
              ICustomerAttributeParser customerAttributeParser,
               IEmailSender emailSender,
              CustomerSettings customerSettings
            )
        {
            this._campaignService = campaignService;
            this._DateTimeHelper = DateTimeHelper;
            this._emailAccountService = emailAccountService;
            this._emailAccountSettings = emailAccountSettings;
            this._newsLetterSubscriptionService = newsLetterSubscriptionService;
            this._localizationService = localizationService;
            this._messageTokenProvider = messageTokenProvider;
            this._storeContext = storeContext;
            this._storeService = storeService;
            this._permissionService = permissionService;
            this._customerService = customerService;
            this._workContext = workContext;
            this._genericAttributeService = genericAttributeService;
            this._customerAttributeService = customerAttributeService;
            this._customerAttributeParser = customerAttributeParser;
            this._customerSettings = customerSettings;
            this._emailSender = emailSender;

        }
        [NonAction]
        protected virtual void PrepareCustomCustomerAttributes(CampaignModel campaign)
        {
            Store currentStore;
            if (_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsVendor())
            {
                var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
                //currentStore = _storeService.GetStoreById(campaign.StoreId);
                if (storeScope == 0)
                    storeScope = 1;
                currentStore = _storeService.GetStoreById(storeScope);
            }
            else { currentStore = _storeService.GetStoreById(_workContext.CurrentCustomer.StoreId); }

            campaign.GenderEnabled = currentStore.GenderEnabled;
            campaign.DateOfBirthEnabled = currentStore.DateOfBirthEnabled;
            campaign.Date2Enabled = currentStore.date2Enabled;
            campaign.Date2Name = currentStore.date2Name;
            campaign.Date3Enabled = currentStore.date3Enabled;
            campaign.Date3Name = currentStore.date3Name;
            if (currentStore.DateOfBirthEnabled)
            {
                //campaign.DateOfBirth = DateTime.Now;
                //campaign.DateOfBirthTo = DateTime.Now;
            }
            var customerAttributes = _customerAttributeService.GetAllCustomerAttributesByStoreID(currentStore.Id);
            foreach (var attribute in customerAttributes)
            {
                var attributeModel = new CustomerAttributeModel
                {
                    Id = attribute.Id,
                    Name = attribute.GetLocalized(x => x.Name),
                    IsRequired = false,
                    AttributeControlType = attribute.AttributeControlType,
                };

                if (attribute.ShouldHaveValues())
                {
                    //values
                    var attributeValues = _customerAttributeService.GetCustomerAttributeValues(attribute.Id);
                    foreach (var attributeValue in attributeValues)
                    {
                        var valueModel = new CustomerAttributeValueModel
                        {
                            Id = attributeValue.Id,
                            Name = attributeValue.GetLocalized(x => x.Name),
                            IsPreSelected = attributeValue.IsPreSelected
                        };
                        attributeModel.Values.Add(valueModel);
                    }
                }

                //set already selected attributes

                //switch (attribute.AttributeControlType)
                //{
                //    case AttributeControlType.DropdownList:
                //    case AttributeControlType.RadioList:
                //    case AttributeControlType.Checkboxes:
                //        {

                //        }
                //        break;
                //    case AttributeControlType.ReadonlyCheckboxes:
                //        {
                //            //do nothing
                //            //values are already pre-set
                //        }
                //        break;
                //    case AttributeControlType.TextBox:
                //    case AttributeControlType.MultilineTextbox:
                //        {

                //        }
                //        break;
                //    case AttributeControlType.ColorSquares:
                //    case AttributeControlType.ImageSquares:
                //    case AttributeControlType.Datepicker:
                //    case AttributeControlType.FileUpload:
                //    default:
                //        //not supported attribute control types
                //        break;
                //}

                campaign.Cabm.Add(attributeModel);
            }



        }

        [NonAction]
        protected virtual void PrepareCustomCustomerAttributesForGift(GiftModel campaign)
        {
            Store currentStore;
            if (_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsVendor())
            {
                var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
                //currentStore = _storeService.GetStoreById(campaign.StoreId);
                if (storeScope == 0)
                    storeScope = 1;
                currentStore = _storeService.GetStoreById(storeScope);
            }
            else { currentStore = _storeService.GetStoreById(_workContext.CurrentCustomer.StoreId); }

            campaign.GenderEnabled = currentStore.GenderEnabled;
            campaign.DateOfBirthEnabled = currentStore.DateOfBirthEnabled;
            campaign.Date2Enabled = currentStore.date2Enabled;
            campaign.Date2Name = currentStore.date2Name;
            campaign.Date3Enabled = currentStore.date3Enabled;
            campaign.Date3Name = currentStore.date3Name;
            if (currentStore.DateOfBirthEnabled)
            {
                //campaign.DateOfBirth = DateTime.Now;
                //campaign.DateOfBirthTo = DateTime.Now;
            }
            var customerAttributes = _customerAttributeService.GetAllCustomerAttributesByStoreID(currentStore.Id);
            foreach (var attribute in customerAttributes)
            {
                var attributeModel = new CustomerAttributeModel
                {
                    Id = attribute.Id,
                    Name = attribute.GetLocalized(x => x.Name),
                    IsRequired = false,
                    AttributeControlType = attribute.AttributeControlType,
                };

                if (attribute.ShouldHaveValues())
                {
                    //values
                    var attributeValues = _customerAttributeService.GetCustomerAttributeValues(attribute.Id);
                    foreach (var attributeValue in attributeValues)
                    {
                        var valueModel = new CustomerAttributeValueModel
                        {
                            Id = attributeValue.Id,
                            Name = attributeValue.GetLocalized(x => x.Name),
                            IsPreSelected = attributeValue.IsPreSelected
                        };
                        attributeModel.Values.Add(valueModel);
                    }
                }

                //set already selected attributes

                //switch (attribute.AttributeControlType)
                //{
                //    case AttributeControlType.DropdownList:
                //    case AttributeControlType.RadioList:
                //    case AttributeControlType.Checkboxes:
                //        {

                //        }
                //        break;
                //    case AttributeControlType.ReadonlyCheckboxes:
                //        {
                //            //do nothing
                //            //values are already pre-set
                //        }
                //        break;
                //    case AttributeControlType.TextBox:
                //    case AttributeControlType.MultilineTextbox:
                //        {

                //        }
                //        break;
                //    case AttributeControlType.ColorSquares:
                //    case AttributeControlType.ImageSquares:
                //    case AttributeControlType.Datepicker:
                //    case AttributeControlType.FileUpload:
                //    default:
                //        //not supported attribute control types
                //        break;
                //}

                campaign.Cabm.Add(attributeModel);
            }



        }




        [ChildActionOnly]
        public ActionResult StoreScopeConfiguration()
        {
            var model = new StoreScopeConfigurationModel();
            if (_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsVendor())
            {
                var allStores = _storeService.GetAllStores();
                if (allStores.Count < 2)
                    return Content("");


                foreach (var s in allStores)
                {
                    model.Stores.Add(new StoreModel
                    {
                        Id = s.Id,
                        Name = s.Name
                    });
                }

                model.StoreId = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            }
            if (_workContext.CurrentCustomer.IsRegistered())
            {
                var allStores = _storeService.GetStoreById(_workContext.CurrentCustomer.StoreId);


                model.Stores.Add(new StoreModel
                {
                    Id = allStores.Id,
                    Name = allStores.Name
                });


            }

            return PartialView(model);
        }
        public ActionResult ChangeStoreScopeConfiguration(int storeid, string returnUrl = "")
        {
            var store = _storeService.GetStoreById(storeid);
            if (store != null || storeid == 0)
            {
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    SystemCustomerAttributeNames.AdminAreaStoreScopeConfiguration, storeid);
            }

            //home page
            if (String.IsNullOrEmpty(returnUrl))
                returnUrl = Url.Action("Index", "Home", new { area = "Admin" });
            //prevent open redirection attack
            if (!Url.IsLocalUrl(returnUrl))
                return RedirectToAction("Index", "Home", new { area = "Admin" });
            return Redirect(returnUrl);
        }


        public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageDiscounts))
                return AccessDeniedView();
            var model = new GiftListModel();
            if (_workContext.CurrentCustomer.IsAdmin())
            {
                var stores = _storeService.GetAllStores();




                foreach (var store in stores)
                {
                    model.AvailableStores.Add(new SelectListItem
                    {
                        Text = store.Name,
                        Value = store.Id.ToString()
                    });
                }
            }
            else
            {
                var store = _storeService.GetStoreById(_workContext.CurrentCustomer.StoreId);
                model.AvailableStores.Add(new SelectListItem
                {
                    Text = store.Name,
                    Value = store.Id.ToString()
                });
            }


            return View(model);
        }
        [HttpPost]
        public ActionResult List(DataSourceRequest command, GiftListModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageDiscounts))
                return AccessDeniedView();

            var campaigns = _campaignService.GetAllgifts(searchModel.StoreId);
            int xy = 4;
            var gridModel = new DataSourceResult
            {
                Data = campaigns.Select(x =>
                {
                    var model = x.ToModel();
                    //model.CreatedOnUtc = _DateTimeHelper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc);
                   
                    return model;

                }),
                Total = campaigns.Count
            };
            return Json(gridModel);
        }

        public ActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageDiscounts))
                return AccessDeniedView();
            int storeScope;
            Store currentStore;
            if (_workContext.CurrentCustomer.IsAdmin())
            {
                storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
                //currentStore = _storeService.GetStoreById(campaign.StoreId);
                // currentStore = _storeService.GetStoreById(storeScope);
            }
            else { storeScope = (_workContext.CurrentCustomer.StoreId); }
            if (storeScope == 0) storeScope = 1;
            currentStore = _storeService.GetStoreById(storeScope);
            var model = new GiftModel();

            //stores
            model.StoreId = storeScope;

            //customer roles

            PrepareCustomCustomerAttributesForGift(model);
            model.gift1Enabled = currentStore.gift1;
            model.gift1Name = currentStore.gift1Name;
            model.gift2Enabled = currentStore.gift2;
            model.gift2Name = currentStore.gift2Name;
            model.gift3Enabled = currentStore.gift3;
            model.gift3Name = currentStore.gift3Name;
            model.gift4Enabled = currentStore.gift4;
            model.gift4Name = currentStore.gift4Name;
            model.isPoints = currentStore.isPoints;


            return View(model);
        }
        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create(GiftModel model, FormCollection form, bool continueEditing)
        {
            IList<CustomerAttribute> caForForm = _customerAttributeService.GetAllCustomerAttributesByStoreID(_workContext.CurrentCustomer.StoreId);
            string[] drops = new string[2];
            string[] strings = new string[2];
            DateTime[] dates = new DateTime[2];
            int cdrops = 0;
            int cstrings = 0;
            //int cdates = 0;
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageDiscounts))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                Gift gift = model.ToEntity();

                gift.CreatedOnUtc = DateTime.Now;
                //campaign.CreatedOnUtcTo = DateTime.MinValue;
                //campaign.Date2 = DateTime.MinValue;
                //campaign.Date2To = DateTime.MinValue;
                //campaign.Date3 = DateTime.MinValue;
                //campaign.Date3To = DateTime.MinValue;
                //campaign.DateOfBirth = DateTime.MinValue;
                //campaign.DontSendBeforeDateUtc = DateTime.MinValue;



                gift.DateOfBirth = model.DateOfBirth.HasValue ?
                  (DateTime?)model.DateOfBirth.Value : null;
                gift.DateOfBirthTo = model.DateOfBirthTo.HasValue ?
                 (DateTime?)model.DateOfBirthTo.Value : null;
                gift.Date2 = model.Date2.HasValue ?
                  (DateTime?)model.Date2.Value : null;
                gift.Date2To = model.Date2To.HasValue ?
                  (DateTime?)model.Date2To.Value : null;
                gift.Date3 = model.Date3.HasValue ?
                 (DateTime?)model.Date3.Value : null;
                gift.Date3To = model.Date3To.HasValue ?
                 (DateTime?)model.Date3To.Value : null;
                /////////////////////////////////////////////////////////////////////////////////


                for (int i = 0; i < caForForm.Count; i++)
                {
                    if (caForForm[i].DisplayOrder > 100) continue;
                    string ccc;

                    string s1 = string.Format("customer_attribute_{0}", caForForm[i].Id);

                    ValueProviderResult x;
                    string y;
                    if (caForForm[i].AttributeControlTypeId == 20) break;
                    if (caForForm[i].AttributeControlTypeId == 1)
                    {
                        x = form.GetValue(s1);
                        y = x.AttemptedValue;
                        if (y == "0") ccc = null;
                        else
                        {
                            ccc = _customerAttributeService.GetCustomerAttributeValueById(Int32.Parse(form.GetValue(s1).AttemptedValue.ToString())).Name;
                        }
                    }
                    else { ccc = form.GetValue(s1).AttemptedValue.ToString(); }
                    if (_customerAttributeService.GetCustomerAttributeById(caForForm[i].Id).AttributeControlTypeId == 1) { drops[cdrops] = ccc; cdrops++; }
                    if (_customerAttributeService.GetCustomerAttributeById(caForForm[i].Id).AttributeControlTypeId == 4) { strings[cstrings] = ccc; cstrings++; }

                    if (i >= 6) break;
                }
                cdrops = 0;
                cstrings = 0;
                //cdates = 0;
                //if(_customerSettings.StreetAddressEnabled)
                gift.StreetAddress = drops[0];
                //if (_customerSettings.StreetAddress2Enabled)
                gift.StreetAddress2 = drops[1];
                //if (_customerSettings.CityEnabled)
                gift.city = strings[0];
                //if (_customerSettings.CompanyEnabled)
                gift.company = strings[1];

                gift.Gender = model.Gender;
                gift.origin = model.origin;
                gift.gift = model.gift;
                gift.EndDate = model.EndDate;

                var subscriptions = _newsLetterSubscriptionService.GetNewsLetterSubscriptions(
                   storeId: gift.StoreId,
                  // customerRoleId: gift.CustomerRoleId,
                   Gender: gift.Gender,

                   company: gift.company,
                   city: gift.city,
                   StreetAddress: gift.StreetAddress,
                   StreetAddress2: gift.StreetAddress2,
                   origin: gift.origin,
                   minPoints: gift.MaxPoints,
                   maxPoints: gift.MinPoints,

                   createdFromUtc: gift.CreatedOnUtc,
                   createdToUtc: gift.CreatedOnUtcTo,
                   DateOfBirthFrom: gift.DateOfBirth,
                   DateOfBirthTo: gift.DateOfBirthTo,
                   date2From: gift.Date2,
                   date2To: gift.Date2To,
                   date3From: gift.Date3,
                   date3To: gift.Date3To);

                Dictionary<string, string> phonesList = new Dictionary<string, string>();
                int i2 = 0;
                
                foreach (var nlsub in subscriptions)
                {

                    if (gift.gift == "1")
                    {
                        nlsub.gift1 = bool.Parse(gift.giftVal);
                        nlsub.gift1EndDate = gift.EndDate;
                    }
                    if (gift.gift == "2")
                    {
                        nlsub.gift2 = bool.Parse(gift.giftVal);
                        nlsub.gift2EndDate = gift.EndDate;
                    }
                    if (gift.gift == "3")
                    {
                        nlsub.gift3 = bool.Parse(gift.giftVal);
                        nlsub.gift3EndDate = gift.EndDate;
                    }
                    if (gift.gift == "4")
                    {
                        nlsub.gift4 = bool.Parse(gift.giftVal);
                        nlsub.gift4EndDate = gift.EndDate;
                    }
                    _newsLetterSubscriptionService.UpdateNewsLetterSubscription(nlsub);
                   
                }
                gift.CustomerCount = subscriptions.Count;



                //if(_customerSettings)



                int storeScope2;
                /////////////////////////////////////////////////////////////////
                if (_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsVendor())
                    storeScope2 = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
                else
                {
                    storeScope2 = _workContext.CurrentCustomer.StoreId;
                }
                gift.StoreId = storeScope2;
                _campaignService.InsertGift(gift);

                SuccessNotification(_localizationService.GetResource("Admin.Promotions.Campaigns.Added"));
                return continueEditing ? RedirectToAction("Edit", new { id = gift.Id }) : RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form

            return View(model);
        }



     




    }

}