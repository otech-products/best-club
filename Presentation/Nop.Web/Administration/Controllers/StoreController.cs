﻿using System;
using System.Linq;
using System.Web.Mvc;
using Nop.Admin.Extensions;
using Nop.Admin.Models.Stores;
using Nop.Core.Domain.Stores;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Services.Messages;
using Nop.Admin.Models.Settings;

namespace Nop.Admin.Controllers
{
    public partial class StoreController : BaseAdminController
    {
        #region Fields

        private readonly IStoreService _storeService;
        private readonly ISettingService _settingService;
        private readonly ILanguageService _languageService;
        private readonly ILocalizationService _localizationService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly IPermissionService _permissionService;
        private readonly IEmailSender _emailSender;
        #endregion

        #region Constructors

        public StoreController(IStoreService storeService,
            ISettingService settingService,
            ILanguageService languageService,
            ILocalizationService localizationService,
            ILocalizedEntityService localizedEntityService,
            IPermissionService permissionService,
            IEmailSender emailSender)
        {
            this._storeService = storeService;
            this._settingService = settingService;
            this._languageService = languageService;
            this._localizationService = localizationService;
            this._localizedEntityService = localizedEntityService;
            this._permissionService = permissionService;
            this._emailSender = emailSender;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected virtual void PrepareLanguagesModel(StoreModel model)
        {
            if (model == null)
                throw new ArgumentNullException("model");
            
            model.AvailableLanguages.Add(new SelectListItem
            {
                Text = "---",
                Value = "0"
            });
            var languages = _languageService.GetAllLanguages(true);
            foreach (var language in languages)
            {
                model.AvailableLanguages.Add(new SelectListItem
                {
                    Text = language.Name,
                    Value = language.Id.ToString()
                });
            }
        }

        [NonAction]
        protected virtual void UpdateAttributeLocales(Store store, StoreModel model)
        {
            foreach (var localized in model.Locales)
            {
                _localizedEntityService.SaveLocalizedValue(store,
                    x => x.Name,
                    localized.Name,
                    localized.LanguageId);
            }
        }

        #endregion

        #region Methods

        public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageStores))
                return AccessDeniedView();

            return View();
        }

        [HttpPost]
        public ActionResult List(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageStores))
                return AccessDeniedView();

            var storeModels = _storeService.GetAllStores()
                .Select(x => x.ToModel())
                .ToList();

            var gridModel = new DataSourceResult
            {
                Data = storeModels,
                Total = storeModels.Count()
            };

            return Json(gridModel);
        }

        public ActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageStores))
                return AccessDeniedView();
            var model = new StoreModel();
            try
            {
                var df = _storeService.GetStoreById(1);
                
                //languages
                PrepareLanguagesModel(model);
                //locales
                AddLocales(_languageService, model.Locales);
                if (df != null)
                {
                    model.rgb = df.rgb;
                    model.welcomeMsg = df.welcomeMsg;
                    model.BdaySmS = df.BdaySmS;
                    model.contactMsg = df.contactMsg;
                    model.contactMsgMonth = df.contactMsgMonth;
                    model.gift1Msg = df.gift1Msg;
                    model.gift1Name = df.gift1Name;
                    model.gift1OnBday = df.gift1OnBday;
                    model.isGifts = df.isGifts;
                    model.MsgLastVisit = df.MsgLastVisit;
                    model.periodicMsg1 = df.periodicMsg1;
                    model.periodicMsg2 = df.periodicMsg2;
                    model.periodicMsg3 = df.periodicMsg3;
                    model.periodicMsg4 = df.periodicMsg4;
                    model.preFirstNameMsg = df.preFirstNameMsg;
                    model.rutineSmSTimeOffset = df.rutineSmSTimeOffset;
                    model.pointsMsg = df.pointsMsg;
                    model.pointsMsg2 = df.pointsMsg2;
                    model.days2GiftEndMsg = df.days2GiftEndMsg;
                    model.Active = true;
                    return View(model);
                }
            }
            catch
            {
               
            }
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create(StoreModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageStores))
                return AccessDeniedView();
            
          
               
                var store = model.ToEntity();
            store.Url = model.Name;
            store.SslEnabled = false;
                store.weeklyCustomerCountInt = 0;
                store.totalCustomersPoints = 0;
                
                //ensure we have "/" at the end
                //if (!store.Url.EndsWith("/"))
                //    store.Url += "/";
                _storeService.InsertStore(store);
                //locales
                UpdateAttributeLocales(store, model);

                SuccessNotification(_localizationService.GetResource("Admin.Configuration.Stores.Added"));
                return continueEditing ? RedirectToAction("Edit", new { id = store.Id }) : RedirectToAction("List");
            

            //If we got this far, something failed, redisplay form

            //languages
            PrepareLanguagesModel(model);
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageStores))
                return AccessDeniedView();

            var store = _storeService.GetStoreById(id);
            if (store == null)
                //No store found with the specified id
                return RedirectToAction("List");

            var model = store.ToModel();
            //languages
            PrepareLanguagesModel(model);
            //locales
            AddLocales(_languageService, model.Locales, (locale, languageId) =>
            {
                locale.Name = store.GetLocalized(x => x.Name, languageId, false, false);
            });
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        [FormValueRequired("save", "save-continue")]
        public ActionResult Edit(StoreModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageStores))
                return AccessDeniedView();

            var store = _storeService.GetStoreById(model.Id);
            if (store == null)
            {  //No store found with the specified id
                return RedirectToAction("List");
            }
            store.Url = model.Name;
            store.SslEnabled = false;

            store = model.ToEntity(store);
                //ensure we have "/" at the end
                //if (!store.Url.EndsWith("/"))
                   store.Url += "/";
                _storeService.UpdateStore(store);
                //locales
                UpdateAttributeLocales(store, model);

                SuccessNotification(_localizationService.GetResource("Admin.Configuration.Stores.Updated"));
                return continueEditing ? RedirectToAction("Edit", new { id = store.Id }) : RedirectToAction("List");
            

            //If we got this far, something failed, redisplay form

            //languages
            PrepareLanguagesModel(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageStores))
                return AccessDeniedView();

            var store = _storeService.GetStoreById(id);
            if (store == null)
                //No store found with the specified id
                return RedirectToAction("List");

            try
            {
                _storeService.DeleteStore(store);

                //when we delete a store we should also ensure that all "per store" settings will also be deleted
                var settingsToDelete = _settingService
                    .GetAllSettings()
                    .Where(s => s.StoreId == id)
                    .ToList();
                    _settingService.DeleteSettings(settingsToDelete);
                //when we had two stores and now have only one store, we also should delete all "per store" settings
                var allStores = _storeService.GetAllStores();
                if (allStores.Count == 1)
                {
                    settingsToDelete = _settingService
                        .GetAllSettings()
                        .Where(s => s.StoreId == allStores[0].Id)
                        .ToList();
                        _settingService.DeleteSettings(settingsToDelete);
                }


                SuccessNotification(_localizationService.GetResource("Admin.Configuration.Stores.Deleted"));
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Edit", new {id = store.Id});
            }
        }


        public ActionResult SendTestEmail(string email, string body,int id, string withName=null, string subject = null)
        {
            ;
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
                return AccessDeniedView();
            
           
            var currentStore = _storeService.GetStoreById(id);


            //if (Boolean.Parse(withName))
            //{
            //    b1 = currentStore.preFirstNameMsg + " " + "לקוח" + "\n";

            //    body = b1 + body;
            //}

            string msg = body;
            msg = msg.Replace("$N$", "גיל");
            msg= msg.Replace("$U$", "100");
            msg = msg.Replace("$D$", "50");
            msg = msg.Replace("$P$", "700");
            msg = msg.Replace("$DAYS$", "10");
            msg = msg.Replace("$GIFT$", "מתנת יום הולדת");






            _emailSender.SendSmS(email, msg, currentStore.SmsUserName, currentStore.SmsPass, "", currentStore.preMsg, currentStore.smsRemovalLink);
            return Json(new
            {
                redirect = ""
            });
        }

        #endregion






        //public ActionResult GeneralCommon()
        //{
        //    if (!_permissionService.Authorize(StandardPermissionProvider.ManageSettings))
        //        return AccessDeniedView();

        //    //set page timeout to 5 minutes
        //    this.Server.ScriptTimeout = 300;

        //    var model = new GeneralCommonSettingsModel();
        //    var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
        //    model.ActiveStoreScopeConfiguration = storeScope;
        //    //store information
        //    var storeInformationSettings = _settingService.LoadSetting<StoreInformationSettings>(storeScope);
        //    var commonSettings = _settingService.LoadSetting<CommonSettings>(storeScope);
        //    model.StoreInformationSettings.StoreClosed = storeInformationSettings.StoreClosed;
        //    //themes
        //    model.StoreInformationSettings.DefaultStoreTheme = storeInformationSettings.DefaultStoreTheme;
        //    model.StoreInformationSettings.AvailableStoreThemes = _themeProvider
        //        .GetThemeConfigurations()
        //        .Select(x => new GeneralCommonSettingsModel.StoreInformationSettingsModel.ThemeConfigurationModel
        //        {
        //            ThemeTitle = x.ThemeTitle,
        //            ThemeName = x.ThemeName,
        //            PreviewImageUrl = x.PreviewImageUrl,
        //            PreviewText = x.PreviewText,
        //            SupportRtl = x.SupportRtl,
        //            Selected = x.ThemeName.Equals(storeInformationSettings.DefaultStoreTheme, StringComparison.InvariantCultureIgnoreCase)
        //        })
        //        .ToList();
        //    model.StoreInformationSettings.AllowCustomerToSelectTheme = storeInformationSettings.AllowCustomerToSelectTheme;
        //    model.StoreInformationSettings.LogoPictureId = storeInformationSettings.LogoPictureId;
        //    //EU Cookie law
          


        //    return View(model);
        //}
        //[HttpPost]
        //[FormValueRequired("save")]
        //public ActionResult GeneralCommon(GeneralCommonSettingsModel model)
        //{
        //    if (!_permissionService.Authorize(StandardPermissionProvider.ManageSettings))
        //        return AccessDeniedView();


        //    //load settings for a chosen store scope
        //    var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
        //    var currentStore = _storeService.GetStoreById(storeScope);
        //    currentStore.LogoPictureId = model.StoreInformationSettings.LogoPictureId;
        //    currentStore.StoreClosed = model.StoreInformationSettings.StoreClosed;
        //    currentStore.DefaultStoreTheme = model.StoreInformationSettings.DefaultStoreTheme;
        //    _storeService.UpdateStore(currentStore);





        //    //store information settings
        //    var storeInformationSettings = _settingService.LoadSetting<StoreInformationSettings>(storeScope);
        //    var commonSettings = _settingService.LoadSetting<CommonSettings>(storeScope);
        //    storeInformationSettings.StoreClosed = model.StoreInformationSettings.StoreClosed;
        //    storeInformationSettings.DefaultStoreTheme = model.StoreInformationSettings.DefaultStoreTheme;
        //    storeInformationSettings.AllowCustomerToSelectTheme = model.StoreInformationSettings.AllowCustomerToSelectTheme;
        //    storeInformationSettings.LogoPictureId = model.StoreInformationSettings.LogoPictureId;

        //    //EU Cookie law
          

        //    return RedirectToAction("GeneralCommon");
        //}

    }
}
