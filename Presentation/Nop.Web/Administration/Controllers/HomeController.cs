﻿using System;
using System.Linq;
using System.Net;
using System.ServiceModel.Syndication;
using System.Web.Mvc;
using System.Xml;
using Nop.Admin.Infrastructure.Cache;
using Nop.Admin.Models.Home;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Core.Domain.Stores;

namespace Nop.Admin.Controllers
{
    public partial class HomeController : BaseAdminController
    {
        #region Fields
        private readonly IStoreContext _storeContext;
        private readonly AdminAreaSettings _adminAreaSettings;
        private readonly ISettingService _settingService;
        private readonly IPermissionService _permissionService;
        private readonly IProductService _productService;
        private readonly IOrderService _orderService;
        private readonly ICustomerService _customerService;
        private readonly IReturnRequestService _returnRequestService;
        private readonly IWorkContext _workContext;
        private readonly ICacheManager _cacheManager;
        private readonly IStoreService _storeService;
        private readonly IRewardPointService _rewardPointService;

        #endregion

        #region Ctor

        public HomeController(IStoreContext storeContext,
            AdminAreaSettings adminAreaSettings, 
            ISettingService settingService,
            IPermissionService permissionService,
            IProductService productService,
            IOrderService orderService,
            ICustomerService customerService,
            IReturnRequestService returnRequestService,
            IWorkContext workContext,
            IStoreService storeService,
            ICacheManager cacheManager,
            IRewardPointService rewardPointService)
        {
            this._storeContext = storeContext;
            this._adminAreaSettings = adminAreaSettings;
            this._settingService = settingService;
            this._permissionService = permissionService;
           
          
            this._customerService = customerService;
            
            this._workContext = workContext;
            this._cacheManager = cacheManager;
            this._storeService = storeService;
            this._rewardPointService = rewardPointService;
        }

        #endregion

        #region Methods

        public ActionResult Index()
        {




            Store currentStore = _storeService.GetStoreById(_workContext.CurrentCustomer.StoreId);
            //DateTime DateTime.Now.Month
            //int monthlyVisits = _rewardPointService.GetVisits(storeId:currentStore.Id);
            var model = new CommonStatisticsModel();

            //model.NumberOfCustomers = currentStore.NumberOfCustomers;
            //model.gift1 = currentStore.gift1;
            //model.gift1Imps = currentStore.gift1Imps;
            //model.gift1Name = currentStore.gift1Name;
            //model.gift2 = currentStore.gift2;
            //model.gift2Imps = currentStore.gift2Imps;
            //model.gift2Name = currentStore.gift2Name;
            //model.internet2Ez = currentStore.internet2Ez;
            //model.monthlyCustomerCount = currentStore.monthlyCustomerCount;
            //model.monthlyVisits = currentStore.monthlyVisits;
            //model.visits = currentStore.visits;
            //model.weeklyVisits = currentStore.weeklyVisits;
            //model.totalCustomersPoints = currentStore.totalCustomersPoints;
            //model.pointImpsSum = currentStore.pointImpsSum;
            //model.pointImpsVisits = currentStore.pointImpsVisits;
            //model.weeklyCustomerCountInt = currentStore.weeklyCustomerCountInt.ToString();
            //model.BdayMsgSentInLastRutine = currentStore.BdayMsgSentInLastRutine;
            //model.SmsOnBday = currentStore.SmsOnBday;
            //model.gift1Imps = currentStore.gift1Imps;
            //model.pointImpsSum = currentStore.pointImpsSum;
            //model.pointImpsVisits = currentStore.pointImpsVisits;
            //model.totalCustomersPoints = currentStore.totalCustomersPoints;
            //model.isPoints = currentStore.isPoints;
            //model.isFacebook = currentStore.isFacebook;




            return View(model);
        }



        [ChildActionOnly]
        public ActionResult CommonStatistics()
        {
           

            //a vendor doesn't have access to this report
            if (_workContext.CurrentVendor != null)
                return Content("");


            var model = new CommonStatisticsModel();
            Store currentStore = _storeService.GetStoreById(_workContext.CurrentCustomer.StoreId);
            model.NumberOfCustomers = currentStore.NumberOfCustomers;
            model.gift1 = currentStore.gift1;
            model.gift1Imps = currentStore.gift1Imps;
            model.gift1Name = currentStore.gift1Name;
            model.gift2 = currentStore.gift2;
            model.gift2Imps = currentStore.gift2Imps;
            model.gift2Name = currentStore.gift2Name;
            model.internet2Ez = currentStore.internet2Ez;
            model.monthlyCustomerCount = currentStore.monthlyCustomerCount;
            model.monthlyVisits = currentStore.monthlyVisits;
            model.visits = currentStore.visits;
            model.weeklyVisits = currentStore.weeklyVisits;
            model.totalCustomersPoints = currentStore.totalCustomersPoints;
            model.pointImpsSum = currentStore.pointImpsSum;
            model.pointImpsVisits = currentStore.pointImpsVisits;
            model.weeklyCustomerCountInt = currentStore.weeklyCustomerCountInt.ToString();
            model.BdayMsgSentInLastRutine = currentStore.BdayMsgSentInLastRutine;
            model.SmsOnBday = currentStore.SmsOnBday;
            model.gift1Imps = currentStore.gift1Imps;
            model.pointImpsSum = currentStore.pointImpsSum;
            model.pointImpsVisits = currentStore.pointImpsVisits;
            model.totalCustomersPoints = currentStore.totalCustomersPoints;
            model.isPoints = currentStore.isPoints;

             return PartialView(model);
             }

            #endregion
        }
}
