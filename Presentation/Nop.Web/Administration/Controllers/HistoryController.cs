﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Nop.Admin.Extensions;
using Nop.Admin.Models.Messages;
using Nop.Core;
using Nop.Core.Domain.Messages;
using Nop.Services.Customers;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Admin.Models.Settings;
using Nop.Core.Domain.Customers;
using Nop.Admin.Models.Stores;
using Nop.Services.Common;
using Nop.Services.Orders;
using Nop.Core.Domain.Catalog;
using Nop.Web.Models.Customer;
using Nop.Core.Domain.Stores;

namespace Nop.Admin.Controllers
{
    public class HistoryController : BaseAdminController
    {

        private readonly IStoreContext _storeContext;
        private readonly IStoreService _storeService;
        private readonly IWorkContext _workContext;
        private readonly IRewardPointService _rewardPointService;
        private readonly IPermissionService _permissionService;
        private readonly IDateTimeHelper _dateTimeHelper;
        public HistoryController(IDateTimeHelper DateTimeHelper, IStoreService storeService, IStoreContext storeContext, IWorkContext workContext,
              IRewardPointService rewardPointService, IPermissionService permissionService)
        {
            this._storeContext = storeContext;
            this._storeService = storeService;
            this._workContext = workContext;
            this._rewardPointService = rewardPointService;
            this._permissionService = permissionService;
            this._dateTimeHelper = DateTimeHelper;

        }
        // GET: History
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
                return AccessDeniedView();
            var model = new RewardPointListModel();
            if (_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator())
            {
                var stores = _storeService.GetAllStores();




                foreach (var store in stores)
                {
                    model.AvailableStores.Add(new SelectListItem
                    {
                        Text = store.Name,
                        Value = store.Id.ToString()
                    });
                }
            }
            else
            {
                var store = _storeService.GetStoreById(_workContext.CurrentCustomer.StoreId);
                model.AvailableStores.Add(new SelectListItem
                {
                    Text = store.Name,
                    Value = store.Id.ToString()
                });
            }


            return View(model);
        }

        [HttpPost]
        public ActionResult List(DataSourceRequest command, RewardPointListModel searchModel)
        {
            IPagedList<RewardPointsHistory> entries;

           
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
                return AccessDeniedView();
            if (_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsForumModerator())
            {
                entries = _rewardPointService.GetRewardPointsHistoryByStoreId(searchModel.StoreId);
            }
            
            else
            {
                entries = _rewardPointService.GetRewardPointsHistoryByParamsAndStoreId(_workContext.CurrentCustomer.StoreId,searchModel.Employee,searchModel.Message,searchModel.SearchEmail,searchModel.StartDate,searchModel.EndDate);
            }


            
            var gridModel = new DataSourceResult
            {
                Data = entries.Select(x =>
                {
                    var m = new RewardPointModel();
                    m.CreatedOnUtc = x.CreatedOnUtc;
                    m.dicPoints = x.dicPoints;
                    m.Employee = x.Employee;
                    m.GiftName = x.GiftName;
                    m.incPoints = x.incPoints;
                    m.Message = x.Message;
                    m.FirstName = x.FirstName;
                    m.LastName = x.LastName;
                    m.Email = x.Email;
                    m.UsedAmount = x.UsedAmount;
                    
                    




                    return m;

                }).OrderByDescending(m => m.CreatedOnUtc),
                Total = entries.Count
            };
            return Json(gridModel);
        }


        [HttpPost]
        public ActionResult searchList(DataSourceRequest command, RewardPointListModel searchModel)
        {
            IPagedList<RewardPointsHistory> entries;


            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
                return AccessDeniedView();

            if (_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsVendor())
            {
               
                entries = _rewardPointService.GetRewardPointsHistoryByStoreId(searchModel.StoreId);
            }

            else
            {


                entries = _rewardPointService.GetRewardPointsHistoryByParamsAndStoreId(storeId: searchModel.StoreId, Email: searchModel.SearchEmail, Employee: searchModel.Employee,Message: searchModel.Message,createdOnUtc:searchModel.StartDate,createdToUtc:searchModel.EndDate);
                //if (!string.IsNullOrWhiteSpace(searchModel.SearchEmail))
                //{
               // _rewardPointService.GetRewardPointsHistoryByParamsAndStoreId(storeId: searchModel.StoreId, Email: searchModel.SearchEmail,Employee:searchModel.Employee);
               // }
               
               // entries = _rewardPointService.GetRewardPointsHistoryByStoreId(_workContext.CurrentCustomer.StoreId);
            }

            




            var gridModel = new DataSourceResult
            {
                Data = entries.Select(x =>
                {
                    var m = new RewardPointModel();
                    m.CreatedOnUtc = x.CreatedOnUtc;
                    m.dicPoints = x.dicPoints;
                    m.Employee = x.Employee;
                    m.GiftName = x.GiftName;
                    m.incPoints = x.incPoints;
                    m.Message = x.Message;
                    m.FirstName = x.FirstName;
                    m.LastName = x.LastName;
                    m.Email = x.Email;
                    m.UsedAmount = x.UsedAmount;




                    return m;

                }).OrderBy(m => m.CreatedOnUtc),
                Total = entries.Count
            };
            return Json(gridModel);
        }



        [ChildActionOnly]
        public virtual ActionResult NlsStatistics()
        {


            return PartialView();
        }

        [ChildActionOnly]
        public virtual ActionResult SumsStatistics()
        {


            return PartialView();
        }

        [ChildActionOnly]
        public virtual ActionResult DicsStatistics()
        {


            return PartialView();
        }


        [ChildActionOnly]
        public virtual ActionResult VisitsStatistics()
        {


            return PartialView();
        }


        [ChildActionOnly]
        public virtual ActionResult GiftsStatistics()
        {


            return PartialView();
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public virtual ActionResult LoadSums(string period)
        {


            int storeId = _workContext.CurrentCustomer.StoreId;
            var result = new List<object>();

            var nowDt = _dateTimeHelper.ConvertToUserTime(DateTime.Now);
            var timeZone = _dateTimeHelper.CurrentTimeZone;
            //var searchCustomerRoleIds = new[] { _customerService.GetCustomerRoleBySystemName(SystemCustomerRoleNames.Registered).Id };

            var culture = new System.Globalization.CultureInfo(_workContext.WorkingLanguage.LanguageCulture);


            switch (period)
            {
                case "year":
                    //year statistics
                    var yearAgoDt = nowDt.AddYears(-1).AddMonths(1);
                    var searchYearDateUser = new DateTime(yearAgoDt.Year, yearAgoDt.Month, 1);
                    if (!timeZone.IsInvalidTime(searchYearDateUser))
                    {
                        for (int i = 0; i <= 12; i++)
                        {
                            result.Add(new
                            {
                                date = searchYearDateUser.Date.ToString("Y", culture),
                                value = _rewardPointService.GetSums (
                                    createdFromUtc: _dateTimeHelper.ConvertToUtcTime(searchYearDateUser, timeZone),
                                    createdToUtc: _dateTimeHelper.ConvertToUtcTime(searchYearDateUser.AddMonths(1), timeZone),
                                    storeId: storeId,
                                    pageIndex: 0,
                                    pageSize: 1).ToString()
                            });

                            searchYearDateUser = searchYearDateUser.AddMonths(1);
                        }
                    }
                    break;

                case "month":
                    //month statistics
                    var monthAgoDt = nowDt.AddDays(-30);
                    var searchMonthDateUser = new DateTime(monthAgoDt.Year, monthAgoDt.Month, monthAgoDt.Day);
                    if (!timeZone.IsInvalidTime(searchMonthDateUser))
                    {
                        for (int i = 0; i <= 30; i++)
                        {
                            result.Add(new
                            {
                                date = searchMonthDateUser.Date.ToString("M", culture),
                                value = _rewardPointService.GetSums(
                                    createdFromUtc: _dateTimeHelper.ConvertToUtcTime(searchMonthDateUser, timeZone),
                                    createdToUtc: _dateTimeHelper.ConvertToUtcTime(searchMonthDateUser.AddDays(1), timeZone),
                                    storeId: storeId,
                                  
                                    pageIndex: 0,
                                    pageSize: 1).ToString()
                            });

                            searchMonthDateUser = searchMonthDateUser.AddDays(1);
                        }
                    }
                    break;

                case "week":
                default:
                    //week statistics
                    var weekAgoDt = nowDt.AddDays(-7);
                    var searchWeekDateUser = new DateTime(weekAgoDt.Year, weekAgoDt.Month, weekAgoDt.Day);
                    if (!timeZone.IsInvalidTime(searchWeekDateUser))
                    {
                        for (int i = 0; i <= 7; i++)
                        {
                            result.Add(new
                            {
                                date = searchWeekDateUser.Date.ToString("d dddd", culture),
                                value = _rewardPointService.GetSums(
                                    createdFromUtc: _dateTimeHelper.ConvertToUtcTime(searchWeekDateUser, timeZone),
                                    createdToUtc: _dateTimeHelper.ConvertToUtcTime(searchWeekDateUser.AddDays(1), timeZone),
                                    storeId: storeId,
                                 
                                    pageIndex: 0,
                                    pageSize: 1).ToString()
                            });

                            searchWeekDateUser = searchWeekDateUser.AddDays(1);
                        }
                    }
                    break;
            }



            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public virtual ActionResult LoadVisits(string period)
        {


            int storeId = _workContext.CurrentCustomer.StoreId;
            var result = new List<object>();

            var nowDt = _dateTimeHelper.ConvertToUserTime(DateTime.Now);
            var timeZone = _dateTimeHelper.CurrentTimeZone;
            //var searchCustomerRoleIds = new[] { _customerService.GetCustomerRoleBySystemName(SystemCustomerRoleNames.Registered).Id };

            var culture = new System.Globalization.CultureInfo(_workContext.WorkingLanguage.LanguageCulture);


            switch (period)
            {
                case "year":
                    //year statistics
                    var yearAgoDt = nowDt.AddYears(-1).AddMonths(1);
                    var searchYearDateUser = new DateTime(yearAgoDt.Year, yearAgoDt.Month, 1);
                    if (!timeZone.IsInvalidTime(searchYearDateUser))
                    {
                        for (int i = 0; i <= 12; i++)
                        {
                            result.Add(new
                            {
                                date = searchYearDateUser.Date.ToString("Y", culture),
                                value = _rewardPointService.GetVisits (
                                    createdFromUtc: _dateTimeHelper.ConvertToUtcTime(searchYearDateUser, timeZone),
                                    createdToUtc: _dateTimeHelper.ConvertToUtcTime(searchYearDateUser.AddMonths(1), timeZone),
                                    storeId: storeId,
                                    pageIndex: 0,
                                    pageSize: 1).ToString()
                            });

                            searchYearDateUser = searchYearDateUser.AddMonths(1);
                        }
                    }
                    break;

                case "month":
                    //month statistics
                    var monthAgoDt = nowDt.AddDays(-30);
                    var searchMonthDateUser = new DateTime(monthAgoDt.Year, monthAgoDt.Month, monthAgoDt.Day);
                    if (!timeZone.IsInvalidTime(searchMonthDateUser))
                    {
                        for (int i = 0; i <= 30; i++)
                        {
                            result.Add(new
                            {
                                date = searchMonthDateUser.Date.ToString("M", culture),
                                value = _rewardPointService.GetVisits(
                                    createdFromUtc: _dateTimeHelper.ConvertToUtcTime(searchMonthDateUser, timeZone),
                                    createdToUtc: _dateTimeHelper.ConvertToUtcTime(searchMonthDateUser.AddDays(1), timeZone),
                                    storeId: storeId,

                                    pageIndex: 0,
                                    pageSize: 1).ToString()
                            });

                            searchMonthDateUser = searchMonthDateUser.AddDays(1);
                        }
                    }
                    break;

                case "week":
                default:
                    //week statistics
                    var weekAgoDt = nowDt.AddDays(-7);
                    var searchWeekDateUser = new DateTime(weekAgoDt.Year, weekAgoDt.Month, weekAgoDt.Day);
                    if (!timeZone.IsInvalidTime(searchWeekDateUser))
                    {
                        for (int i = 0; i <= 7; i++)
                        {
                            result.Add(new
                            {
                                date = searchWeekDateUser.Date.ToString("d dddd", culture),
                                value = _rewardPointService.GetVisits(
                                    createdFromUtc: _dateTimeHelper.ConvertToUtcTime(searchWeekDateUser, timeZone),
                                    createdToUtc: _dateTimeHelper.ConvertToUtcTime(searchWeekDateUser.AddDays(1), timeZone),
                                    storeId: storeId,

                                    pageIndex: 0,
                                    pageSize: 1).ToString()
                            });

                            searchWeekDateUser = searchWeekDateUser.AddDays(1);
                        }
                    }
                    break;
            }



            return Json(result, JsonRequestBehavior.AllowGet);
        }




        [AcceptVerbs(HttpVerbs.Get)]
        public virtual ActionResult LoadDics(string period)
        {


            int storeId = _workContext.CurrentCustomer.StoreId;
            var result = new List<object>();

            var nowDt = _dateTimeHelper.ConvertToUserTime(DateTime.Now);
            var timeZone = _dateTimeHelper.CurrentTimeZone;
            //var searchCustomerRoleIds = new[] { _customerService.GetCustomerRoleBySystemName(SystemCustomerRoleNames.Registered).Id };

            var culture = new System.Globalization.CultureInfo(_workContext.WorkingLanguage.LanguageCulture);


            switch (period)
            {
                case "year":
                    //year statistics
                    var yearAgoDt = nowDt.AddYears(-1).AddMonths(1);
                    var searchYearDateUser = new DateTime(yearAgoDt.Year, yearAgoDt.Month, 1);
                    if (!timeZone.IsInvalidTime(searchYearDateUser))
                    {
                        for (int i = 0; i <= 12; i++)
                        {
                            result.Add(new
                            {
                                date = searchYearDateUser.Date.ToString("Y", culture),
                                value = _rewardPointService.GetDics (
                                    createdFromUtc: _dateTimeHelper.ConvertToUtcTime(searchYearDateUser, timeZone),
                                    createdToUtc: _dateTimeHelper.ConvertToUtcTime(searchYearDateUser.AddMonths(1), timeZone),
                                    storeId: storeId,
                                    pageIndex: 0,
                                    pageSize: 1).ToString()
                            });

                            searchYearDateUser = searchYearDateUser.AddMonths(1);
                        }
                    }
                    break;

                case "month":
                    //month statistics
                    var monthAgoDt = nowDt.AddDays(-30);
                    var searchMonthDateUser = new DateTime(monthAgoDt.Year, monthAgoDt.Month, monthAgoDt.Day);
                    if (!timeZone.IsInvalidTime(searchMonthDateUser))
                    {
                        for (int i = 0; i <= 30; i++)
                        {
                            result.Add(new
                            {
                                date = searchMonthDateUser.Date.ToString("M", culture),
                                value = _rewardPointService.GetDics(
                                    createdFromUtc: _dateTimeHelper.ConvertToUtcTime(searchMonthDateUser, timeZone),
                                    createdToUtc: _dateTimeHelper.ConvertToUtcTime(searchMonthDateUser.AddDays(1), timeZone),
                                    storeId: storeId,

                                    pageIndex: 0,
                                    pageSize: 1).ToString()
                            });

                            searchMonthDateUser = searchMonthDateUser.AddDays(1);
                        }
                    }
                    break;

                case "week":
                default:
                    //week statistics
                    var weekAgoDt = nowDt.AddDays(-7);
                    var searchWeekDateUser = new DateTime(weekAgoDt.Year, weekAgoDt.Month, weekAgoDt.Day);
                    if (!timeZone.IsInvalidTime(searchWeekDateUser))
                    {
                        for (int i = 0; i <= 7; i++)
                        {
                            result.Add(new
                            {
                                date = searchWeekDateUser.Date.ToString("d dddd", culture),
                                value = _rewardPointService.GetDics(
                                    createdFromUtc: _dateTimeHelper.ConvertToUtcTime(searchWeekDateUser, timeZone),
                                    createdToUtc: _dateTimeHelper.ConvertToUtcTime(searchWeekDateUser.AddDays(1), timeZone),
                                    storeId: storeId,

                                    pageIndex: 0,
                                    pageSize: 1).ToString()
                            });

                            searchWeekDateUser = searchWeekDateUser.AddDays(1);
                        }
                    }
                    break;
            }



            return Json(result, JsonRequestBehavior.AllowGet);
        }





        [AcceptVerbs(HttpVerbs.Get)]
        public virtual ActionResult LoadGifts(string period, string giftName)
        {


            int storeId = _workContext.CurrentCustomer.StoreId;
            var result = new List<object>();

            var nowDt = _dateTimeHelper.ConvertToUserTime(DateTime.Now);
            var timeZone = _dateTimeHelper.CurrentTimeZone;
            //var searchCustomerRoleIds = new[] { _customerService.GetCustomerRoleBySystemName(SystemCustomerRoleNames.Registered).Id };

            var culture = new System.Globalization.CultureInfo(_workContext.WorkingLanguage.LanguageCulture);


            switch (period)
            {
                case "year":
                    //year statistics
                    var yearAgoDt = nowDt.AddYears(-1).AddMonths(1);
                    var searchYearDateUser = new DateTime(yearAgoDt.Year, yearAgoDt.Month, 1);
                    if (!timeZone.IsInvalidTime(searchYearDateUser))
                    {
                        for (int i = 0; i <= 12; i++)
                        {
                            result.Add(new
                            {
                                date = searchYearDateUser.Date.ToString("Y", culture),
                                value = _rewardPointService.GetGifts (
                                    createdFromUtc: _dateTimeHelper.ConvertToUtcTime(searchYearDateUser, timeZone),
                                    createdToUtc: _dateTimeHelper.ConvertToUtcTime(searchYearDateUser.AddMonths(1), timeZone),
                                    storeId: storeId,
                                    giftName: giftName,
                                    pageIndex: 0,
                                    pageSize: 1).ToString()
                            });

                            searchYearDateUser = searchYearDateUser.AddMonths(1);
                        }
                    }
                    break;

                case "month":
                    //month statistics
                    var monthAgoDt = nowDt.AddDays(-30);
                    var searchMonthDateUser = new DateTime(monthAgoDt.Year, monthAgoDt.Month, monthAgoDt.Day);
                    if (!timeZone.IsInvalidTime(searchMonthDateUser))
                    {
                        for (int i = 0; i <= 30; i++)
                        {
                            result.Add(new
                            {
                                date = searchMonthDateUser.Date.ToString("M", culture),
                                value = _rewardPointService.GetGifts(
                                    createdFromUtc: _dateTimeHelper.ConvertToUtcTime(searchMonthDateUser, timeZone),
                                    createdToUtc: _dateTimeHelper.ConvertToUtcTime(searchMonthDateUser.AddDays(1), timeZone),
                                    storeId: storeId,
                                      giftName: giftName,
                                    pageIndex: 0,
                                    pageSize: 1).ToString()
                            });

                            searchMonthDateUser = searchMonthDateUser.AddDays(1);
                        }
                    }
                    break;

                case "week":
                default:
                    //week statistics
                    var weekAgoDt = nowDt.AddDays(-7);
                    var searchWeekDateUser = new DateTime(weekAgoDt.Year, weekAgoDt.Month, weekAgoDt.Day);
                    if (!timeZone.IsInvalidTime(searchWeekDateUser))
                    {
                        for (int i = 0; i <= 7; i++)
                        {
                            result.Add(new
                            {
                                date = searchWeekDateUser.Date.ToString("d dddd", culture),
                                value = _rewardPointService.GetGifts(
                                    createdFromUtc: _dateTimeHelper.ConvertToUtcTime(searchWeekDateUser, timeZone),
                                    createdToUtc: _dateTimeHelper.ConvertToUtcTime(searchWeekDateUser.AddDays(1), timeZone),
                                    storeId: storeId,
                                      giftName: giftName,

                                    pageIndex: 0,
                                    pageSize: 1).ToString()
                            });

                            searchWeekDateUser = searchWeekDateUser.AddDays(1);
                        }
                    }
                    break;
            }



            return Json(result, JsonRequestBehavior.AllowGet);
        }




    }
}