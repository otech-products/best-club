﻿using System.Collections.Generic;
using System.Web.Mvc;
using FluentValidation.Attributes;
using Nop.Admin.Validators.Stores;
using Nop.Web.Framework;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Models.Stores
{
    [Validator(typeof(StoreValidator))]
    public partial class StoreModel : BaseNopEntityModel, ILocalizedModel<StoreLocalizedModel>
    {
        public StoreModel()
        {
            Locales = new List<StoreLocalizedModel>();
            AvailableLanguages = new List<SelectListItem>();
        }



        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.Name")]
        [AllowHtml]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.Url")]
        [AllowHtml]
        public string Url { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.SslEnabled")]
        public virtual bool SslEnabled { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.SecureUrl")]
        [AllowHtml]
        public virtual string SecureUrl { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.Hosts")]
        [AllowHtml]
        public string Hosts { get; set; }

        //default language
        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.DefaultLanguage")]
        [AllowHtml]
        public int DefaultLanguageId { get; set; }
        public IList<SelectListItem> AvailableLanguages { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.CompanyName")]
        [AllowHtml]
        public string CompanyName { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.CompanyAddress")]
        [AllowHtml]
        public string CompanyAddress { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.CompanyPhoneNumber")]
        [AllowHtml]
        public string CompanyPhoneNumber { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.CompanyVat")]
        [AllowHtml]
        public string CompanyVat { get; set; }

        //[NopResourceDisplayName("Admin.Configuration.Stores.Fields.NumberOfCustomers")]
        
        //public int NumberOfCustomers { get; set; }
        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.SmsUserName")]
        [AllowHtml]
        public string SmsUserName { get; set; }
        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.SmsPass")]
        [AllowHtml]
        public string SmsPass { get; set; }
        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.welcomeMsg")]
        [AllowHtml]
        public string welcomeMsg { get; set; }
        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.periodicMsg1")]
        [AllowHtml]
        public string periodicMsg1 { get; set; }
        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.periodicMsg2")]
        [AllowHtml]
        public string periodicMsg2 { get; set; }
        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.periodicMsg3")]
        [AllowHtml]
        public string periodicMsg3 { get; set; }
        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.periodicMsg4")]
        [AllowHtml]
        public string periodicMsg4 { get; set; }
        //[NopResourceDisplayName("Admin.Configuration.Stores.Fields.weeklyCustomerCount")]
        //[AllowHtml]
        //public string weeklyCustomerCount { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.isPoints")]
        [AllowHtml]
        public bool isPoints { get; set; }
        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.isGifts")]
        [AllowHtml]
        public bool isGifts { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.preMsg")]
        [AllowHtml]
        public string preMsg { get; set; }
        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.gift1")]
        [AllowHtml]
        public bool gift1 { get; set; }
        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.gift2")]
        [AllowHtml]
        public bool gift2 { get; set; }
        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.pointImpsSum ")]
        [AllowHtml]

        public bool gift3 { get; set; }
        public bool gift4 { get; set; }
        //public int pointImpsSum { get; set; }
        //[NopResourceDisplayName("Admin.Configuration.Stores.Fields.pointImpsVisits")]
        //[AllowHtml]
        //public int pointImpsVisits { get; set; }


        //[NopResourceDisplayName("Admin.Configuration.Stores.Fields.gift1Imps")]
        
        //public int gift1Imps { get; set; }
        //[NopResourceDisplayName("Admin.Configuration.Stores.Fields.gift2Imps")]
      
        //public int gift2Imps { get; set; }

        public bool DateOfBirthEnabled { get; set; }
        public bool DateOfBirthRequired { get; set; }
        public bool PhoneEnabled { get; set; }
        public bool PhoneRequired { get; set; }

        public bool HoneypotEnabled { get; set; }
        public bool GenderEnabled { get; set; }
        public bool captchaEnabled { get; set; }
        public bool SmsOnReg { get; set; }
        public bool SmsOnBday { get; set; }
        public bool SmsOnDate2 { get; set; }
        public bool gift2OnBday { get; set; }
        public bool gift2OnDate2 { get; set; }
        public string gift1Name { get; set; }
        public string gift2Name { get; set; }
        public string gift3Name { get; set; }
        public string gift4Name { get; set; }
        public float sum2PointsCoefficient { get; set; }
        public string pointsMsg { get; set; }
        public string pointsMsg2 { get; set; }
        public bool date2Enabled { get; set; }
        public string date2Name { get; set; }
        public bool date3Enabled { get; set; }
        public string date3Name { get; set; }
        public bool date2Required { get; set; }
        public bool date3Required { get; set; }
        public int pointsOnReg { get; set; }
        public bool gift1OnReg { get; set; }
        public bool gift2OnReg { get; set; }
        public string BdaySmS { get; set; }
        public bool Active { get; set; }

        public string smsRemovalLink { get; set; }

        public string rgb { get; set; }
        public string preFirstNameMsg { get; set; }
        public string gift1Msg { get; set; }
        public string gift2Msg { get; set; }
        public string gift3Msg { get; set; }
        public string gift4Msg { get; set; }
        public string MsgLastVisit { get; set; }
        public string contactMsg { get; set; }

        public string contactMsgMonth { get; set; }
        public bool gift1OnBday { get; set; }
        public int rutineSmSTimeOffset { get; set; }
        public bool SendSmSFromLastVisit { get; set; }
        public int MonthsFromLastVisit { get; set; }
        public int? days2GiftEnd { get; set; }

        public string days2GiftEndMsg { get; set; }

        public string contactPhone { get; set; }


        public bool multiField { get; set; }
        public bool check1 { get; set; }
        public bool check2 { get; set; }
        public bool check3 { get; set; }
        public bool check4 { get; set; }
        public bool check5 { get; set; }

        public string multiFieldName { get; set; }
        public string check1String { get; set; }
        public string check2String { get; set; }
        public string check3String { get; set; }
        public string check4String { get; set; }
        public string check5String { get; set; }


        public bool isFacebook { get; set; }

        ////14.11.17 - start////////////////////
        public bool gift1ValidityPeriod { get; set; }
        public bool gift2ValidityPeriod { get; set; }
        public bool gift3ValidityPeriod { get; set; }
        public bool gift4ValidityPeriod { get; set; }



        public int gift1ValidityDays { get; set; }

        public int gift2ValidityDays { get; set; }

        public int gift3ValidityDays { get; set; }

        public int gift4ValidityDays { get; set; }


        public int gift1NotImp { get; set; }

        public int gift2NotImp { get; set; }

        public int gift3NotImp { get; set; }

        public int gift4NotImp { get; set; }

        public string randString { get; set; }

        public int weeklyTarget { get; set; }


        ////14.11.17 - end////////////////////
        [NopResourceDisplayName("Doc_num_lak")]
        [AllowHtml]
        public string Api_Custom_1 { get; set; }

        [NopResourceDisplayName("Doc_mach")]
        public string Api_Custom_2 { get; set; }
        [NopResourceDisplayName("Api_user")]
        [AllowHtml]
        public string Api_user { get; set; }
        [NopResourceDisplayName("Api_pass")]
        [AllowHtml]
        public string Api_pass { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.AllowTZ")]
        [AllowHtml]
        public bool AllowTZ { get; set; }
        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.SearchByTZ")]
        [AllowHtml]
        public bool SearchByTZ { get; set; }
        [NopResourceDisplayName("Admin.Configuration.Stores.TZ")]
        [AllowHtml]
        public int TZ { get; set; }

        public bool AllowRealEmail { get; set; }


        public IList<StoreLocalizedModel> Locales { get; set; }
    }

    public partial class StoreLocalizedModel : ILocalizedModelLocal
    {
        public int LanguageId { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.Name")]
        [AllowHtml]
        public string Name { get; set; }
    }
}