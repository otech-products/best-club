﻿using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Models.Home
{
    public partial class CommonStatisticsModel : BaseNopModel
    {
      

        public int NumberOfCustomers { get; set; }
        public string weeklyCustomerCountInt { get; set; }
        public int pointImpsSum { get; set; }
        public int pointImpsVisits { get; set; }

        public int visits { get; set; }

        public int gift1Imps { get; set; }

        public int gift2Imps { get; set; }

        public int monthlyVisits { get; set; }

        public int weeklyVisits { get; set; }

        public string gift1Name { get; set; }
        public string gift2Name { get; set; }

        public int? monthlyCustomerCount { get; set; }

        public int internet2Ez { get; set; }

        public int BdayMsgSentInLastRutine { get; set; }
        public int Date2MsgSentInLastRutine { get; set; }
        public bool SmsOnBday { get; set; }
        public bool SmsOnDate2 { get; set; }
       
        public bool gift1 { get; set; }

        public bool gift2 { get; set; }

        public int totalCustomersPoints { get; set; }

        public bool isPoints { get; set; }

        public bool isFacebook { get; set; }







    }
}