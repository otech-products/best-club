﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using FluentValidation.Attributes;
using Nop.Admin.Validators.Messages;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Web.Models.Customer;


namespace Nop.Admin.Models.Messages
{
  
        public partial class GiftModel : BaseNopEntityModel
        {
            public GiftModel()
            {
                this.AvailableStores = new List<SelectListItem>();
              
                this.Cabm = new List<CustomerAttributeModel>();
            }

            [NopResourceDisplayName("Admin.Promotions.Campaigns.Fields.Name")]
            [AllowHtml]
            public string Name { get; set; }

            [NopResourceDisplayName("Admin.Promotions.Campaigns.Fields.Subject")]
            [AllowHtml]
            public string Subject { get; set; }

            [NopResourceDisplayName("Admin.Promotions.Campaigns.Fields.Body")]
            [AllowHtml]
            public string Body { get; set; }

            [NopResourceDisplayName("Admin.Promotions.Campaigns.Fields.Store")]
            public int StoreId { get; set; }
            public IList<SelectListItem> AvailableStores { get; set; }

            [NopResourceDisplayName("Admin.Promotions.Campaigns.Fields.CustomerRole")]
            //public int CustomerRoleId { get; set; }
            public IList<SelectListItem> AvailableCustomerRoles { get; set; }

            [NopResourceDisplayName("Admin.Promotions.Campaigns.Fields.CreatedOn")]
            public DateTime CreatedOn { get; set; }

            [NopResourceDisplayName("Admin.Promotions.Campaigns.Fields.DontSendBeforeDate")]
            [UIHint("DateTimeNullable")]
            public DateTime? EndDate { get; set; }

            [NopResourceDisplayName("Admin.Promotions.Campaigns.Fields.AllowedTokens")]
            public string AllowedTokens { get; set; }

            [NopResourceDisplayName("Admin.Promotions.Campaigns.Fields.TestEmail")]
            [AllowHtml]
            public string TestEmail { get; set; }

            public string Gender { get; set; }

            public string phone { get; set; }

            public string company { get; set; }

            public string StreetAddress { get; set; }
            public string StreetAddress2 { get; set; }

            public string City { get; set; }

            public int points { get; set; }

            public string gift { get; set; }

        //public int gift2 { get; set; }

        //public int gift3 { get; set; }

        //public int gift4 { get; set; }
        public bool gift1Enabled { get; set; }

        public bool gift2Enabled { get; set; }

        public bool gift3Enabled { get; set; }

        public bool gift4Enabled { get; set; }
        public string gift1Name { get; set; }

        public string gift2Name { get; set; }

        public string gift3Name { get; set; }

        public string gift4Name { get; set; }

        public int origin { get; set; }
           
            [UIHint("DateTimeNullable")]
            public DateTime? DateOfBirth { get; set; }
            [UIHint("DateTimeNullable")]
            public DateTime? Date2 { get; set; }
            [UIHint("DateTimeNullable")]
            public DateTime? Date3 { get; set; }

            [NopResourceDisplayName("Account.Fields.Gender")]
            public bool GenderEnabled { get; set; }



            public bool CompanyEnabled { get; set; }


            public bool StreetAddressEnabled { get; set; }


            public bool StreetAddress2Enabled { get; set; }

            public bool DateOfBirthEnabled { get; set; }
            [UIHint("DateTimeNullable")]
            public DateTime? CreatedOnUtcTo { get; set; }
            [UIHint("DateTimeNullable")]
            public DateTime? DateOfBirthTo { get; set; }
            [UIHint("DateTimeNullable")]
            public DateTime? Date2To { get; set; }
            [UIHint("DateTimeNullable")]
            public DateTime? Date3To { get; set; }

            public List<CustomerAttributeModel> Cabm { get; set; }

            public bool withName { get; set; }
            public bool Date2Enabled { get; set; }
            public string Date2Name { get; set; }

            public bool Date3Enabled { get; set; }
            public string Date3Name { get; set; }

            public int? MaxPoints { get; set; }

            public int? MinPoints { get; set; }
            public bool isPoints { get; set; }
            public int CustomerCount { get; set; }
        public string giftVal { get; set; }

    }
    }
