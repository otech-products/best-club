﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Admin.Models.Messages
{
    public class NewsLetterSubscriptionLight
    {
        public DateTime CreatedOn { get; set; }
        public string Email { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
    }
}