﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using FluentValidation.Attributes;
using Nop.Admin.Validators.Messages;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Web.Models.Customer;

namespace Nop.Admin.Models.Messages
{
    [Validator(typeof(CampaignValidator))]
    public partial class CampaignModel : BaseNopEntityModel
    {
        public CampaignModel()
        {
            this.AvailableStores = new List<SelectListItem>();
            //this.AvailableCustomerRoles = new List<SelectListItem>();
            this.Cabm = new List<CustomerAttributeModel>();
            this.isSent = 0;
        }

        [NopResourceDisplayName("Admin.Promotions.Campaigns.Fields.Name")]
        [AllowHtml]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Promotions.Campaigns.Fields.Subject")]
        [AllowHtml]
        public string Subject { get; set; }

        [NopResourceDisplayName("Admin.Promotions.Campaigns.Fields.Body")]
        [AllowHtml]
        public string Body { get; set; }

        [NopResourceDisplayName("Admin.Promotions.Campaigns.Fields.Store")]
        public int StoreId { get; set; }
        public IList<SelectListItem> AvailableStores { get; set; }

       
      

        [NopResourceDisplayName("Admin.Promotions.Campaigns.Fields.CreatedOn")]
        [UIHint("DateTimeNullable")]
        public DateTime? CreatedOn { get; set; }

        [NopResourceDisplayName("Admin.Promotions.Campaigns.Fields.DontSendBeforeDate")]
        [UIHint("DateTimeNullable")]
        public DateTime? DontSendBeforeDate { get; set; }

        [NopResourceDisplayName("Admin.Promotions.Campaigns.Fields.AllowedTokens")]
        public string AllowedTokens { get; set; }

        [NopResourceDisplayName("Admin.Promotions.Campaigns.Fields.TestEmail")]
        [AllowHtml]
        public string TestEmail { get; set; }

        public string Gender { get; set; }

        public string phone { get; set; }

        public string company { get; set; }

        public string StreetAddress { get; set; }
        public string StreetAddress2 { get; set; }

        public string City { get; set; }

        public int points { get; set; }

        public int gift1 { get; set; }

        public int gift2 { get; set; }

        public int gift3 { get; set; }

        public int  gift4 { get; set; }
        public bool gift1Enabled { get; set; }

        public bool gift2Enabled { get; set; }

        public bool gift3Enabled { get; set; }

        public bool gift4Enabled { get; set; }
        public string gift1Name { get; set; }

        public string gift2Name { get; set; }

        public string gift3Name { get; set; }

        public string gift4Name { get; set; }

        public int origin { get; set; }
        [UIHint("DateTimeNullable")]
        public DateTime? CreatedOnUtc { get; set; }
        [UIHint("DateTimeNullable")]
        public DateTime? Date2 { get; set; }
        [UIHint("DateTimeNullable")]
        public DateTime? DateOfBirth { get; set; }
        [UIHint("DateTimeNullable")]
        public DateTime? Date3 { get; set; }

        [NopResourceDisplayName("Account.Fields.Gender")]
        public bool GenderEnabled { get; set; }
    

     
        public bool CompanyEnabled { get; set; }

       
        public bool StreetAddressEnabled { get; set; }

      
        public bool StreetAddress2Enabled { get; set; }

        public bool DateOfBirthEnabled { get; set; }
        [UIHint("DateTimeNullable")]
        public DateTime? CreatedTo { get; set; }
        [UIHint("DateTimeNullable")]
        public DateTime? DateOfBirthTo { get; set; }
        [UIHint("DateTimeNullable")]
        public DateTime? Date2To { get; set; }
        [UIHint("DateTimeNullable")]
        public DateTime? Date3To { get; set; }

        public List<CustomerAttributeModel> Cabm { get; set; }

        public bool withName { get; set; }
         public bool Date2Enabled { get; set; }
        public string Date2Name { get; set; }

        public bool Date3Enabled { get; set; }
        public string Date3Name { get; set; }

        public int? MaxPoints { get; set; }

        public int? MinPoints { get; set; }
         public bool isPoints { get; set; }
        public bool AdminOrVendor { get; set; }

        public bool multiField { get; set; }
        public bool check1 { get; set; }
        public bool check2 { get; set; }
        public bool check3 { get; set; }
        public bool check4 { get; set; }
        public bool check5 { get; set; }


        public bool check1Val { get; set; }
        public bool check2Val { get; set; }
        public bool check3Val { get; set; }
        public bool check4Val { get; set; }
        public bool check5Val { get; set; }

        public string multiFieldName { get; set; }
        public string check1String { get; set; }
        public string check2String { get; set; }
        public string check3String { get; set; }
        public string check4String { get; set; }
        public string check5String { get; set; }




        public int? DateOfBirthDay { get; set; }
        [NopResourceDisplayName("Account.Fields.DateOfBirth")]
        public int? DateOfBirthMonth { get; set; }
        [NopResourceDisplayName("Account.Fields.DateOfBirth")]
        public int? DateOfBirthYear { get; set; }
      
        public DateTime? ParseDateOfBirth()
        {
            if (!DateOfBirthMonth.HasValue || !DateOfBirthDay.HasValue)
                return null;

            DateTime? dateOfBirth = null;

            try
            {
                dateOfBirth = new DateTime(1900, DateOfBirthMonth.Value, DateOfBirthDay.Value);
            }
            catch { }



            return dateOfBirth;
        }


        public DateTime? ParseDateOfBirthWithYear()
        {
            if (!DateOfBirthMonth.HasValue || !DateOfBirthDay.HasValue || !DateOfBirthYear.HasValue)
                return null;

            DateTime? dateOfBirth = null;
            try
            {
                dateOfBirth = new DateTime(DateOfBirthYear.Value, DateOfBirthMonth.Value, DateOfBirthDay.Value);
            }
            catch { }



            return dateOfBirth;
        }








        public int? DateOfBirthToDay { get; set; }
        [NopResourceDisplayName("Account.Fields.DateOfBirthTo")]
        public int? DateOfBirthToMonth { get; set; }
        [NopResourceDisplayName("Account.Fields.DateOfBirthTo")]
        public int? DateOfBirthToYear { get; set; }
        public bool DateOfBirthToRequired { get; set; }
        public DateTime? ParseDateOfBirthTo()
        {
            if (!DateOfBirthToMonth.HasValue || !DateOfBirthToDay.HasValue)
                return null;

            DateTime? DateOfBirthTo = null;

            try
            {
                DateOfBirthTo = new DateTime(1900, DateOfBirthToMonth.Value, DateOfBirthToDay.Value);
            }
            catch { }



            return DateOfBirthTo;
        }


        public DateTime? ParseDateOfBirthToWithYear()
        {
            if (!DateOfBirthToMonth.HasValue || !DateOfBirthToDay.HasValue || !DateOfBirthToYear.HasValue)
                return null;

            DateTime? DateOfBirthTo = null;
            try
            {
                DateOfBirthTo = new DateTime(DateOfBirthToYear.Value, DateOfBirthToMonth.Value, DateOfBirthToDay.Value);
            }
            catch { }



            return DateOfBirthTo;
        }










        public int? Date2Day { get; set; }
        [NopResourceDisplayName("Account.Fields.Date2")]
        public int? Date2Month { get; set; }
        [NopResourceDisplayName("Account.Fields.Date2")]
        public int? Date2Year { get; set; }
        public bool Date2Required { get; set; }
        public DateTime? ParseDate2()
        {
            if (!Date2Month.HasValue || !Date2Day.HasValue)
                return null;

            DateTime? Date2 = null;

            try
            {
                Date2 = new DateTime(1900, Date2Month.Value, Date2Day.Value);
            }
            catch { }



            return Date2;
        }


        public DateTime? ParseDate2WithYear()
        {
            if (!Date2Month.HasValue || !Date2Day.HasValue || !Date2Year.HasValue)
                return null;

            DateTime? Date2 = null;
            try
            {
                Date2 = new DateTime(Date2Year.Value, Date2Month.Value, Date2Day.Value);
            }
            catch { }



            return Date2;
        }




        public int? Date2ToDay { get; set; }
        [NopResourceDisplayName("Account.Fields.Date2To")]
        public int? Date2ToMonth { get; set; }
        [NopResourceDisplayName("Account.Fields.Date2To")]
        public int? Date2ToYear { get; set; }
        public bool Date2ToRequired { get; set; }
        public DateTime? ParseDate2To()
        {
            if (!Date2ToMonth.HasValue || !Date2ToDay.HasValue)
                return null;

            DateTime? Date2To = null;

            try
            {
                Date2To = new DateTime(1900, Date2ToMonth.Value, Date2ToDay.Value);
            }
            catch { }



            return Date2To;
        }


        public DateTime? ParseDate2ToWithYear()
        {
            if (!Date2ToMonth.HasValue || !Date2ToDay.HasValue || !Date2ToYear.HasValue)
                return null;

            DateTime? Date2To = null;
            try
            {
                Date2To = new DateTime(Date2ToYear.Value, Date2ToMonth.Value, Date2ToDay.Value);
            }
            catch { }



            return Date2To;
        }



        public int? Date3Day { get; set; }
        [NopResourceDisplayName("Account.Fields.Date3")]
        public int? Date3Month { get; set; }
        [NopResourceDisplayName("Account.Fields.Date3")]
        public int? Date3Year { get; set; }
        public bool Date3Required { get; set; }
        public DateTime? ParseDate3()
        {
            if (!Date3Month.HasValue || !Date3Day.HasValue)
                return null;

            DateTime? Date3 = null;

            try
            {
                Date3 = new DateTime(1900, Date3Month.Value, Date3Day.Value);
            }
            catch { }



            return Date3;
        }


        public DateTime? ParseDate3WithYear()
        {
            if (!Date3Month.HasValue || !Date3Day.HasValue || !Date3Year.HasValue)
                return null;

            DateTime? Date3 = null;
            try
            {
                Date3 = new DateTime(Date3Year.Value, Date3Month.Value, Date3Day.Value);
            }
            catch { }



            return Date3;
        }





        public int? Date3ToDay { get; set; }
        [NopResourceDisplayName("Account.Fields.Date3To")]
        public int? Date3ToMonth { get; set; }
        [NopResourceDisplayName("Account.Fields.Date3To")]
        public int? Date3ToYear { get; set; }
        public bool Date3ToRequired { get; set; }
        public DateTime? ParseDate3To()
        {
            if (!Date3ToMonth.HasValue || !Date3ToDay.HasValue)
                return null;

            DateTime? Date3To = null;

            try
            {
                Date3To = new DateTime(1900, Date3ToMonth.Value, Date3ToDay.Value);
            }
            catch { }



            return Date3To;
        }


        public DateTime? ParseDate3ToWithYear()
        {
            if (!Date3ToMonth.HasValue || !Date3ToDay.HasValue || !Date3ToYear.HasValue)
                return null;

            DateTime? Date3To = null;
            try
            {
                Date3To = new DateTime(Date3ToYear.Value, Date3ToMonth.Value, Date3ToDay.Value);
            }
            catch { }



            return Date3To;
        }
        public int NumOfSent { get; set; }

        public bool ezDevice { get; set; }

        public bool facebook { get; set; }
        public bool instagram { get; set; }
        public bool website { get; set; }
        public bool internet { get; set; }
        public bool legacyDb { get; set; }

        public bool other { get; set; }
      public bool multiOrigin { get; set; }
        public int isSent { get; set; }
        public DateTime? SentOn { get; set; }
        //  public bool allOrigin { get; set; }







































    }

   


}
