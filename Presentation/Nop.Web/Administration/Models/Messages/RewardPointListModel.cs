﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;


namespace Nop.Admin.Models.Messages
{
    public class RewardPointListModel : BaseNopModel
    {
        public RewardPointListModel()
        {
            this.AvailableStores = new List<SelectListItem>();
            //this.AvailableCustomerRoles = new List<SelectListItem>();
           
        }
        public string SearchEmail { get; set; }

        [NopResourceDisplayName("Admin.Promotions.NewsLetterSubscriptions.List.SearchStore")]
        public int StoreId { get; set; }


        
        public string Employee { get; set; }

        public string Message { get; set; }

        public string GiftName { get; set; }









        [NopResourceDisplayName("Admin.Promotions.NewsLetterSubscriptions.List.StartDate")]
        [UIHint("DateNullable")]
        public DateTime? StartDate { get; set; }

        [NopResourceDisplayName("Admin.Promotions.NewsLetterSubscriptions.List.EndDate")]
        [UIHint("DateNullable")]
        public DateTime? EndDate { get; set; }
        public IList<SelectListItem> AvailableStores { get; set; }
    }
}