﻿using System;
using System.Web.Mvc;
using FluentValidation.Attributes;
using Nop.Admin.Validators.Messages;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Models.Messages
{
    [Validator(typeof(NewsLetterSubscriptionValidator))]
    public partial class NewsLetterSubscriptionModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("Admin.Promotions.NewsLetterSubscriptions.Fields.Email")]
        [AllowHtml]
        public string Email { get; set; }

        [NopResourceDisplayName("Admin.Promotions.NewsLetterSubscriptions.Fields.Active")]
        public bool Active { get; set; }

        [NopResourceDisplayName("Admin.Promotions.NewsLetterSubscriptions.Fields.Store")]
        public string StoreName { get; set; }

        [NopResourceDisplayName("Admin.Promotions.NewsLetterSubscriptions.Fields.CreatedOn")]
        public DateTime CreatedOn { get; set; }

        [NopResourceDisplayName("Admin.Promotions.NewsLetterSubscriptions.Fields.FirstName")]
        public string FirstName { get; set; }
        [NopResourceDisplayName("Admin.Promotions.NewsLetterSubscriptions.Fields.LastName")]
        public string LastName { get; set; }

        // public string Gender { get; set; }
        /// <summary>
        /// Gets or sets the newsletter subscription GUID
        /// </summary>
        public Guid NewsLetterSubscriptionGuid { get; set; }

      

        /// <summary>
        /// Gets or sets the store identifier in which a customer has subscribed to newsletter
        /// </summary>
       // public int StoreId { get; set; }

        /// <summary>
        /// Gets or sets the date and time when subscription was created
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public DateTime? Date2 { get; set; }

        public DateTime? Date3 { get; set; }
        [NopResourceDisplayName("Admin.Promotions.NewsLetterSubscriptions.Fields.Gender")]

        public string Gender { get; set; }

        public string phone { get; set; }

        public string company { get; set; }

        public string StreetAddress { get; set; }
        public string StreetAddress2 { get; set; }

        public string City { get; set; }
        [NopResourceDisplayName("Admin.Promotions.NewsLetterSubscriptions.Fields.points ")]
        public int points { get; set; }
        [NopResourceDisplayName("Admin.Promotions.NewsLetterSubscriptions.Fields.gift1 ")]
        public bool gift1 { get; set; }
        [NopResourceDisplayName("Admin.Promotions.NewsLetterSubscriptions.Fields.gift2 ")]
        public bool gift2 { get; set; }
        [NopResourceDisplayName("Admin.Promotions.NewsLetterSubscriptions.Fields.lastvisit ")]
        public DateTime? lastVisit{ get; set; }
        public int origin { get; set; }
        /// <summary>
        /// /////// ad to db 4.4
        /// </summary>
        public string LastIpAddress { get; set; }

        public int visits { get; set; }

    }
}