﻿using Nop.Web.Framework;
using Nop.Web.Models.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Home
{
    public class lightSubModel
    {
        public lightSubModel()
        {
            
            this.CustomerAttributes = new List<CustomerAttributeModel>();
        }
        public int storeID;

        public string Email { get; set; }
        [NopResourceDisplayName("Admin.Promotions.NewsLetterSubscriptions.Fields.FirstName")]
        public string FirstName { get; set; }
        [NopResourceDisplayName("Admin.Promotions.NewsLetterSubscriptions.Fields.LastName")]
        public string LastName { get; set; }
        public bool gift1 { get; set; }

        public string dateOfBirth { get; set; }

        public bool gift2 { get; set; }
        public bool gift3 { get; set; }
        public bool gift4 { get; set; }
        [NopResourceDisplayName("Admin.Promotions.NewsLetterSubscriptions.Fields.points")]
        public int points { get; set; }
        public bool isPoints { get; set; }

        public int incPoints { get; set; }
        public int dicPoints { get; set; }
        public string gift1Name { get; set; }
        public string gift2Name { get; set; }
        public string gift3Name { get; set; }
        public string gift4Name { get; set; }
        public string rgb { get; set; }
        public IList<CustomerAttributeModel> CustomerAttributes { get;  set; }
    }
}