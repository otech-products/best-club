﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nop.Web.Framework;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc;

namespace Nop.Web.Models.Home
{
    public class StoreModel
    {
        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.Name")]
    
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.weeklyCustomerCount")]
        
        public int weeklyCustomerCountInt { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Stores.Fields.NumberOfCustomers")]

        public int NumberOfCustomers { get; set; }
        public int id { get; set; }
        public bool isPoints { get; set; }
        public int visits { get; set; }
        public int monthlyVisits { get; set; }

        public int weeklyVisits { get; set; }
        public int pointsOnReg { get; set; }
        public float sum2PointsCoefficient { get; set; }
        public int pointImpsSum { get; set; }

        public int pointImpsVisits { get; set; }
      
        public string welcomeMsg { get; set; }

        public string rgb { get; set; }
    }
}