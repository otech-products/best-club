﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Home
{
    public class statModel
    {
        public int visits { get; set; }
        public int monthlyVisits { get; set; }

        public int weeklyVisits { get; set; }
        public int pointsOnReg { get; set; }
        public float sum2PointsCoefficient { get; set; }
        public int pointImpsSum { get; set; }

        public int pointImpsVisits { get; set; }
        public string weeklyCustomerCount { get; set; }
        public int NumberOfCustomers { get; set; }
        public string welcomeMsg { get; set; }

    }
}