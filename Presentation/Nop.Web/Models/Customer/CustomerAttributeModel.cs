﻿using System.Collections.Generic;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Mvc;
using System;

namespace Nop.Web.Models.Customer
{
    public partial class CustomerAttributeModel : BaseNopEntityModel
    {
        public CustomerAttributeModel()
        {
            Values = new List<CustomerAttributeValueModel>();
        }
        public int DisplayOrder { get; set; }
        public string Name { get; set; }

        public bool IsRequired { get; set; }

        /// <summary>
        /// Default value for textboxes
        /// </summary>
        public string DefaultValue { get; set; }

        public AttributeControlType AttributeControlType { get; set; }

        public IList<CustomerAttributeValueModel> Values { get; set; }

        public int? Day { get; set; }
        
        public int? Month { get; set; }
        
        public int? Year { get; set; }
       


        public System.DateTime? ParseDate()
        {
            if (!Year.HasValue || !Month.HasValue || !Day.HasValue)
                return null;

            DateTime? date = null;
            try
            {
                date = new System.DateTime(Year.Value, Month.Value, Day.Value);
            }
            catch { }
            return date;
        }

    }

    public partial class CustomerAttributeValueModel : BaseNopEntityModel
    {
        public string Name { get; set; }

        public bool IsPreSelected { get; set; }
    }
}