﻿using Nop.Web.Framework.Mvc;

namespace Nop.Web.Models.Customer
{
    public partial class RegisterResultModel : BaseNopModel
    {
        public string Result { get; set; }
        public int storeID { get; set; }
    }
}