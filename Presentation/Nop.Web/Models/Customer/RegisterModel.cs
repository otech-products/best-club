﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using FluentValidation.Attributes;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using Nop.Web.Validators.Customer;

namespace Nop.Web.Models.Customer
{
    [Validator(typeof(RegisterValidator))]
    public partial class RegisterModel : BaseNopModel
    {
        public RegisterModel()
        {
            //this.AvailableTimeZones = new List<SelectListItem>();
            //this.AvailableCountries = new List<SelectListItem>();
            //this.AvailableStates = new List<SelectListItem>();
            this.CustomerAttributes = new List<CustomerAttributeModel>();
        }

        [NopResourceDisplayName("Account.Fields.Email")]
        [AllowHtml]
        public string Email { get; set; }

        public bool AllowRealEmail { get; set; }
        public string RealEmail { get; set; }

        public bool EnteringEmailTwice { get; set; }
        [NopResourceDisplayName("Account.Fields.ConfirmEmail")]
        [AllowHtml]
        public string ConfirmEmail { get; set; }

        public bool UsernamesEnabled { get; set; }
        [NopResourceDisplayName("Account.Fields.Username")]
        [AllowHtml]
        public string Username { get; set; }

        public bool CheckUsernameAvailabilityEnabled { get; set; }

        [DataType(DataType.Password)]
        [NoTrim]
        [NopResourceDisplayName("Account.Fields.Password")]
        [AllowHtml]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [NoTrim]
        [NopResourceDisplayName("Account.Fields.ConfirmPassword")]
        [AllowHtml]
        public string ConfirmPassword { get; set; }

        //form fields & properties
        public bool GenderEnabled { get; set; }
        [NopResourceDisplayName("Account.Fields.Gender")]
        public string Gender { get; set; }

        [NopResourceDisplayName("Account.Fields.FirstName")]
        [AllowHtml]
        public string FirstName { get; set; }
        [NopResourceDisplayName("Account.Fields.LastName")]
        [AllowHtml]
        public string LastName { get; set; }


        public bool DateOfBirthEnabled { get; set; }
        [NopResourceDisplayName("Account.Fields.DateOfBirth")]
        public int? DateOfBirthDay { get; set; }
        [NopResourceDisplayName("Account.Fields.DateOfBirth")]
        public int? DateOfBirthMonth { get; set; }
        [NopResourceDisplayName("Account.Fields.DateOfBirth")]
        public int? DateOfBirthYear { get; set; }
        public bool DateOfBirthRequired { get; set; }
        public DateTime? ParseDateOfBirth()
        {
            if ( !DateOfBirthMonth.HasValue || !DateOfBirthDay.HasValue)
                return null;

            DateTime? dateOfBirth = null;
           
                try
                {
                    dateOfBirth = new DateTime(1900, DateOfBirthMonth.Value, DateOfBirthDay.Value);
                }
                catch { }
           

            
            return dateOfBirth;
        }


        public DateTime? ParseDateOfBirthWithYear()
        {
            if (!DateOfBirthMonth.HasValue || !DateOfBirthDay.HasValue || !DateOfBirthYear.HasValue)
                return null;

            DateTime? dateOfBirth = null;
             try
                {
                    dateOfBirth = new DateTime(DateOfBirthYear.Value, DateOfBirthMonth.Value, DateOfBirthDay.Value);
                }
                catch { }
          

           
            return dateOfBirth;
        }

        public bool Date2Enabled { get; set; }
        public string Date2Name { get; set; }

        public int? Day2 { get; set; }

        public int? Month2 { get; set; }

        public int? Year2 { get; set; }



        public System.DateTime? ParseDate2()
        {
            if (!Year2.HasValue || !Month2.HasValue || !Day2.HasValue)
                return null;

            DateTime? date2 = null;
            try
            {
                date2 = new System.DateTime(Year2.Value, Month2.Value, Day2.Value);
            }
            catch { }
            return date2;
        }
        public bool Date3Enabled { get; set; }

        public string Date3Name { get; set; }

        public int? Day3 { get; set; }

        public int? Month3 { get; set; }

        public int? Year3 { get; set; }

        public string rgb { get; set; }
        public string phone { get; set; }


        public System.DateTime? ParseDate3()
        {
            if (!Year3.HasValue || !Month3.HasValue || !Day3.HasValue)
                return null;

            DateTime? date3 = null;
            try
            {
                date3 = new System.DateTime(Year3.Value, Month3.Value, Day3.Value);
            }
            catch { }
            return date3;
        }
        public bool Date2Required { get; set; }
        public bool Date3Required { get; set; }
        //public bool CompanyEnabled { get; set; }
        //public bool CompanyRequired { get; set; }
        //[NopResourceDisplayName("Account.Fields.Company")]
        //[AllowHtml]
        //public string Company { get; set; }

        //public bool StreetAddressEnabled { get; set; }
        //public bool StreetAddressRequired { get; set; }
        //[NopResourceDisplayName("Account.Fields.StreetAddress")]
        //[AllowHtml]
        //public string StreetAddress { get; set; }

        //public bool StreetAddress2Enabled { get; set; }
        //public bool StreetAddress2Required { get; set; }
        //[NopResourceDisplayName("Account.Fields.StreetAddress2")]
        //[AllowHtml]
        //public string StreetAddress2 { get; set; }

        //public bool ZipPostalCodeEnabled { get; set; }
        //public bool ZipPostalCodeRequired { get; set; }
        //[NopResourceDisplayName("Account.Fields.ZipPostalCode")]
        //[AllowHtml]
        //public string ZipPostalCode { get; set; }

        //public bool CityEnabled { get; set; }
        //public bool CityRequired { get; set; }
        //[NopResourceDisplayName("Account.Fields.City")]
        //[AllowHtml]
        //public string City { get; set; }

        //public bool CountryEnabled { get; set; }
        //public bool CountryRequired { get; set; }
        //[NopResourceDisplayName("Account.Fields.Country")]
        //public int CountryId { get; set; }
        //public string CountryName { get; set; }
        //public IList<SelectListItem> AvailableCountries { get; set; }

        //public bool StateProvinceEnabled { get; set; }
        //public bool StateProvinceRequired { get; set; }
        //[NopResourceDisplayName("Account.Fields.StateProvince")]
        //public int StateProvinceId { get; set; }
        //public IList<SelectListItem> AvailableStates { get; set; }

        //public bool PhoneEnabled { get; set; }
        //public bool PhoneRequired { get; set; }
        //[NopResourceDisplayName("Account.Fields.Phone")]
        //[AllowHtml]
        //public string Phone { get; set; }

        //public bool FaxEnabled { get; set; }
        //public bool FaxRequired { get; set; }
        //[NopResourceDisplayName("Account.Fields.Fax")]
        //[AllowHtml]
        //public string Fax { get; set; }
        
        public bool NewsletterEnabled { get; set; }
        [NopResourceDisplayName("Account.Fields.Newsletter")]
        public bool Newsletter { get; set; }
        
        public bool AcceptPrivacyPolicyEnabled { get; set; }

        ////time zone
        //[NopResourceDisplayName("Account.Fields.TimeZone")]
        //public string TimeZoneId { get; set; }
        //public bool AllowCustomersToSetTimeZone { get; set; }
        //public IList<SelectListItem> AvailableTimeZones { get; set; }

        ////EU VAT
        //[NopResourceDisplayName("Account.Fields.VatNumber")]
        //public string VatNumber { get; set; }
        //public bool DisplayVatNumber { get; set; }

        //public bool HoneypotEnabled { get; set; }
        //public bool DisplayCaptcha { get; set; }

        public IList<CustomerAttributeModel> CustomerAttributes { get; set; }

        public bool multiField { get; set; }
        public bool check1 { get; set; }
        public bool check2 { get; set; }
        public bool check3 { get; set; }
        public bool check4 { get; set; }
        public bool check5 { get; set; }


        public bool check1Val { get; set; }
        public bool check2Val { get; set; }
        public bool check3Val { get; set; }
        public bool check4Val { get; set; }
        public bool check5Val { get; set; }

        public string multiFieldName { get; set; }
        public string check1String { get; set; }
        public string check2String { get; set; }
        public string check3String { get; set; }
        public string check4String { get; set; }
        public string check5String { get; set; }

        public bool AllowTZ { get; set; }
        [NopResourceDisplayName("Account.Fields.LastName")]
        
        public string TZ { get; set; }


        public string signature { get; set; }


    }
}