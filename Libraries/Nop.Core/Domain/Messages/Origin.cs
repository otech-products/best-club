﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Messages
{
    public enum Origin
    {
        all=0,
        Tablet=1,
        Facebook=2,
        Site=3,
        Imported=4,
    }
}
