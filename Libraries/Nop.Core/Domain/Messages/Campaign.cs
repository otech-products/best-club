﻿using System;

namespace Nop.Core.Domain.Messages
{
    /// <summary>
    /// Represents a campaign
    /// </summary>
    public partial class Campaign : BaseEntity
    {
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// Gets or sets the subject
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the body
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Gets or sets the store identifier  which subscribers it will be sent to; set 0 for all newsletter subscribers
        /// </summary>
        public int StoreId { get; set; }

        /// <summary>
        /// Gets or sets the customer role identifier  which subscribers it will be sent to; set 0 for all newsletter subscribers
        /// </summary>
        public int CustomerRoleId { get; set; }

        /// <summary>
        /// Gets or sets the date and time of instance creation
        /// </summary>
        public DateTime? CreatedOnUtc { get; set; }

        //public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Gets or sets the date and time in UTC before which this email should not be sent
        /// </summary>
        public DateTime? DontSendBeforeDateUtc { get; set; }

        public string StreetAddress { get; set; }
        public string StreetAddress2 { get; set; }

        public string city { get; set; }

        public string company { get; set; }

        public int? MaxPoints { get; set; }

        public int? MinPoints { get; set; }

        public string Gender { get; set; }

        //  [NopResourceDisplayName("Account.Fields.Gender")]
        public bool GenderEnabled { get; set; }
    

       // [NopResourceDisplayName("Account.Fields.Company")]
        public bool CompanyEnabled { get; set; }

        //[NopResourceDisplayName("Account.Fields.StreetAddress")]
        public bool StreetAddressEnabled { get; set; }

       // [NopResourceDisplayName("Account.Fields.StreetAddress2")]
        public bool StreetAddress2Enabled { get; set; }

        public bool DateOfBirthEnabled { get; set; }

        public int gift1 { get; set; }

        public int gift2 { get; set; }


        public int gift3 { get; set; }

        public int gift4 { get; set; }

        public int origin { get; set; }
        public DateTime? Date2 { get; set; }

        public DateTime? Date3 { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public DateTime? CreatedTo { get; set; }
        public DateTime? CreatedOn { get; set; }

        public DateTime? DateOfBirthTo { get; set; }

        public DateTime? Date2To { get; set; }

        public DateTime? Date3To { get; set; }
        public bool withName { get; set; }

        public bool SmsPhoneHeader { get; set; }
        public int NumOfSent { get; set; }

        public bool ezDevice { get; set; }

        public bool facebook { get; set; }
        public bool instagram { get; set; }
        public bool website { get; set; }
        public bool internet { get; set; }
        public bool legacyDb { get; set; }

        public bool other { get; set; }

      public bool multiOrigin { get; set; }

        public string checkbox1 { get; set; }
        public string checkbox2 { get; set; }
        public string checkbox3 { get; set; }
        public string checkbox4 { get; set; }
        public string checkbox5 { get; set; }

        public int isSent { get; set; }

        public DateTime? SentOn { get; set; }


        // public bool allOrigin { get; set; }
    }
}
