﻿using System;

namespace Nop.Core.Domain.Messages
{
    /// <summary>
    /// Represents NewsLetterSubscription entity
    /// </summary>
    public partial class NewsLetterSubscription : BaseEntity
    {
        public int firstOrigin { get; set; }
        public string FirstName { get; set; }

        public string LastName { get; set; }

       // public string Gender { get; set; }
        /// <summary>
        /// Gets or sets the newsletter subscription GUID
        /// </summary>
        public Guid? NewsLetterSubscriptionGuid { get; set; }

        /// <summary>
        /// Gets or sets the subcriber email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether subscription is active
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets the store identifier in which a customer has subscribed to newsletter
        /// </summary>
        public int StoreId { get; set; }

        public string StoreName { get; set; }

        /// <summary>
        /// Gets or sets the date and time when subscription was created
        /// </summary>
        public DateTime? CreatedOnUtc { get; set; }

        public DateTime? ConfirmedOnUtc { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public DateTime? DateOfBirthWithYear{ get; set; }

        public DateTime? Date2 { get; set; }

        public DateTime? Date3 { get; set; }

        public DateTime? lastVisit { get; set; }

        public string Gender { get; set; }

        public string phone { get; set; }

        public string company { get; set; }

        public string StreetAddress { get; set; }
        public string StreetAddress2 { get; set; }

        public string City { get; set; }

        public int points { get; set; }

        public bool gift1 { get; set; }
        public DateTime? gift1EndDate { get; set; }
        public DateTime? gift2EndDate { get; set; }
        public DateTime? gift3EndDate { get; set; }
        public DateTime? gift4EndDate { get; set; }

        public bool gift2 { get; set; }
        public bool gift3 { get; set; }
        public bool gift4 { get; set; }

       

        public int origin { get; set; }
        /// <summary>
        /// /////// ad to db 4.4
        /// </summary>
        public string LastIpAddress { get; set; }

        public string ownerIpAddress { get; set; }

        public string confirmIpAddress { get; set; }


        public int visits { get; set; }

        public int gift1SmsCnt { get; set; }
        public int gift2SmsCnt { get; set; }
        public int gift3SmsCnt { get; set; }
        public int gift4SmsCnt { get; set; }

        public int routineVisitSmsCnt { get; set; }

        public int routinegPointsSmsCnt { get; set; }


        public string checkbox1 { get; set; }
        public string checkbox2 { get; set; }
        public string checkbox3 { get; set; }
        public string checkbox4 { get; set; }
        public string checkbox5 { get; set; }

        public string RealEmail { get; set; }








    }
}
