﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Stores
{
    public partial class Gift : BaseEntity
    {
        public int CustomerCount { get; set; }

        public string Name { get; set; }


        /// <summary>
        /// Gets or sets the subject
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the body
        /// </summary>
       

        /// <summary>
        /// Gets or sets the store identifier  which subscribers it will be sent to; set 0 for all newsletter subscribers
        /// </summary>
        public int StoreId { get; set; }

        /// <summary>
        /// Gets or sets the customer role identifier  which subscribers it will be sent to; set 0 for all newsletter subscribers
        /// </summary>
        public int CustomerRoleId { get; set; }

        /// <summary>
        /// Gets or sets the date and time of instance creation
        /// </summary>
        public DateTime? CreatedOnUtc { get; set; }

        //public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Gets or sets the date and time in UTC before which this email should not be sent
        /// </summary>
        public DateTime? EndDate { get; set; }

        public string StreetAddress { get; set; }
        public string StreetAddress2 { get; set; }

        public string city { get; set; }

        public string company { get; set; }

        public int? MaxPoints { get; set; }

        public int? MinPoints { get; set; }

        public string Gender { get; set; }

        //  [NopResourceDisplayName("Account.Fields.Gender")]
        public bool GenderEnabled { get; set; }


        // [NopResourceDisplayName("Account.Fields.Company")]
        public bool CompanyEnabled { get; set; }

        //[NopResourceDisplayName("Account.Fields.StreetAddress")]
        public bool StreetAddressEnabled { get; set; }

        // [NopResourceDisplayName("Account.Fields.StreetAddress2")]
        public bool StreetAddress2Enabled { get; set; }

        public bool DateOfBirthEnabled { get; set; }

      

        public string giftVal { get; set; }



        public int origin { get; set; }
        public DateTime? Date2 { get; set; }

        public DateTime? Date3 { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public DateTime? CreatedOnUtcTo { get; set; }

        public DateTime? DateOfBirthTo { get; set; }

        public DateTime? Date2To { get; set; }

        public DateTime? Date3To { get; set; }
        public bool withName { get; set; }

        public bool SmsPhoneHeader { get; set; }

        public bool gift1Enabled { get; set; }

        public bool gift2Enabled { get; set; }

        public bool gift3Enabled { get; set; }

        public bool gift4Enabled { get; set; }
        public string gift1Name { get; set; }

        public string gift2Name { get; set; }

        public string gift3Name { get; set; }

        public string gift4Name { get; set; }
        public string gift { get; set; }


    }
}
