using Nop.Core.Domain.Localization;
using System;

namespace Nop.Core.Domain.Stores
{
    /// <summary>
    /// Represents a store
    /// </summary>
    public partial class Store : BaseEntity, ILocalizedEntity
    {

        public bool isFacebook { get; set; }
        /// <summary>
        /// Gets or sets the store name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the store URL
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether SSL is enabled
        /// </summary>
        public bool SslEnabled { get; set; }

        /// <summary>
        /// Gets or sets the store secure URL (HTTPS)
        /// </summary>
        public string SecureUrl { get; set; }

        /// <summary>
        /// Gets or sets the comma separated list of possible HTTP_HOST values
        /// </summary>
        public string Hosts { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the default language for this store; 0 is set when we use the default language display order
        /// </summary>
        public int DefaultLanguageId { get; set; }

        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        public int DisplayOrder { get; set; }

        /// <summary>
        /// Gets or sets the company name
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Gets or sets the company address
        /// </summary>
        public string CompanyAddress { get; set; }

        /// <summary>
        /// Gets or sets the store phone number
        /// </summary>
        public string CompanyPhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets the company VAT (used in Europe Union countries)
        /// </summary>
        public string CompanyVat { get; set; }

        public int NumberOfCustomers { get; set; }

        public string SmsUserName { get; set; }

        public string SmsPass { get; set; }

        public string welcomeMsg { get; set; }

        public string MsgLastVisit { get; set; }
        public string periodicMsg1 { get; set; }
        public string periodicMsg2 { get; set; }

        public string periodicMsg3 { get; set; }
        public string periodicMsg4 { get; set; }
        public string contactMsg { get; set; }

        public string contactMsgMonth { get; set; }

        public string weeklyCustomerCount { get; set; }
        public int weeklyCustomerCountInt { get; set; }

        public bool isPoints { get; set; }

        public bool isGifts { get; set; }

        public string preMsg { get; set; }

        public bool gift1 { get; set; }

        public bool gift2 { get; set; }

        public bool gift3 { get; set; }
        public bool gift4 { get; set; }
        public int pointImpsSum { get; set; }

        

        public int pointImpsVisits { get; set; }

        public float sum2PointsCoefficient { get; set; }

        public string pointsMsg { get; set; }

        public int visits { get; set; }
        public int gift1Imps { get; set; }

        public int gift2Imps { get; set; }

        public int gift3Imps { get; set; }

        public int gift4Imps { get; set; }

        public int monthlyVisits { get; set; }

        public int weeklyVisits { get; set; }

        public int pointsOnReg { get; set; }
        public bool gift1OnReg { get; set; }
        public bool gift2OnReg { get; set; }

        public int LogoPictureId { get; set; }
        public bool StoreClosed { get; set; }
        public string DefaultStoreTheme { get; set; }
        public bool DateOfBirthEnabled { get; set; }
        public bool DateOfBirthRequired { get; set; }
        public bool PhoneEnabled { get; set; }
        public bool PhoneRequired { get; set; }
       
        public bool HoneypotEnabled { get; set; }
        public bool GenderEnabled { get; set; }
        public bool captchaEnabled { get; set; }
        public bool SmsOnReg { get; set; }
        public bool SmsOnBday { get; set; }
        public bool SmsOnDate2 { get; set; }
        public bool gift2OnBday { get; set; }
        public bool gift2OnDate2 { get; set; }
        public string gift1Name { get; set; }
        public string gift2Name { get; set; }
        public string gift3Name { get; set; }
        public string gift4Name { get; set; }
        public int totalCustomersPoints { get; set; }
        public string pointsMsg2 { get; set; }
        public int? monthlyCustomerCount { get; set; }

        public bool date2Enabled { get; set; }
        public string date2Name { get; set; }
        public bool date3Enabled { get; set; }
        public string date3Name { get; set; }
        public bool date2Required { get; set; }
        public bool date3Required { get; set; }

        public string contactPhone { get; set; }

        public string BdaySmS { get; set; }
        public bool Active { get; set; }
        public int rutineSmSTimeOffset { get; set; }
         public bool SendSmSFromLastVisit { get; set; }
        public int MonthsFromLastVisit { get; set; }

        public string smsRemovalLink { get; set; }
        public string rgb { get; set; }

        public string preFirstNameMsg { get; set; }


        public string gift1Msg { get; set; }
        public string gift2Msg { get; set; }
        public string gift3Msg { get; set; }
        public string gift4Msg { get; set; }

        public int face2Ez { get; set; }
        public int Instagram2Ez { get; set; }
        public int Website2Ez { get; set; }

        public int internet2Ez { get; set; }

        public int Legacy2Ez { get; set; }

        public int Other2Ez { get; set; }

        public bool days2GiftEnd { get; set; }

        public string days2GiftEndMsg { get; set; }

        public bool gift1OnBday { get; set; }

        public  bool useDefaultMsg { get; set; }
        public DateTime? monthSatrtMsgSentDate { get; set; }

        public DateTime? monthEndMsgSentDate { get; set; }

        public bool multiField { get; set; }
        public bool check1 { get; set; }
        public bool check2 { get; set; }
        public bool check3 { get; set; }
        public bool check4 { get; set; }
        public bool check5 { get; set; }

        public string multiFieldName { get; set; }
        public string check1String { get; set; }
        public string check2String { get; set; }
        public string check3String { get; set; }
        public string check4String { get; set; }
        public string check5String { get; set; }




        // msg loogin

        public DateTime? lastBdayRutine { get; set; }
        public int BdayMsgSentInLastRutine { get; set; }

        public DateTime? lastDate2Rutine { get; set; }
        public int Date2MsgSentInLastRutine { get; set; }



        ////29.10.17 - start////////////////////
        public bool gift1ValidityPeriod { get; set; }
        public bool gift2ValidityPeriod { get; set; }
        public bool gift3ValidityPeriod { get; set; }
        public bool gift4ValidityPeriod { get; set; }



        public int gift1ValidityDays { get; set; }

        public int gift2ValidityDays { get; set; }

        public int gift3ValidityDays { get; set; }

        public int gift4ValidityDays { get; set; }


        public int gift1NotImp { get; set; }

        public int gift2NotImp { get; set; }

        public int gift3NotImp { get; set; }

        public int gift4NotImp { get; set; }

        public string randString { get; set; }

        public int weeklyTarget { get; set; }


        ////29.10.17 - end////////////////////


        public string Api_Custom_1 { get; set; }
        public string Api_Custom_2 { get; set; }
        public string Api_user { get; set; }
        public string Api_pass { get; set; }

        public bool AllowRealEmail { get; set; }
        public string  RealEmail { get; set; }


        public bool AllowTZ { get; set; }
        public bool SearchByTZ { get; set; }

        public int TZ { get; set; }












    }
}
