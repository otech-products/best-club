using System;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Messages;

namespace Nop.Core.Domain.Customers
{
    /// <summary>
    /// Represents a reward point history entry
    /// </summary>
    public partial class RewardPointsHistory : BaseEntity
    {
        /// <summary>
        /// Gets or sets the customer identifier
        /// </summary>
        /// 
        public int NewsLetterSubscriptionId { get; set; }

        /// <summary>
        /// Gets or sets the store identifier in which these reward points were awarded or redeemed
        /// </summary>
        public int StoreId { get; set; }

        /// <summary>
        /// Gets or sets the points redeemed/added
        /// </summary>
        public int incPoints { get; set; }
        public int Points { get; set; }
        public int dicPoints { get; set; }

        /// <summary>
        /// Gets or sets the points balance
        /// </summary>
        public int PointsBalance { get; set; }

        /// <summary>
        /// Gets or sets the used amount
        /// </summary>
        public decimal UsedAmount { get; set; }

        /// <summary>
        /// Gets or sets the message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the date and time of instance creation
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }


        public string Employee { get; set; }

        public string GiftName { get; set; }
        //public string Gift2Name { get; set; }
        //public string Gift3Name { get; set; }
        //public string Gift4Name { get; set; }
         public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }


        /// <summary>
        /// Gets or sets the customer
        /// </summary>
        public virtual NewsLetterSubscription NewsLetterSubscription { get; set; }
    }
}
