﻿using System;
using Nop.Services.Logging;
using Nop.Services.Tasks;
using Nop.Services.Stores;
using Nop.Core.Domain.Messages;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nop.Services.Messages
{
    /// <summary>
    /// Represents a task for sending queued message 
    /// </summary>
    public partial class DailyHouseKeepingTask : ITask
    {
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
        private readonly IStoreService _storeService;

        public DailyHouseKeepingTask(IQueuedEmailService queuedEmailService, IStoreService storeService, INewsLetterSubscriptionService newsLetterSubscriptionService,
            IEmailSender emailSender, ILogger logger)
        {
            this._queuedEmailService = queuedEmailService;
            this._emailSender = emailSender;
            this._logger = logger;
            this._newsLetterSubscriptionService = newsLetterSubscriptionService;
            this._storeService = storeService;


        }

        /// <summary>
        /// Executes a task
        /// </summary>
        public virtual void Execute()
        {
            IList<NewsLetterSubscription> subs = null;
            var stores = _storeService.GetAllStores();
            int bdayStore = 0;
            int date2Store = 0;
            int visitStore = 0;
            string timeToSend = "";
            //Dictionary<string, string> PhoneList = new Dictionary<string, string>() ;

            foreach (var store in stores)
            {
                //PhoneList.Clear();
                if (store.Active)
                {

                    if (DateTime.Today.DayOfWeek == DayOfWeek.Saturday)
                    {
                        store.weeklyCustomerCount = store.weeklyCustomerCountInt.ToString();
                        store.weeklyCustomerCountInt = 0;
                        _storeService.UpdateStore(store);

                    }
                   

                    if ((store.gift1ValidityPeriod || store.gift2ValidityPeriod) == false)
                    { continue; }
                    ////// enters code below only if condition above is spacified

                    if (store.rutineSmSTimeOffset > 0)
                    {
                        var h = DateTime.Now.AddHours(store.rutineSmSTimeOffset).ToString("HH:mm");
                        var d = DateTime.Now.AddHours(store.rutineSmSTimeOffset).ToString();
                        var d1 = d.Split('/');
                        var d2 = d1[0];
                        var d3 = d1[1];
                        var d4 = DateTime.Now.AddHours(store.rutineSmSTimeOffset).ToString("yyyy");
                        timeToSend = d3 + "/" + d2 + "/" + d4 + " " + h;
                    }

                    try
                    {
                        subs = _newsLetterSubscriptionService.GetNewsLetterSubscriptions(store.Id);
                    }
                    catch (Exception e)
                    {
                        var x = e.Message;
                        var y = e.Data;
                        var z = e.InnerException;
                        continue;
                    }
                    if (subs.Count == 0) continue;

                    foreach (var sub in subs)
                    {
                        //Task<bool> isSubActive = _emailSender.checkPhoneForBlockAsync(sub.Email, store.SmsUserName, store.SmsPass);

                        if (sub.Active)
                        {

                            if (sub.gift1EndDate.HasValue)
                            {
                                if (DateTime.Now >= sub.gift1EndDate.Value  )
                                {
                                    sub.gift1 = false; sub.gift1SmsCnt = 0; sub.gift1EndDate = null;
                                    store.gift1NotImp -= 1;
                                }
                            }

                            if (sub.gift2EndDate.HasValue)
                            {
                                if (sub.gift2EndDate.Value >= DateTime.Now)
                                {
                                    sub.gift2 = false; sub.gift2SmsCnt = 0; sub.gift2EndDate = null;
                                    store.gift2NotImp -= 1;
                                }
                            }

                            if (sub.gift3EndDate.HasValue)
                            {
                                if (sub.gift3EndDate.Value >= DateTime.Now)
                                {
                                    sub.gift3 = false; sub.gift3SmsCnt = 0; sub.gift3EndDate = null;
                                    store.gift3NotImp -= 1;
                                }
                            }

                            if (sub.gift4EndDate.HasValue)
                            {
                                if (sub.gift4EndDate.Value >= DateTime.Now)
                                {
                                    sub.gift4 = false; sub.gift4SmsCnt = 0; sub.gift4EndDate = null;
                                    store.gift4NotImp -= 1;
                                }
                            }


                            _newsLetterSubscriptionService.UpdateNewsLetterSubscription(sub);










                        }
                    }
                    //store.active logic
                  
                }
            }
        }
    }
}
