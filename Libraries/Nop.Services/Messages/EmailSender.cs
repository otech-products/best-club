﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using Nop.Core.Domain.Messages;
using Nop.Services.Media;
using System.Text;
using System.Web;
using Nop.Services.Logging;
using System.Xml;
using System.Threading.Tasks;

namespace Nop.Services.Messages
{
    /// <summary>
    /// Email sender
    /// </summary>
    public partial class EmailSender : IEmailSender
    {
        private readonly IDownloadService _downloadService;
        private readonly ILogger _logger;

        public EmailSender(IDownloadService downloadService, ILogger logger)
        {
            this._downloadService = downloadService;
            this._logger = logger;

        }

        /// <summary>
        /// Sends an email
        /// </summary>
        /// <param name="emailAccount">Email account to use</param>
        /// <param name="subject">Subject</param>
        /// <param name="body">Body</param>
        /// <param name="fromAddress">From address</param>
        /// <param name="fromName">From display name</param>
        /// <param name="toAddress">To address</param>
        /// <param name="toName">To display name</param>
        /// <param name="replyTo">ReplyTo address</param>
        /// <param name="replyToName">ReplyTo display name</param>
        /// <param name="bcc">BCC addresses list</param>
        /// <param name="cc">CC addresses list</param>
        /// <param name="attachmentFilePath">Attachment file path</param>
        /// <param name="attachmentFileName">Attachment file name. If specified, then this file name will be sent to a recipient. Otherwise, "AttachmentFilePath" name will be used.</param>
        /// <param name="attachedDownloadId">Attachment download ID (another attachedment)</param>
        /// <param name="headers">Headers</param>
        public virtual void SendEmail(EmailAccount emailAccount, string subject, string body,
            string fromAddress, string fromName, string toAddress, string toName,
             string replyTo = null, string replyToName = null,
            IEnumerable<string> bcc = null, IEnumerable<string> cc = null,
            string attachmentFilePath = null, string attachmentFileName = null,
            int attachedDownloadId = 0, IDictionary<string, string> headers = null)
        {
            var message = new MailMessage();
            //from, to, reply to
            message.From = new MailAddress(fromAddress, fromName);
            message.To.Add(new MailAddress(toAddress, toName));
            if (!String.IsNullOrEmpty(replyTo))
            {
                message.ReplyToList.Add(new MailAddress(replyTo, replyToName));
            }

            //BCC
            if (bcc != null)
            {
                foreach (var address in bcc.Where(bccValue => !String.IsNullOrWhiteSpace(bccValue)))
                {
                    message.Bcc.Add(address.Trim());
                }
            }

            //CC
            if (cc != null)
            {
                foreach (var address in cc.Where(ccValue => !String.IsNullOrWhiteSpace(ccValue)))
                {
                    message.CC.Add(address.Trim());
                }
            }

            //content
            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = true;

            //headers
            if (headers != null)
                foreach (var header in headers)
                {
                    message.Headers.Add(header.Key, header.Value);
                }

            //create the file attachment for this e-mail message
            if (!String.IsNullOrEmpty(attachmentFilePath) &&
                File.Exists(attachmentFilePath))
            {
                var attachment = new Attachment(attachmentFilePath);
                attachment.ContentDisposition.CreationDate = File.GetCreationTime(attachmentFilePath);
                attachment.ContentDisposition.ModificationDate = File.GetLastWriteTime(attachmentFilePath);
                attachment.ContentDisposition.ReadDate = File.GetLastAccessTime(attachmentFilePath);
                if (!String.IsNullOrEmpty(attachmentFileName))
                {
                    attachment.Name = attachmentFileName;
                }
                message.Attachments.Add(attachment);
            }
            //another attachment?
            if (attachedDownloadId > 0)
            {
                var download = _downloadService.GetDownloadById(attachedDownloadId);
                if (download != null)
                {
                    //we do not support URLs as attachments
                    if (!download.UseDownloadUrl)
                    {
                        string fileName = !String.IsNullOrWhiteSpace(download.Filename) ? download.Filename : download.Id.ToString();
                        fileName += download.Extension;


                        var ms = new MemoryStream(download.DownloadBinary);                        
                        var attachment = new Attachment(ms, fileName);
                        //string contentType = !String.IsNullOrWhiteSpace(download.ContentType) ? download.ContentType : "application/octet-stream";
                        //var attachment = new Attachment(ms, fileName, contentType);
                        attachment.ContentDisposition.CreationDate = DateTime.UtcNow;
                        attachment.ContentDisposition.ModificationDate = DateTime.UtcNow;
                        attachment.ContentDisposition.ReadDate = DateTime.UtcNow;
                        message.Attachments.Add(attachment);                        
                    }
                }
            }

            //send email
            using (var smtpClient = new SmtpClient())
            {
                smtpClient.UseDefaultCredentials = emailAccount.UseDefaultCredentials;
                smtpClient.Host = emailAccount.Host;
                smtpClient.Port = emailAccount.Port;
                smtpClient.EnableSsl = emailAccount.EnableSsl;
                smtpClient.Credentials = emailAccount.UseDefaultCredentials ? 
                    CredentialCache.DefaultNetworkCredentials :
                    new NetworkCredential(emailAccount.Username, emailAccount.Password);
                smtpClient.Send(message);
            }
        }
        public void SendSmS(string PhoneList, string Msg, string userName, string password, string timeToSend, string sender, string  smsRemovalLink, string storeId = "0")
        {



            if (string.IsNullOrWhiteSpace(Msg))
                return;
           
            string msg3 = Msg + "\n" + smsRemovalLink;
           
            string messageText = System.Security.SecurityElement.Escape(msg3);
            string messageInterval = "0";



            StringBuilder sbXml = new StringBuilder();
            sbXml.Append("<Inforu>");
            sbXml.Append("<User>");
            sbXml.Append("<Username>" + userName + "</Username>");
            sbXml.Append("<Password>" + password + "</Password>");
            sbXml.Append("</User>");
            sbXml.Append("<Content Type=\"sms\">");
            sbXml.Append("<Message>" + messageText + "</Message>");
            sbXml.Append("</Content>");
            sbXml.Append("<Recipients>");
            sbXml.Append("<PhoneNumber>" + PhoneList + "</PhoneNumber>");
            sbXml.Append("</Recipients>");
            sbXml.Append("<Settings>");
            sbXml.Append("<Sender>" + sender + "</Sender>");
            sbXml.Append("<MessageInterval>" + messageInterval + "</MessageInterval>");
            sbXml.Append("<TimeToSend>" + timeToSend+ "</TimeToSend>");
            sbXml.Append("</Settings>");
            sbXml.Append("</Inforu >");
            string strXML = HttpUtility.UrlEncode(sbXml.ToString(), System.Text.Encoding.UTF8);
            string result = PostDataToURL("http://api.inforu.co.il/SendMessageXml.ashx", "InforuXML=" + strXML, sbXml.ToString(), storeId);
            
        }

        public void SendMasSmS(Dictionary<string, string> PhoneList, string Msg, string userName, string password, DateTime? timeToSend, string PreMsg = null,string smsRemovalLink = "", string storeId = "0", Dictionary<string, string> pointsNlsList = null)
        {
            if (string.IsNullOrWhiteSpace(Msg))
                return;
            // string messageText = System.Security.SecurityElement.Escape(Msg);
            string timeToSend2;
            if (!timeToSend.HasValue)
            {
                timeToSend2 = "";
            }
            else
            {
                //timeToSend2 = timeToSend.Value.ToString("dd/mm/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                var h = timeToSend.Value.ToString("HH:mm");
                var d = timeToSend.Value.ToString();
                var d1 = d.Split('/');
                var d2 = d1[0];
                var d3 = d1[1];
                var d4 = timeToSend.Value.ToString("yyyy");
                timeToSend2 = d3 + "/" + d2 + "/" + d4 + " " + h;

            }
            string messageInterval = "0";
            string strXML;
            string result;
            string sender2;
          
             sender2 = PreMsg;
           
            string messageText;
            string tempMsg;
            StringBuilder sbXml = new StringBuilder();
            sbXml.Append("<InforuRoot>");
            int i = 0;
            bool flag99 = true;
           
                foreach (var item in PhoneList)
                {

                    //tempMsg = PreMsg + " " + item.Value + " " + Msg;
                    tempMsg = Msg.Replace("$N$", item.Value) + "\n" + smsRemovalLink;
                tempMsg = tempMsg.Replace("$P$", pointsNlsList[item.Key]);
               messageText = System.Security.SecurityElement.Escape(tempMsg);
                    sbXml.Append("<Inforu>");
                    sbXml.Append("<User>");
                    sbXml.Append("<Username>" + userName + "</Username>");
                    sbXml.Append("<Password>" + password + "</Password>");
                    sbXml.Append("</User>");
                    sbXml.Append("<Content Type=\"sms\">");
                    sbXml.Append("<Message>" + messageText + "</Message>");
                    sbXml.Append("</Content>");
                    sbXml.Append("<Recipients>");
                    sbXml.Append("<PhoneNumber>" + item.Key + "</PhoneNumber>");
                    sbXml.Append("</Recipients>");
                    sbXml.Append("<Settings>");
                    sbXml.Append("<Sender>" + sender2 + "</Sender>");
                    sbXml.Append("<MessageInterval>" + messageInterval + "</MessageInterval>");
                    sbXml.Append("<TimeToSend>" + timeToSend2 +"</TimeToSend>");
                    sbXml.Append("</Settings>");
                    sbXml.Append("</Inforu >");
                    if (i % 99 == 0 && i != 0)
                    {
                        sbXml.Append("</InforuRoot>");

                        strXML = HttpUtility.UrlEncode(sbXml.ToString(), System.Text.Encoding.UTF8);
                        result = PostDataToURL("http://api.inforu.co.il/SendMessageXml.ashx", "InforuXML=" + strXML, sbXml.ToString(),storeId);
                        sbXml.Clear();
                        sbXml.Append("<InforuRoot>");


                    }
                i++;
                }

                 sbXml.Append("</InforuRoot>");
                 strXML = HttpUtility.UrlEncode(sbXml.ToString(), System.Text.Encoding.UTF8);
                result = PostDataToURL("http://api.inforu.co.il/SendMessageXml.ashx", "InforuXML=" + strXML, sbXml.ToString(),storeId);
                sbXml.Clear();
            
            //else
            //{
            //    foreach (var item in PhoneList)
            //    {

            //        tempMsg = Msg;
            //        messageText = System.Security.SecurityElement.Escape(tempMsg);
            //        sbXml.Append("<Inforu>");
            //        sbXml.Append("<User>");
            //        sbXml.Append("<Username>" + userName + "</Username>");
            //        sbXml.Append("<Password>" + password + "</Password>");
            //        sbXml.Append("</User>");
            //        sbXml.Append("<Content Type=\"sms\">");
            //        sbXml.Append("<Message>" + messageText + "</Message>");
            //        sbXml.Append("</Content>");
            //        sbXml.Append("<Recipients>");
            //        sbXml.Append("<PhoneNumber>" + item.Key + "</PhoneNumber>");
            //        sbXml.Append("</Recipients>");
            //        sbXml.Append("<Settings>");
            //        sbXml.Append("<Sender>" + sender + "</Sender>");
            //        sbXml.Append("<MessageInterval>" + messageInterval + "</MessageInterval>");
            //        sbXml.Append("<TimeToSend>" + timeToSend2+ "</TimeToSend>");
            //        sbXml.Append("</Settings>");
            //        sbXml.Append("</Inforu >");
            //        if (i % 80 == 0)
            //        {
            //            sbXml.Append("</InforuRoot>");

            //            strXML = HttpUtility.UrlEncode(sbXml.ToString(), System.Text.Encoding.UTF8);
            //            result = PostDataToURL("http://api.inforu.co.il/SendMessageXml.ashx", "InforuXML=" + strXML);
            //            sbXml.Clear();
            //            sbXml.Append("<InforuRoot>");


            //        }
            //    }


            //    strXML = HttpUtility.UrlEncode(sbXml.ToString(), System.Text.Encoding.UTF8);
            //    result = PostDataToURL("http://api.inforu.co.il/SendMessageXml.ashx", "InforuXML=" + strXML);
            //    sbXml.Clear();
            //}


        }
        public async Task<bool>  checkPhoneForBlockAsync (string phone,string userName,string password)
        {

            string szUrl = "http://api.inforu.co.il/WebTools/CheckIfNumberBlocked.ashx";
            string szData = "userName=" + userName + "&password=" + password + "&phoneNumber=" + phone + "&prepaidAmount=0";
            Task<string> resultAsync = PostDataToURLAsync(szUrl, szData);
            
            XmlDocument doc = new XmlDocument();
            string result = await resultAsync;
            doc.LoadXml(result);
            XmlNodeList statusNodes = doc.DocumentElement.SelectNodes("/Result/status");

            foreach (XmlNode statusNode in statusNodes)
            {
                string statusMessage = statusNode.InnerText;
                if (statusMessage == "1")
                {
                    return true;
                  
                }

                if (statusMessage == "-49")
                {
                    return false;
                }


            }
            return true;





        }

        public bool checkPhoneForBlock(string phone, string userName, string password)
        {

            string szUrl = "http://api.inforu.co.il/WebTools/CheckIfNumberBlocked.ashx";
            string szData = "userName=" + userName + "&password=" + password + "&phoneNumber=" + phone + "&prepaidAmount=0";
            string result = PostDataToURL(szUrl, szData);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(result);
            XmlNodeList statusNodes = doc.DocumentElement.SelectNodes("/Result/status");

            foreach (XmlNode statusNode in statusNodes)
            {
                string statusMessage = statusNode.InnerText;
                if (statusMessage == "1")
                {
                    return true;

                }

                if (statusMessage == "-49")
                {
                    return false;
                }


            }
            return true;





        }


        string PostDataToURL(string szUrl, string szData,string original = "",string storeId = "0")
        {
            //Setup the web request
            string szResult = string.Empty;
            WebRequest Request = WebRequest.Create(szUrl);
            Request.Timeout = 30000;
            Request.Method = "POST";
            Request.ContentType = "application/x-www-form-urlencoded";
            byte[] PostBuffer;
            try
            {
                // replacing " " with "+" according to Http post RPC
                szData = szData.Replace(" ", "+");
                //Specify the length of the buffer
                PostBuffer = Encoding.UTF8.GetBytes(szData);
                Request.ContentLength = PostBuffer.Length;
                //Open up a request stream
                Stream RequestStream = Request.GetRequestStream();
                //Write the POST data
                RequestStream.Write(PostBuffer, 0, PostBuffer.Length);
                //Close the stream
                RequestStream.Close();
                //Create the Response object
                WebResponse Response;
                Response = Request.GetResponse();
                //Create the reader for the response
                StreamReader sr = new StreamReader(Response.GetResponseStream(), Encoding.UTF8);
                //Read the response
                szResult = sr.ReadToEnd();
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(szResult);

                XmlNodeList statusNodes = doc.DocumentElement.SelectNodes("/Result/Status");

                foreach (XmlNode statusNode in statusNodes)
                {
                    string statusMessage = statusNode.InnerText;
                    if (statusMessage != "1")
                    {

                        XmlNode errorNode = doc.DocumentElement.SelectSingleNode("/Result/Status/Description");
                        _logger.Error(string.Format("storeId = {0}  -   Error sending SmS xml response xml error   -   original:{1}", storeId,original));
                        _logger.Error(string.Format("storeId = {3},  Error sending SmS. {0} - {1} -   original:{2}", statusMessage, errorNode.InnerText,storeId, original));
                    }

                }


                //Close the reader, and response
                sr.Close();
                Response.Close();

              

                return szResult;
            }
            catch (XmlException e)
            {
                _logger.Error(string.Format("storeId = {0}  -   Error sending SmS xml response xml error", storeId ));
                return szResult;
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("storeId = {0}  -  Error sending SmS - connection error", storeId));
                return szResult;
            }

        }

        async Task<string> PostDataToURLAsync(string szUrl, string szData)
        {
            //Setup the web request
            string szResult = string.Empty;
            WebRequest Request = WebRequest.Create(szUrl);
            Request.Timeout = 30000;
            Request.Method = "POST";
            Request.ContentType = "application/x-www-form-urlencoded";
            byte[] PostBuffer;
            try
            {
                // replacing " " with "+" according to Http post RPC
                szData = szData.Replace(" ", "+");
                //Specify the length of the buffer
                PostBuffer = Encoding.UTF8.GetBytes(szData);
                Request.ContentLength = PostBuffer.Length;
                //Open up a request stream
                Stream RequestStream = Request.GetRequestStream();
                //Write the POST data
                RequestStream.Write(PostBuffer, 0, PostBuffer.Length);
                //Close the stream
                RequestStream.Close();
                //Create the Response object
                WebResponse Response;
                Response = Request.GetResponse();
                //Create the reader for the response
                StreamReader sr = new StreamReader(Response.GetResponseStream(), Encoding.UTF8);
                //Read the response
                szResult = sr.ReadToEnd();
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(szResult);

                XmlNodeList statusNodes = doc.DocumentElement.SelectNodes("/Result/status");

                foreach (XmlNode statusNode in statusNodes)
                {
                    string statusMessage = statusNode.InnerText;
                    if (statusMessage != "1")
                    {

                        XmlNode errorNode = doc.DocumentElement.SelectSingleNode("/Result/status/Description");
                        _logger.Error(string.Format("Error sending SmS. {0} - {1}", statusMessage, errorNode.InnerText));
                    }

                }


                //Close the reader, and response
                sr.Close();
                Response.Close();



                return szResult;
            }
            catch (XmlException e)
            {
                _logger.Error(string.Format("Error sending SmS xml response xml error"));
                return szResult;
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("Error sending SmS - connection error"));
                return szResult;
            }

        }



    }
}
