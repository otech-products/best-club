﻿using System;
using Nop.Services.Logging;
using Nop.Services.Tasks;
using Nop.Services.Stores;
using Nop.Core.Domain.Messages;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nop.Services.Messages
{
    /// <summary>
    /// Represents a task for sending queued message 
    /// </summary>
    public partial class QueuedMessagesSendTask : ITask
    {
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
        private readonly IStoreService _storeService;

        public QueuedMessagesSendTask(IQueuedEmailService queuedEmailService, IStoreService storeService, INewsLetterSubscriptionService newsLetterSubscriptionService,
            IEmailSender emailSender, ILogger logger)
        {
            this._queuedEmailService = queuedEmailService;
            this._emailSender = emailSender;
            this._logger = logger;
            this._newsLetterSubscriptionService = newsLetterSubscriptionService;
            this._storeService = storeService;


        }

        /// <summary>
        /// Executes a task
        /// </summary>
        public virtual void Execute()
        {
            IList<NewsLetterSubscription> subs = null;
            var stores = _storeService.GetAllStores();
            int bdayStore = 0;
            int date2Store = 0;
            int visitStore = 0;
            string timeToSend = "";
            //Dictionary<string, string> PhoneList = new Dictionary<string, string>() ;

            foreach (var store in stores)
            {
                //PhoneList.Clear();
                if (store.Active)
                {
                    if(store.SmsOnBday == false)
                    { continue; }

                    bdayStore = 0;
                    date2Store = 0;
                    visitStore = 0;
                    if (store.rutineSmSTimeOffset > 0)
                    {
                        var h = DateTime.Now.AddHours(store.rutineSmSTimeOffset).ToString("HH:mm");
                        var d = DateTime.Now.AddHours(store.rutineSmSTimeOffset).ToString();
                        var d1 = d.Split('/');
                        var d2 = d1[0];
                        var d3 = d1[1];
                        var d4 = DateTime.Now.AddHours(store.rutineSmSTimeOffset).ToString("yyyy");
                        timeToSend = d3 + "/" + d2 + "/" + d4 + " " + h;
                    }

                    try
                    {
                        subs = _newsLetterSubscriptionService.GetNewsLetterSubscriptions(storeId: store.Id);//, DateOfBirthFrom: DateTime.Now.ad (DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) - DateTime.Now.Day));
                    }
                    catch (Exception e)
                    {
                        var x = e.Message;
                        var y = e.Data;
                        var z = e.InnerException;
                        continue;
                    }
                    if (subs.Count == 0) continue;

                    foreach (var sub in subs)
                    {
                        //Task<bool> isSubActive = _emailSender.checkPhoneForBlockAsync(sub.Email, store.SmsUserName, store.SmsPass);

                        if (sub.Active)
                        {


                            //smsCnt count sms sendings for end of gift period notafication

                            if (sub.gift1EndDate.HasValue) {
                                if (sub.gift1EndDate.Value <= DateTime.Now)
                                {
                                    sub.gift1 = false; sub.gift1SmsCnt = 0; sub.gift1EndDate = null;
                                }
                            }
                            if (sub.gift2EndDate.HasValue) { if (sub.gift2EndDate.Value <= DateTime.Now) { sub.gift2 = false; sub.gift2SmsCnt = 0; sub.gift2EndDate = null; } }
                            if (sub.gift3EndDate.HasValue) { if (sub.gift3EndDate.Value <= DateTime.Now) { sub.gift3 = false; sub.gift3SmsCnt = 0; sub.gift3EndDate = null; } }
                            if (sub.gift4EndDate.HasValue) { if (sub.gift4EndDate.Value <= DateTime.Now) { sub.gift4 = false; sub.gift4SmsCnt = 0; sub.gift4EndDate = null; } }

                            //if (sub.DateOfBirth.HasValue && store.gift1OnBday &&sub.DateOfBirth.Value.Month == DateTime.Now.Month 
                            //  && sub.gift1EndDate == null)// in bday month but gift not yet been given for some reason because EndDate=null
                            //  {

                            //          sub.gift1 = true;
                            //          sub.gift1EndDate = DateTime.Now.AddDays(DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) - 1);
                            //          sub.gift1EndDate = new DateTime(sub.gift1EndDate.Value.Year, sub.gift1EndDate.Value.Month, sub.gift1EndDate.Value.Day, 23, 59, 59);


                            //  }

                            if ((DateTime.Today.Day <=5 && (DateTime.Today.DayOfWeek != DayOfWeek.Saturday)) || (DateTime.Today.Day == 2 && (DateTime.Today.DayOfWeek == DayOfWeek.Sunday)))
                            {



                                if (store.SmsOnBday)
                                {
                                    if (sub.DateOfBirth.HasValue)
                                    {

                                        if (sub.DateOfBirth.Value.Month == DateTime.Now.Month)
                                        {

                                            if (store.gift1)// && !sub.gift1EndDate.HasValue)
                                            {
                                                sub.gift1 = true;
                                                sub.gift1EndDate = DateTime.Now.AddDays(DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) - DateTime.Now.Day);
                                                sub.gift1EndDate = new DateTime(sub.gift1EndDate.Value.Year, sub.gift1EndDate.Value.Month, sub.gift1EndDate.Value.Day, 23, 59, 59);
                                               // bdayStore++;
                                            }


                                            try
                                            {
                                                string msgWithNameBday = store.BdaySmS.Replace("$N$", sub.FirstName);
                                                _emailSender.SendSmS(sub.Email,
                                                    msgWithNameBday,
                                                    store.SmsUserName,
                                                   store.SmsPass,
                                                   timeToSend,
                                                   store.preMsg,
                                                   store.smsRemovalLink, store.Id.ToString());
                                                bdayStore++;

                                            }
                                            catch { }


                                            continue;

                                        }
                                    }
                                }
                                //if (store.SmsOnDate2)
                                //{
                                //    if (sub.Date2.HasValue)
                                //    {

                                //        if (sub.Date2.Value.Month == DateTime.Now.Month)
                                //        {

                                //            if (store.gift2OnDate2 && !sub.gift2EndDate.HasValue)
                                //            {
                                //                sub.gift2 = true;
                                //                sub.gift2EndDate = DateTime.Now.AddDays(DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) - DateTime.Now.Day);
                                //                sub.gift2EndDate = new DateTime(sub.gift2EndDate.Value.Year, sub.gift2EndDate.Value.Month, sub.gift2EndDate.Value.Day, 23, 59, 59);

                                //            }


                                //            try
                                //            {
                                //                string msgWithNameDate2 = store.gift2Msg.Replace("$N$", sub.FirstName);
                                //                _emailSender.SendSmS(sub.Email,
                                //                    msgWithNameDate2,
                                //                    store.SmsUserName,
                                //                   store.SmsPass,
                                //                   timeToSend,
                                //                   store.preMsg,
                                //                   store.smsRemovalLink, store.Id.ToString());
                                //                date2Store++;

                                //            }
                                //            catch { }

                                //        }

                                //    }
                                //}
                                //if (store.SendSmSFromLastVisit && !store.isPoints)
                                //{
                                //    // send last visit msg without points
                                //    if (sub.lastVisit.HasValue)
                                //    {

                                //        if (sub.lastVisit.Value.Month == DateTime.Now.Month + store.MonthsFromLastVisit || sub.lastVisit.Value.Month == DateTime.Now.Month + store.MonthsFromLastVisit + 1)
                                //        {
                                //            // in the target month from last visit and the one after it
                                //            try
                                //            {
                                //                string MsgLastVisit;
                                //                MsgLastVisit = store.MsgLastVisit.Replace("$N$", sub.FirstName);

                                //                _emailSender.SendSmS(sub.Email,
                                //                    MsgLastVisit,
                                //                    store.SmsUserName,
                                //                   store.SmsPass,
                                //                   timeToSend,
                                //                   store.preMsg, store.smsRemovalLink, store.Id.ToString());
                                //            }
                                //            catch { }

                                //            visitStore++;
                                //            continue;


                                //        }
                                //    }
                                //}

                                //if (store.SendSmSFromLastVisit && store.isPoints)
                                //{
                                //    // send last visit msg with points
                                //    if (sub.lastVisit.HasValue && sub.points > 0)
                                //    {

                                //        if (sub.lastVisit.Value.Month == DateTime.Now.Month + store.MonthsFromLastVisit)
                                //        {
                                //            string msggg = store.MsgLastVisit;
                                //            msggg = msggg.Replace("$P$", sub.points.ToString());
                                //            msggg = msggg.Replace("$N$", sub.FirstName);
                                //            try
                                //            {
                                //                _emailSender.SendSmS(sub.Email,
                                //                    store.MsgLastVisit,
                                //                    store.SmsUserName,
                                //                   store.SmsPass,
                                //                   timeToSend,
                                //                   store.preMsg, store.smsRemovalLink, store.Id.ToString());
                                //            }
                                //            catch
                                //            {

                                //            }

                                //            visitStore++;
                                //            continue;


                                //        }
                                //    }
                                //}





                            }
                            // sub.Active = await isSubActive;

                            //if (store.gift1OnBday && (DateTime.Now.Day >= 20 && store.monthEndMsgSentDate.HasValue)
                            //          && store.monthEndMsgSentDate.Value.Year == DateTime.Now.Year
                            //          && store.monthEndMsgSentDate.Value.Month < DateTime.Now.Month)
                            if (DateTime.Now.DayOfWeek != DayOfWeek.Saturday)
                            {
                                if (store.gift1 && (DateTime.Now.Day >= 20))
                                {
                                    if (sub.gift1 == true && sub.gift1SmsCnt == 0 && sub.gift1EndDate.HasValue && (sub.gift1EndDate.Value.Month == DateTime.Now.Month))
                                    {
                                        var msg2 = store.days2GiftEndMsg.Replace("$GIFT$", store.gift1Name);
                                        msg2 = msg2.Replace("$N$", sub.FirstName);
                                        try
                                        {
                                            _emailSender.SendSmS(sub.Email,
                                                      msg2,
                                                      store.SmsUserName,
                                                     store.SmsPass,
                                                     timeToSend,
                                                     store.preMsg);
                                        }
                                        catch
                                        {

                                        }

                                        sub.gift1SmsCnt += 1;
                                    }


                                    //{
                                    //    if (sub.gift1EndDate.HasValue && sub.gift1EndDate.Value.DayOfYear - DateTime.Now.DayOfYear <= store.days2GiftEnd.Value
                                    //            && sub.gift1SmsCnt == 0 && sub.gift1EndDate.Value.DayOfYear - DateTime.Now.DayOfYear >= 0)
                                    //    {

                                    //        var msg2 = store.days2GiftEndMsg.Replace("$GIFT$", store.gift1Name);
                                    //        msg2 = msg2.Replace("$N$", sub.FirstName);
                                    //        msg2 = msg2.Replace("$DAYS$", (sub.gift1EndDate.Value.DayOfYear - DateTime.Now.DayOfYear).ToString());
                                    //        try
                                    //        {
                                    //            _emailSender.SendSmS(sub.Email,
                                    //                      msg2,
                                    //                      store.SmsUserName,
                                    //                     store.SmsPass,
                                    //                     timeToSend,
                                    //                     store.preMsg);
                                    //        }
                                    //        catch
                                    //        {

                                    //        }

                                    //        sub.gift1SmsCnt += 1;

                                    //    }

                                    //    if (sub.gift2EndDate.HasValue && sub.gift2EndDate.Value.DayOfYear - DateTime.Now.DayOfYear <= store.days2GiftEnd.Value && sub.gift2SmsCnt == 0 && sub.gift2EndDate.Value.DayOfYear - DateTime.Now.DayOfYear >= 0)
                                    //    {

                                    //        var msg2 = store.days2GiftEndMsg.Replace("$GIFT$", store.gift2Name);
                                    //        msg2 = msg2.Replace("$N$", sub.FirstName);
                                    //        msg2 = msg2.Replace("$DAYS$", (sub.gift2EndDate.Value.DayOfYear - DateTime.Now.DayOfYear).ToString());
                                    //        try
                                    //        {
                                    //            _emailSender.SendSmS(sub.Email,
                                    //                      msg2,
                                    //                      store.SmsUserName,
                                    //                     store.SmsPass,
                                    //                     timeToSend,
                                    //                     store.preMsg);
                                    //        }
                                    //        catch
                                    //        {

                                    //        }

                                    //        sub.gift2SmsCnt += 1;

                                    //    }

                                    //    if (sub.gift3EndDate.HasValue && sub.gift3EndDate.Value.DayOfYear - DateTime.Now.DayOfYear <= store.days2GiftEnd.Value && sub.gift3SmsCnt == 0 && sub.gift3EndDate.Value.DayOfYear - DateTime.Now.DayOfYear >= 0)
                                    //    {

                                    //        var msg2 = store.days2GiftEndMsg.Replace("$GIFT$", store.gift3Name);
                                    //        msg2 = msg2.Replace("$N$", sub.FirstName);
                                    //        msg2 = msg2.Replace("$DAYS$", (sub.gift3EndDate.Value.DayOfYear - DateTime.Now.DayOfYear).ToString());
                                    //        try
                                    //        {
                                    //            _emailSender.SendSmS(sub.Email,
                                    //                      msg2,
                                    //                      store.SmsUserName,
                                    //                     store.SmsPass,
                                    //                     timeToSend,
                                    //                     store.preMsg);
                                    //        }
                                    //        catch
                                    //        {

                                    //        }

                                    //        sub.gift3SmsCnt += 1;

                                    //    }

                                    //    if (sub.gift4EndDate.HasValue && sub.gift4EndDate.Value.DayOfYear - DateTime.Now.DayOfYear <= store.days2GiftEnd.Value && sub.gift4SmsCnt == 0 && sub.gift4EndDate.Value.DayOfYear - DateTime.Now.DayOfYear >= 0)
                                    //    {

                                    //        var msg2 = store.days2GiftEndMsg.Replace("$GIFT$", store.gift4Name);
                                    //        msg2 = msg2.Replace("$N$", sub.FirstName);
                                    //        msg2 = msg2.Replace("$DAYS$", (sub.gift4EndDate.Value.DayOfYear - DateTime.Now.DayOfYear).ToString());
                                    //        try
                                    //        {
                                    //            _emailSender.SendSmS(sub.Email,
                                    //                      msg2,
                                    //                      store.SmsUserName,
                                    //                     store.SmsPass,
                                    //                     timeToSend,
                                    //                     store.preMsg);
                                    //        }
                                    //        catch
                                    //        {

                                    //        }

                                    //        sub.gift4SmsCnt += 1;

                                    //    }


                                }
                                _newsLetterSubscriptionService.UpdateNewsLetterSubscription(sub);
                            }
                        }
                    }

                            //if (DateTime.Today.DayOfWeek == DayOfWeek.Sunday)
                            //{

                            //    string contactMsg = store.contactMsg;
                            //    contactMsg = contactMsg.Replace("$week$", store.weeklyCustomerCountInt.ToString());
                            //    contactMsg = contactMsg.Replace("$month$", store.monthlyCustomerCount.ToString());
                            //    contactMsg = contactMsg.Replace("$total$", store.NumberOfCustomers.ToString());

                            //    store.weeklyCustomerCountInt = 0;


                                
                            //    try
                            //    {
                            //        _emailSender.SendSmS(store.contactPhone,
                            //        contactMsg,
                            //        store.SmsUserName,
                            //       store.SmsPass,
                            //       timeToSend,
                            //       store.preMsg);
                            //    }
                            //    catch
                            //    {

                            //    }




                            //}
                            //if (DateTime.Today.Day == 1)
                            //{
                                //if (!string.IsNullOrWhiteSpace(store.contactMsgMonth))
                                //{

                                //    store.monthlyCustomerCount = 0;
                                //    var msgg = store.contactMsgMonth;
                                //    msgg = msgg.Replace("$bday$", bdayStore.ToString());
                                //    msgg = msgg.Replace("$date2$", date2Store.ToString());
                                //    msgg = msgg.Replace("$visit$", visitStore.ToString());
                                //    try
                                //    {
                                //        _emailSender.SendSmS(store.contactPhone,
                                //      msgg,
                                //      store.SmsUserName,
                                //     store.SmsPass,
                                //     timeToSend,
                                //     store.preMsg);
                                //    }
                                //    catch
                                //    {

                                //    }
                                //}




                            //}
                        
                    
                }
               // store logigic before updating
                store.BdayMsgSentInLastRutine = bdayStore;
                store.lastBdayRutine = DateTime.Now;
                store.Date2MsgSentInLastRutine = date2Store;
                store.lastDate2Rutine = DateTime.Now;
                _storeService.UpdateStore(store);

                //string contactMsg = store.contactMsg;
                //contactMsg = contactMsg.Replace("$Name$", store.CompanyName.ToString());
                //contactMsg = contactMsg.Replace("$StoreName$", store.Name.ToString());
                //contactMsg = contactMsg.Replace("$month$", store.monthlyCustomerCount.ToString());
                //contactMsg = contactMsg.Replace("$total$", store.NumberOfCustomers.ToString());
                //contactMsg = contactMsg.Replace("$gift1Imps$", store.gift1Imps.ToString());
                //contactMsg = contactMsg.Replace("$gift2Imps$", store.gift2Imps.ToString());
                //contactMsg = contactMsg.Replace("$gift1NotImp$", store.gift1NotImp.ToString());
                //contactMsg = contactMsg.Replace("$gift2NotImp$", store.gift2NotImp.ToString());
                //contactMsg = contactMsg.Replace("$TotalPoints$", store.totalCustomersPoints.ToString());
                //contactMsg = contactMsg.Replace("$pointImpsSum$", store.pointImpsSum.ToString());
                //contactMsg = contactMsg.Replace("$pointImpsVisits$", store.pointImpsVisits.ToString());
                //contactMsg = contactMsg.Replace("$TotalVisits$", (store.visits).ToString());
                //try
                //{
                //    _emailSender.SendSmS(store.contactPhone,
                //    contactMsg,
                //    store.SmsUserName,
                //    store.SmsPass,
                //    timeToSend,
                //    store.preMsg);
                //}
                //catch
                //{

                //}

            }
        }
    }
}
