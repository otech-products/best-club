﻿using System;
using Nop.Services.Logging;
using Nop.Services.Tasks;
using Nop.Services.Stores;
using Nop.Core.Domain.Messages;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nop.Services.Messages
{
    /// <summary>
    /// Represents a task for sending queued message 
    /// </summary>
    public partial class WeeklyMsg : ITask
    {
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
        private readonly IStoreService _storeService;

        public WeeklyMsg(IQueuedEmailService queuedEmailService, IStoreService storeService, INewsLetterSubscriptionService newsLetterSubscriptionService,
            IEmailSender emailSender, ILogger logger)
        {
            this._queuedEmailService = queuedEmailService;
            this._emailSender = emailSender;
            this._logger = logger;
            this._newsLetterSubscriptionService = newsLetterSubscriptionService;
            this._storeService = storeService;


        }

        /// <summary>
        /// Executes a task
        /// </summary>
        public virtual void Execute()
        {
            IList<NewsLetterSubscription> subs = null;
            var stores = _storeService.GetAllStores();
            int bdayStore = 0;
            int date2Store = 0;
            int visitStore = 0;
            string timeToSend = "";
            //Dictionary<string, string> PhoneList = new Dictionary<string, string>() ;

            foreach (var store in stores)
            {
                //PhoneList.Clear();
                try
                {
                    subs = _newsLetterSubscriptionService.GetNewsLetterSubscriptions(storeId: store.Id);//, DateOfBirthFrom: DateTime.Now.ad (DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) - DateTime.Now.Day));
                }
                catch
                {

                }






































            }
                }
            }
        }
    