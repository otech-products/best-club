﻿using System;
using Nop.Core;
using Nop.Core.Domain.Messages;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nop.Services.Messages
{
    /// <summary>
    /// Newsletter subscription service interface
    /// </summary>
    public partial interface INewsLetterSubscriptionService
    {
        /// <summary>
        /// Inserts a newsletter subscription
        /// </summary>
        /// <param name="newsLetterSubscription">NewsLetter subscription</param>
        /// <param name="publishSubscriptionEvents">if set to <c>true</c> [publish subscription events].</param>
        void InsertNewsLetterSubscription(NewsLetterSubscription newsLetterSubscription, bool publishSubscriptionEvents = true);

        /// <summary>
        /// Updates a newsletter subscription
        /// </summary>
        /// <param name="newsLetterSubscription">NewsLetter subscription</param>
        /// <param name="publishSubscriptionEvents">if set to <c>true</c> [publish subscription events].</param>
        void UpdateNewsLetterSubscription(NewsLetterSubscription newsLetterSubscription, bool publishSubscriptionEvents = true);

        /// <summary>
        /// Deletes a newsletter subscription
        /// </summary>
        /// <param name="newsLetterSubscription">NewsLetter subscription</param>
        /// <param name="publishSubscriptionEvents">if set to <c>true</c> [publish subscription events].</param>
        void DeleteNewsLetterSubscription(NewsLetterSubscription newsLetterSubscription, bool publishSubscriptionEvents = true);

        /// <summary>
        /// Gets a newsletter subscription by newsletter subscription identifier
        /// </summary>
        /// <param name="newsLetterSubscriptionId">The newsletter subscription identifier</param>
        /// <returns>NewsLetter subscription</returns>
        NewsLetterSubscription GetNewsLetterSubscriptionById(int newsLetterSubscriptionId);

        /// <summary>
        /// Gets a newsletter subscription by newsletter subscription GUID
        /// </summary>
        /// <param name="newsLetterSubscriptionGuid">The newsletter subscription GUID</param>
        /// <returns>NewsLetter subscription</returns>
        NewsLetterSubscription GetNewsLetterSubscriptionByGuid(Guid newsLetterSubscriptionGuid);

        /// <summary>
        /// Gets a newsletter subscription by email and store ID
        /// </summary>
        /// <param name="email">The newsletter subscription email</param>
        /// <param name="storeId">Store identifier</param>
        /// <returns>NewsLetter subscription</returns>
        NewsLetterSubscription GetNewsLetterSubscriptionByEmailAndStoreId(string email, int storeId);

        /// <summary>
        /// Gets the newsletter subscription list
        /// </summary>
        /// <param name="email">Email to search or string. Empty to load all records.</param>
        /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
        /// <param name="storeId">Store identifier. 0 to load all records.</param>
        /// <param name="isActive">Value indicating whether subscriber record should be active or not; null to load all records</param>
        /// <param name="customerRoleId">Customer role identifier. Used to filter subscribers by customer role. 0 to load all records.</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>NewsLetterSubscription entities</returns>
        IPagedList<NewsLetterSubscription> GetAllNewsLetterSubscriptions(string email = null,
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            int storeId = 0, bool? isActive = null, int customerRoleId = 0,
            int pageIndex = 0, int pageSize = int.MaxValue, string FirstName = null, string LastName = null, string Month = null, bool? gif1 = null, int origin = 0, int firstOrigin = 0);


        List<NewsLetterSubscription> GetNewsLetterSubscriptions(

           int storeId = 0,
           string Gender = null, string phone = null, string company = null, string city = null,
           string StreetAddress = null, string StreetAddress2 = null,
           int origin = -999, int? minPoints = null, int? maxPoints = null, bool? gift1 = null, bool? gift2 = null, bool? gift3 = null, bool? gift4 = null,
           DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
           DateTime? DateOfBirthFrom = null, DateTime? DateOfBirthTo = null, DateTime? date2From = null, DateTime? date2To = null
            , DateTime? date3From = null, DateTime? date3To = null, bool multiOrigin = false, bool ezDevice = false, bool facebook=false, bool instagram=false, bool website=false, bool internet=false, bool legacyDb =false, bool other=false,
           string check1 = null, string check2 = null, string check3 = null, string check4 = null, string check5 = null);

        int GetStats(string email = null,
           DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
           int storeId = 0, bool? isActive = null, int customerRoleId = 0,
           int pageIndex = 0, int pageSize = int.MaxValue, string FirstName = null, string LastName = null, string Month = null, bool? gif1 = null, int origin = 0, int firstOrigin = 0);


        NewsLetterSubscription GetNewsLetterSubscriptionByIpAdrressAndStoreId(string IpAddress, int storeId);



        //Task<List<NewsLetterSubscription>> GetNewsLetterSubscriptionsAsync(

        // int storeId = 0,
        // string Gender = null, string phone = null, string company = null, string city = null,
        // string StreetAddress = null, string StreetAddress2 = null,
        // int origin = -999, int? minPoints = null, int? maxPoints = null, bool? gift1 = null, bool? gift2 = null, bool? gift3 = null, bool? gift4 = null,
        // DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
        // DateTime? DateOfBirthFrom = null, DateTime? DateOfBirthTo = null, DateTime? date2From = null, DateTime? date2To = null
        //  , DateTime? date3From = null, DateTime? date3To = null, bool multiOrigin = false, bool ezDevice = false, bool facebook = false, bool instagram = false, bool website = false, bool internet = false, bool legacyDb = false, bool other = false);
    }
}
