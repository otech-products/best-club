﻿using System;
using Nop.Services.Logging;
using Nop.Services.Tasks;
using Nop.Services.Stores;
using Nop.Core.Domain.Messages;
using System.Collections.Generic;

namespace Nop.Services.Messages
{
    public partial class notifyCampeign : ITask
    {
        private readonly ICampaignService _campaignService;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
        private readonly IStoreService _storeService;

        public notifyCampeign( IStoreService storeService, INewsLetterSubscriptionService newsLetterSubscriptionService,
            IEmailSender emailSender, ILogger logger, ICampaignService campaignService)
        {
           
            this._emailSender = emailSender;
            this._logger = logger;
            this._newsLetterSubscriptionService = newsLetterSubscriptionService;
            this._storeService = storeService;
            this._campaignService = campaignService;


        }

        public virtual void Execute()
        {

            var stores = _storeService.GetAllStores();

            foreach (var store in stores)
            {
                if (store.Active)
                {
                    var campaigns = _campaignService.GetAllCampaigns(store.Id);
                    if(campaigns.Count > 0)
                    {
                      if(DateTime.Now.DayOfYear -  campaigns[0].CreatedOnUtc.Value.DayOfYear >= 14)
                        {

                        }   
                    }


                }
            }
        }
    }
}
