﻿using System;
using Nop.Services.Logging;
using Nop.Services.Tasks;
using Nop.Services.Stores;
using Nop.Core.Domain.Messages;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nop.Services.Messages
{
    /// <summary>
    /// Represents a task for sending queued message 
    /// </summary>
    public partial class WeeklyHouseKeepingTask : ITask
    {
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
        private readonly IStoreService _storeService;

        public WeeklyHouseKeepingTask(IQueuedEmailService queuedEmailService, IStoreService storeService, INewsLetterSubscriptionService newsLetterSubscriptionService,
            IEmailSender emailSender, ILogger logger)
        {
            this._queuedEmailService = queuedEmailService;
            this._emailSender = emailSender;
            this._logger = logger;
            this._newsLetterSubscriptionService = newsLetterSubscriptionService;
            this._storeService = storeService;


        }

        /// <summary>
        /// Executes a task
        /// </summary>
        public virtual void Execute()
        {
            IList<NewsLetterSubscription> subs = null;
            var stores = _storeService.GetAllStores();
            int bdayStore = 0;
            int date2Store = 0;
            int visitStore = 0;
            string timeToSend = "";
            //Dictionary<string, string> PhoneList = new Dictionary<string, string>() ;

            foreach (var store in stores)
            {
                //PhoneList.Clear();
                if (store.Active)
                {

                        string contactMsg = store.contactMsg;
                        contactMsg = contactMsg.Replace("$Name$", store.CompanyName.ToString());
                        contactMsg = contactMsg.Replace("$StoreName$", store.Name.ToString());
                        contactMsg = contactMsg.Replace("$week$", store.weeklyCustomerCount);
                        contactMsg = contactMsg.Replace("$total$", store.NumberOfCustomers.ToString());
                        contactMsg = contactMsg.Replace("$Target$", store.weeklyTarget.ToString());











                        try
                        {
                           _emailSender.SendSmS(store.contactPhone,
                           contactMsg,
                           store.SmsUserName,
                           store.SmsPass,
                           timeToSend,
                           store.preMsg);
                        }
                        catch
                        {

                        }

                        




                    
                   














                        }
            }
                }
            }
        }
    