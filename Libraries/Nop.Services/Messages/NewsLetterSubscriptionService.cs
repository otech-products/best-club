using System;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Messages;
using Nop.Data;
using Nop.Services.Customers;
using Nop.Services.Events;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nop.Services.Messages
{
    /// <summary>
    /// Newsletter subscription service
    /// </summary>
    public class NewsLetterSubscriptionService : INewsLetterSubscriptionService
    {
        #region Fields

        private readonly IEventPublisher _eventPublisher;
        private readonly IDbContext _context;
        private readonly IRepository<NewsLetterSubscription> _subscriptionRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly ICustomerService _customerService;

        #endregion

        #region Ctor

        public NewsLetterSubscriptionService(IDbContext context,
            IRepository<NewsLetterSubscription> subscriptionRepository,
            IRepository<Customer> customerRepository,
            IEventPublisher eventPublisher,
            ICustomerService customerService)
        {
            this._context = context;
            this._subscriptionRepository = subscriptionRepository;
            this._customerRepository = customerRepository;
            this._eventPublisher = eventPublisher;
            this._customerService = customerService;
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Publishes the subscription event.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="isSubscribe">if set to <c>true</c> [is subscribe].</param>
        /// <param name="publishSubscriptionEvents">if set to <c>true</c> [publish subscription events].</param>
        private void PublishSubscriptionEvent(string email, bool isSubscribe, bool publishSubscriptionEvents)
        {
            if (publishSubscriptionEvents)
            {
                if (isSubscribe)
                {
                    _eventPublisher.PublishNewsletterSubscribe(email);
                }
                else
                {
                    _eventPublisher.PublishNewsletterUnsubscribe(email);
                }
            }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Inserts a newsletter subscription
        /// </summary>
        /// <param name="newsLetterSubscription">NewsLetter subscription</param>
        /// <param name="publishSubscriptionEvents">if set to <c>true</c> [publish subscription events].</param>
        public virtual void InsertNewsLetterSubscription(NewsLetterSubscription newsLetterSubscription, bool publishSubscriptionEvents = true)
        {
            if (newsLetterSubscription == null)
            {
                throw new ArgumentNullException("newsLetterSubscription");
            }

            //Handle e-mail
            // newsLetterSubscription.Email = CommonHelper.EnsureSubscriberEmailOrThrow(newsLetterSubscription.Email);

            //Persist
            _subscriptionRepository.Insert(newsLetterSubscription);

            //Publish the subscription event 
            if (newsLetterSubscription.Active)
            {
                PublishSubscriptionEvent(newsLetterSubscription.Email, true, publishSubscriptionEvents);
            }

            //Publish event
            _eventPublisher.EntityInserted(newsLetterSubscription);
        }

        /// <summary>
        /// Updates a newsletter subscription
        /// </summary>
        /// <param name="newsLetterSubscription">NewsLetter subscription</param>
        /// <param name="publishSubscriptionEvents">if set to <c>true</c> [publish subscription events].</param>
        public virtual void UpdateNewsLetterSubscription(NewsLetterSubscription newsLetterSubscription, bool publishSubscriptionEvents = true)
        {
            if (newsLetterSubscription == null)
            {
                throw new ArgumentNullException("newsLetterSubscription");
            }

            //Handle e-mail
            //newsLetterSubscription.Email = CommonHelper.EnsureSubscriberEmailOrThrow(newsLetterSubscription.Email);

            //Get original subscription record
            var originalSubscription = _context.LoadOriginalCopy(newsLetterSubscription);

            //Persist
            _subscriptionRepository.Update(newsLetterSubscription);

            //Publish the subscription event 
            if ((originalSubscription.Active == false && newsLetterSubscription.Active) ||
                (newsLetterSubscription.Active && (originalSubscription.Email != newsLetterSubscription.Email)))
            {
                //If the previous entry was false, but this one is true, publish a subscribe.
                PublishSubscriptionEvent(newsLetterSubscription.Email, true, publishSubscriptionEvents);
            }

            if ((originalSubscription.Active && newsLetterSubscription.Active) &&
                (originalSubscription.Email != newsLetterSubscription.Email))
            {
                //If the two emails are different publish an unsubscribe.
                PublishSubscriptionEvent(originalSubscription.Email, false, publishSubscriptionEvents);
            }

            if ((originalSubscription.Active && !newsLetterSubscription.Active))
            {
                //If the previous entry was true, but this one is false
                PublishSubscriptionEvent(originalSubscription.Email, false, publishSubscriptionEvents);
            }

            //Publish event
            _eventPublisher.EntityUpdated(newsLetterSubscription);
        }

        /// <summary>
        /// Deletes a newsletter subscription
        /// </summary>
        /// <param name="newsLetterSubscription">NewsLetter subscription</param>
        /// <param name="publishSubscriptionEvents">if set to <c>true</c> [publish subscription events].</param>
        public virtual void DeleteNewsLetterSubscription(NewsLetterSubscription newsLetterSubscription, bool publishSubscriptionEvents = true)
        {
            if (newsLetterSubscription == null) throw new ArgumentNullException("newsLetterSubscription");

            _subscriptionRepository.Delete(newsLetterSubscription);

            //Publish the unsubscribe event 
            PublishSubscriptionEvent(newsLetterSubscription.Email, false, publishSubscriptionEvents);

            //event notification
            _eventPublisher.EntityDeleted(newsLetterSubscription);
        }

        /// <summary>
        /// Gets a newsletter subscription by newsletter subscription identifier
        /// </summary>
        /// <param name="newsLetterSubscriptionId">The newsletter subscription identifier</param>
        /// <returns>NewsLetter subscription</returns>
        public virtual NewsLetterSubscription GetNewsLetterSubscriptionById(int newsLetterSubscriptionId)
        {
            if (newsLetterSubscriptionId == 0) return null;

            return _subscriptionRepository.GetById(newsLetterSubscriptionId);
        }

        /// <summary>
        /// Gets a newsletter subscription by newsletter subscription GUID
        /// </summary>
        /// <param name="newsLetterSubscriptionGuid">The newsletter subscription GUID</param>
        /// <returns>NewsLetter subscription</returns>
        public virtual NewsLetterSubscription GetNewsLetterSubscriptionByGuid(Guid newsLetterSubscriptionGuid)
        {
            if (newsLetterSubscriptionGuid == Guid.Empty) return null;

            var newsLetterSubscriptions = from nls in _subscriptionRepository.Table
                                          where nls.NewsLetterSubscriptionGuid == newsLetterSubscriptionGuid
                                          orderby nls.Id
                                          select nls;

            return newsLetterSubscriptions.FirstOrDefault();
        }

        /// <summary>
        /// Gets a newsletter subscription by email and store ID
        /// </summary>
        /// <param name="email">The newsletter subscription email</param>
        /// <param name="storeId">Store identifier</param>
        /// <returns>NewsLetter subscription</returns>
        public virtual NewsLetterSubscription GetNewsLetterSubscriptionByEmailAndStoreId(string email, int storeId)
        {
            if (!CommonHelper.IsValidEmail(email))
                return null;

            email = email.Trim();

            var newsLetterSubscriptions = from nls in _subscriptionRepository.Table
                                          where nls.Email == email && nls.StoreId == storeId
                                          orderby nls.Id
                                          select nls;

            return newsLetterSubscriptions.FirstOrDefault();
        }


        public virtual NewsLetterSubscription GetNewsLetterSubscriptionByIpAdrressAndStoreId(string IpAddress, int storeId)
        {
            

            var newsLetterSubscriptions = from nls in _subscriptionRepository.Table
                                          where nls.LastIpAddress == IpAddress && nls.StoreId == storeId
                                          orderby nls.Id
                                          select nls;

            return newsLetterSubscriptions.FirstOrDefault();
        }

        /// <summary>
        /// Gets the newsletter subscription list
        /// </summary>
        /// <param name="email">Email to search or string. Empty to load all records.</param>
        /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
        /// <param name="storeId">Store identifier. 0 to load all records.</param>
        /// <param name="customerRoleId">Customer role identifier. Used to filter subscribers by customer role. 0 to load all records.</param>
        /// <param name="isActive">Value indicating whether subscriber record should be active or not; null to load all records</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>NewsLetterSubscription entities</returns>
        public virtual IPagedList<NewsLetterSubscription> GetAllNewsLetterSubscriptions(string email = null,
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            int storeId = 0, bool? isActive = null, int customerRoleId = 0,
            int pageIndex = 0, int pageSize = int.MaxValue, string FirstName = null, string LastName = null, string Month = null, bool? gif1 = null, int origin = 0, int firstOrigin = 0)
        {
            //if (customerRoleId == 0)
            //{
                //do not filter by customer role
                var query = _subscriptionRepository.Table;
                if (!String.IsNullOrEmpty(email))
                    query = query.Where(nls => nls.Email.Contains(email));
                if (createdFromUtc.HasValue)
                    query = query.Where(nls => nls.CreatedOnUtc >= createdFromUtc.Value);
                if (createdToUtc.HasValue)
                    query = query.Where(nls => nls.CreatedOnUtc <= createdToUtc.Value);
                if (storeId > 0)
                    query = query.Where(nls => nls.StoreId == storeId);
                if (isActive.HasValue)
                    query = query.Where(nls => nls.Active == isActive.Value);
                if (!string.IsNullOrWhiteSpace(FirstName))
                    query = query.Where(nls => nls.FirstName == FirstName);
                if (!string.IsNullOrWhiteSpace(LastName))
                    query = query.Where(nls => nls.LastName == LastName);
                if (origin != 0)
                    query = query.Where(nls => nls.origin == origin);
                if (firstOrigin != 0)
                    query = query.Where(nls => nls.firstOrigin == firstOrigin);
                query = query.OrderBy(nls => nls.CreatedOnUtc);


                var subscriptions = new PagedList<NewsLetterSubscription>(query, pageIndex, pageSize);
                return subscriptions;
           // }
            //else
            //{
            //    //filter by customer role
            //    var guestRole = _customerService.GetCustomerRoleBySystemName(SystemCustomerRoleNames.Guests);
            //    if (guestRole == null)
            //        throw new NopException("'Guests' role could not be loaded");

            //    if (guestRole.Id == customerRoleId)
            //    {
            //        //guests
            //        var query = _subscriptionRepository.Table;
            //        if (!String.IsNullOrEmpty(email))
            //            query = query.Where(nls => nls.Email.Contains(email));
            //        if (createdFromUtc.HasValue)
            //            query = query.Where(nls => nls.CreatedOnUtc >= createdFromUtc.Value);
            //        if (createdToUtc.HasValue)
            //            query = query.Where(nls => nls.CreatedOnUtc <= createdToUtc.Value);
            //        if (storeId > 0)
            //            query = query.Where(nls => nls.StoreId == storeId);
            //        if (isActive.HasValue)
            //            query = query.Where(nls => nls.Active == isActive.Value);
            //        query = query.Where(nls => !_customerRepository.Table.Any(c => c.Email == nls.Email));
            //        query = query.OrderBy(nls => nls.Email);

            //        var subscriptions = new PagedList<NewsLetterSubscription>(query, pageIndex, pageSize);
            //        return subscriptions;
            //    }
            //    else
            //    {
            //        //other customer roles (not guests)
            //        var query = _subscriptionRepository.Table.Join(_customerRepository.Table,
            //            nls => nls.Email,
            //            c => c.Email,
            //            (nls, c) => new
            //            {
            //                NewsletterSubscribers = nls,
            //                Customer = c
            //            });
            //        query = query.Where(x => x.Customer.CustomerRoles.Any(cr => cr.Id == customerRoleId));
            //        if (!String.IsNullOrEmpty(email))
            //            query = query.Where(x => x.NewsletterSubscribers.Email.Contains(email));
            //        if (createdFromUtc.HasValue)
            //            query = query.Where(x => x.NewsletterSubscribers.CreatedOnUtc >= createdFromUtc.Value);
            //        if (createdToUtc.HasValue)
            //            query = query.Where(x => x.NewsletterSubscribers.CreatedOnUtc <= createdToUtc.Value);
            //        if (storeId > 0)
            //            query = query.Where(x => x.NewsletterSubscribers.StoreId == storeId);
            //        if (isActive.HasValue)
            //            query = query.Where(x => x.NewsletterSubscribers.Active == isActive.Value);
            //        query = query.OrderBy(x => x.NewsletterSubscribers.Email);

            //        var subscriptions = new PagedList<NewsLetterSubscription>(query.Select(x => x.NewsletterSubscribers), pageIndex, pageSize);
            //        return subscriptions;
            //    }
            //}
        }


        public virtual List<NewsLetterSubscription> GetNewsLetterSubscriptions(

            int storeId = 0,
           string Gender = null, string phone = null, string company = null, string city = null,
           string StreetAddress = null, string StreetAddress2 = null,
           int origin = -999, int? minPoints = null, int? maxPoints = null, bool? gift1 = null, bool? gift2 = null, bool? gift3 = null, bool? gift4 = null,
           DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
           DateTime? DateOfBirthFrom = null, DateTime? DateOfBirthTo = null, DateTime? date2From = null, DateTime? date2To = null
            , DateTime? date3From = null, DateTime? date3To = null, bool multiOrigin = false, bool ezDevice = false, bool facebook = false, bool instagram = false, bool website = false, bool internet = false, bool legacyDb = false, bool other = false,
           string check1 = null, string check2 = null, string check3 = null, string check4 = null, string check5 = null)

        {
            if(maxPoints.HasValue && maxPoints.Value == 0)
            {
                maxPoints = null;
            }
            if (minPoints.HasValue && minPoints.Value == 0)
            {
                minPoints = null;
            }
            //do not filter by customer role
            var query = _subscriptionRepository.Table;
            query = query.Where(nls => nls.StoreId == storeId);
            query = query.Where(nls => nls.Active == true);
            if (!String.IsNullOrWhiteSpace(Gender))
                query = query.Where(nls => nls.Gender == Gender);
            if (!String.IsNullOrWhiteSpace(phone))
                query = query.Where(nls => nls.phone == phone);
            if (!String.IsNullOrWhiteSpace(city))
                query = query.Where(nls => nls.City == city);
            if (!String.IsNullOrWhiteSpace(company))
                query = query.Where(nls => nls.company == company);
            if (!String.IsNullOrWhiteSpace(StreetAddress))
                query = query.Where(nls => nls.StreetAddress == StreetAddress);
            if (!String.IsNullOrWhiteSpace(StreetAddress2))
                query = query.Where(nls => nls.StreetAddress2 == StreetAddress2);
            //if(origin != -999)
            //    query = query.Where(nls => nls.origin == origin);
            if (minPoints.HasValue && !maxPoints.HasValue)
                query = query.Where(nls => nls.points >= minPoints);
            if (maxPoints.HasValue && !minPoints.HasValue)
                query = query.Where(nls => nls.points <= maxPoints);
            if (maxPoints.HasValue && minPoints.HasValue)
                query = query.Where(nls => nls.points <= maxPoints && nls.points >= minPoints);
            if (gift1.HasValue)
                query = query.Where(nls => nls.gift1 == gift1.Value);
            if (gift2.HasValue)
                query = query.Where(nls => nls.gift2 == gift2.Value);
            if (gift3.HasValue)
                query = query.Where(nls => nls.gift2 == gift2.Value);
            if (gift4.HasValue)
                query = query.Where(nls => nls.gift2 == gift2.Value);
            if (createdFromUtc.HasValue && !createdToUtc.HasValue)
                query = query.Where(nls => nls.CreatedOnUtc >= createdFromUtc.Value);
            if (createdToUtc.HasValue && !createdFromUtc.HasValue)
                query = query.Where(nls => nls.CreatedOnUtc <= createdToUtc.Value);
            if (createdToUtc.HasValue && createdFromUtc.HasValue)
                query = query.Where(nls => nls.CreatedOnUtc <= createdToUtc.Value && nls.CreatedOnUtc >= createdFromUtc);

            if (DateOfBirthFrom.HasValue && !DateOfBirthTo.HasValue)
            {

                query = query.Where(nls => nls.DateOfBirth.Value >= DateOfBirthFrom.Value);

            }

            if (DateOfBirthTo.HasValue && !DateOfBirthFrom.HasValue)
                query = query.Where(nls => nls.DateOfBirth.Value <= DateOfBirthTo.Value);




            if (DateOfBirthFrom.HasValue && DateOfBirthTo.HasValue)
                query = query.Where(nls => nls.DateOfBirth.Value <= DateOfBirthTo.Value && nls.DateOfBirth.Value >= DateOfBirthFrom.Value);

            if (date2From.HasValue && !date2To.HasValue)
                query = query.Where(nls => nls.Date2.Value >= date2From.Value);
            if (date2To.HasValue && !date2From.HasValue)
                query = query.Where(nls => nls.Date2.Value <= date2To.Value);
            if (date2To.HasValue && date2From.HasValue)
                query = query.Where(nls => nls.Date2.Value <= date2To.Value && nls.Date2.Value >= date2From.Value);

            if (date3From.HasValue && !date3To.HasValue)
                query = query.Where(nls => nls.Date3.Value >= date3From.Value);
            if (date3To.HasValue && !date3From.HasValue)
                query = query.Where(nls => nls.Date3.Value <= date3To.Value);
            if (date3To.HasValue && date3From.HasValue)
                query = query.Where(nls => nls.Date3.Value <= date3To.Value && nls.Date3.Value >= date3From.Value);

            if (multiOrigin)
            {
                if (ezDevice) query = query.Where(nls => nls.origin == 1);
                if (facebook) query = query.Where(nls => nls.origin == 2);
                if (instagram) query = query.Where(nls => nls.origin == 3);
                if (website) query = query.Where(nls => nls.origin == 4);
                if (internet) query = query.Where(nls => nls.origin == 5);
                if (legacyDb) query = query.Where(nls => nls.origin == 6);
                if (other) query = query.Where(nls => nls.origin == 7);


            }
            if (!string.IsNullOrEmpty(check1))
            {
                query = query.Where(nls => nls.checkbox1 == check1 || nls.checkbox2 == check1 || nls.checkbox3 == check1 || nls.checkbox4 == check1 || nls.checkbox5 == check1);

            }
            if (!string.IsNullOrEmpty(check2))
            {
                query = query.Where(nls => nls.checkbox1 == check2 || nls.checkbox2 == check2 || nls.checkbox3 == check2 || nls.checkbox4 == check2 || nls.checkbox5 == check2);

            }
            if (!string.IsNullOrEmpty(check3))
            {
                query = query.Where(nls => nls.checkbox1 == check3 || nls.checkbox2 == check3 || nls.checkbox3 == check3 || nls.checkbox4 == check3 || nls.checkbox5 == check3);

            }
            if (!string.IsNullOrEmpty(check4))
            {
                query = query.Where(nls => nls.checkbox1 == check4 || nls.checkbox2 == check4 || nls.checkbox3 == check4 || nls.checkbox4 == check4 || nls.checkbox5 == check4);

            }
            if (!string.IsNullOrEmpty(check5))
            {
                query = query.Where(nls => nls.checkbox1 == check5 || nls.checkbox2 == check5 || nls.checkbox3 == check5 || nls.checkbox4 == check5 || nls.checkbox5 == check5);

            }







            //if (DateOfBirthFrom.HasValue && DateOfBirthTo.HasValue)
            //    query = query.Where(nls => nls.DateOfBirth.Value.DayOfYear <= DateOfBirthTo.Value.DayOfYear && nls.DateOfBirth.Value.DayOfYear >= DateOfBirthFrom.Value.DayOfYear);

            //if (date2From.HasValue && !date2To.HasValue)
            //    query = query.Where(nls => nls.Date2.Value.DayOfYear >= date2From.Value.DayOfYear);
            //if (date2To.HasValue && !date2From.HasValue)
            //    query = query.Where(nls => nls.Date2.Value.DayOfYear <= date2To.Value.DayOfYear);
            //if (date2To.HasValue && date2From.HasValue)
            //    query = query.Where(nls => nls.Date2.Value.DayOfYear <= date2To.Value.DayOfYear && nls.Date2.Value.DayOfYear >= date2From.Value.DayOfYear);

            //if (date3From.HasValue && !date3To.HasValue)
            //    query = query.Where(nls => nls.Date3.Value.DayOfYear >= date3From.Value.DayOfYear);
            //if (date3To.HasValue && !date3From.HasValue)
            //    query = query.Where(nls => nls.Date3.Value.DayOfYear <= date3To.Value.DayOfYear);
            //if (date3To.HasValue && date3From.HasValue)
            //    query = query.Where(nls => nls.Date3.Value.DayOfYear <= date3To.Value.DayOfYear && nls.Date3.Value.DayOfYear >= date3From.Value.DayOfYear);
            // query = query.OrderBy(nls => nls.Email);

            //if (DateOfBirthFrom.HasValue )
            //    query = query.Where(nls => nls.DateOfBirth >= DateOfBirthFrom.Value);
            //if (DateOfBirthTo.HasValue)
            //    query = query.Where(nls => nls.DateOfBirth <= DateOfBirthTo.Value);
            //if (date2From.HasValue)
            //    query = query.Where(nls => nls.Date2 >=date2From.Value);
            //if (date2To.HasValue)
            //    query = query.Where(nls => nls.Date2 >= date2To.Value);
            //if (date3From.HasValue)
            //    query = query.Where(nls => nls.Date3 >= date3From.Value);
            //if (date2To.HasValue)
            //    query = query.Where(nls => nls.Date3 >= date3To.Value);



            var subscriptions = query.ToList();
            return subscriptions;



        }


        //public virtual async Task<List<NewsLetterSubscription>> GetNewsLetterSubscriptionsAync(

        // int storeId = 0,
        //string Gender = null, string phone = null, string company = null, string city = null,
        //string StreetAddress = null, string StreetAddress2 = null,
        //int origin = -999, int? minPoints = null, int? maxPoints = null, bool? gift1 = null, bool? gift2 = null, bool? gift3 = null, bool? gift4 = null,
        //DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
        //DateTime? DateOfBirthFrom = null, DateTime? DateOfBirthTo = null, DateTime? date2From = null, DateTime? date2To = null
        // , DateTime? date3From = null, DateTime? date3To = null, bool multiOrigin = false, bool ezDevice = false, bool facebook = false, bool instagram = false, bool website = false, bool internet = false, bool legacyDb = false, bool other = false)

        //{

        //    //do not filter by customer role
        //    var query = _subscriptionRepository.Table;
        //    query = query.Where(nls => nls.StoreId == storeId);
        //    query = query.Where(nls => nls.Active == true);
        //    if (!String.IsNullOrWhiteSpace(Gender))
        //        query = query.Where(nls => nls.Gender == Gender);
        //    if (!String.IsNullOrWhiteSpace(phone))
        //        query = query.Where(nls => nls.phone == phone);
        //    if (!String.IsNullOrWhiteSpace(city))
        //        query = query.Where(nls => nls.City == city);
        //    if (!String.IsNullOrWhiteSpace(company))
        //        query = query.Where(nls => nls.company == company);
        //    if (!String.IsNullOrWhiteSpace(StreetAddress))
        //        query = query.Where(nls => nls.StreetAddress == StreetAddress);
        //    if (!String.IsNullOrWhiteSpace(StreetAddress2))
        //        query = query.Where(nls => nls.StreetAddress2 == StreetAddress2);
        //    if (origin != -999)
        //        query = query.Where(nls => nls.origin == origin);
        //    if (minPoints.HasValue && !maxPoints.HasValue)
        //        query = query.Where(nls => nls.points >= minPoints);
        //    if (maxPoints.HasValue && !minPoints.HasValue)
        //        query = query.Where(nls => nls.points <= maxPoints);
        //    if (maxPoints.HasValue && minPoints.HasValue)
        //        query = query.Where(nls => nls.points <= maxPoints && nls.points >= minPoints);
        //    if (gift1.HasValue)
        //        query = query.Where(nls => nls.gift1 == gift1.Value);
        //    if (gift2.HasValue)
        //        query = query.Where(nls => nls.gift2 == gift2.Value);
        //    if (gift3.HasValue)
        //        query = query.Where(nls => nls.gift2 == gift2.Value);
        //    if (gift4.HasValue)
        //        query = query.Where(nls => nls.gift2 == gift2.Value);
        //    if (createdFromUtc.HasValue && !createdToUtc.HasValue)
        //        query = query.Where(nls => nls.CreatedOnUtc >= createdFromUtc.Value);
        //    if (createdToUtc.HasValue && !createdFromUtc.HasValue)
        //        query = query.Where(nls => nls.CreatedOnUtc <= createdToUtc.Value);
        //    if (createdToUtc.HasValue && createdFromUtc.HasValue)
        //        query = query.Where(nls => nls.CreatedOnUtc <= createdToUtc.Value && nls.CreatedOnUtc >= createdFromUtc);

        //    if (DateOfBirthFrom.HasValue && !DateOfBirthTo.HasValue)
        //    {

        //        query = query.Where(nls => nls.DateOfBirth.Value >= DateOfBirthFrom.Value);

        //    }

        //    if (DateOfBirthTo.HasValue && !DateOfBirthFrom.HasValue)
        //        query = query.Where(nls => nls.DateOfBirth.Value <= DateOfBirthTo.Value);




        //    if (DateOfBirthFrom.HasValue && DateOfBirthTo.HasValue)
        //        query = query.Where(nls => nls.DateOfBirth.Value <= DateOfBirthTo.Value && nls.DateOfBirth.Value >= DateOfBirthFrom.Value);

        //    if (date2From.HasValue && !date2To.HasValue)
        //        query = query.Where(nls => nls.Date2.Value >= date2From.Value);
        //    if (date2To.HasValue && !date2From.HasValue)
        //        query = query.Where(nls => nls.Date2.Value <= date2To.Value);
        //    if (date2To.HasValue && date2From.HasValue)
        //        query = query.Where(nls => nls.Date2.Value <= date2To.Value && nls.Date2.Value >= date2From.Value);

        //    if (date3From.HasValue && !date3To.HasValue)
        //        query = query.Where(nls => nls.Date3.Value >= date3From.Value);
        //    if (date3To.HasValue && !date3From.HasValue)
        //        query = query.Where(nls => nls.Date3.Value <= date3To.Value);
        //    if (date3To.HasValue && date3From.HasValue)
        //        query = query.Where(nls => nls.Date3.Value <= date3To.Value && nls.Date3.Value >= date3From.Value);

        //    if (multiOrigin)
        //    {
        //        if (ezDevice) query = query.Where(nls => nls.origin == 1);
        //        if (facebook) query = query.Where(nls => nls.origin == 2);
        //        if (instagram) query = query.Where(nls => nls.origin == 3);
        //        if (website) query = query.Where(nls => nls.origin == 4);
        //        if (internet) query = query.Where(nls => nls.origin == 5);
        //        if (legacyDb) query = query.Where(nls => nls.origin == 6);
        //        if (other) query = query.Where(nls => nls.origin == 7);


        //    }







        //    //if (DateOfBirthFrom.HasValue && DateOfBirthTo.HasValue)
        //    //    query = query.Where(nls => nls.DateOfBirth.Value.DayOfYear <= DateOfBirthTo.Value.DayOfYear && nls.DateOfBirth.Value.DayOfYear >= DateOfBirthFrom.Value.DayOfYear);

        //    //if (date2From.HasValue && !date2To.HasValue)
        //    //    query = query.Where(nls => nls.Date2.Value.DayOfYear >= date2From.Value.DayOfYear);
        //    //if (date2To.HasValue && !date2From.HasValue)
        //    //    query = query.Where(nls => nls.Date2.Value.DayOfYear <= date2To.Value.DayOfYear);
        //    //if (date2To.HasValue && date2From.HasValue)
        //    //    query = query.Where(nls => nls.Date2.Value.DayOfYear <= date2To.Value.DayOfYear && nls.Date2.Value.DayOfYear >= date2From.Value.DayOfYear);

        //    //if (date3From.HasValue && !date3To.HasValue)
        //    //    query = query.Where(nls => nls.Date3.Value.DayOfYear >= date3From.Value.DayOfYear);
        //    //if (date3To.HasValue && !date3From.HasValue)
        //    //    query = query.Where(nls => nls.Date3.Value.DayOfYear <= date3To.Value.DayOfYear);
        //    //if (date3To.HasValue && date3From.HasValue)
        //    //    query = query.Where(nls => nls.Date3.Value.DayOfYear <= date3To.Value.DayOfYear && nls.Date3.Value.DayOfYear >= date3From.Value.DayOfYear);
        //    // query = query.OrderBy(nls => nls.Email);

        //    //if (DateOfBirthFrom.HasValue )
        //    //    query = query.Where(nls => nls.DateOfBirth >= DateOfBirthFrom.Value);
        //    //if (DateOfBirthTo.HasValue)
        //    //    query = query.Where(nls => nls.DateOfBirth <= DateOfBirthTo.Value);
        //    //if (date2From.HasValue)
        //    //    query = query.Where(nls => nls.Date2 >=date2From.Value);
        //    //if (date2To.HasValue)
        //    //    query = query.Where(nls => nls.Date2 >= date2To.Value);
        //    //if (date3From.HasValue)
        //    //    query = query.Where(nls => nls.Date3 >= date3From.Value);
        //    //if (date2To.HasValue)
        //    //    query = query.Where(nls => nls.Date3 >= date3To.Value);



        //    Task<NewsLetterSubscription> subscriptions =  query.ToList(); 
        //    return subscriptions;



        //}


        public void resetPointsForSub(string phone, int storeId)
        {
            var sub = GetNewsLetterSubscriptionByEmailAndStoreId(phone, storeId);
            sub.points = 0;
        }

        public Task<List<NewsLetterSubscription>> GetNewsLetterSubscriptionsAsync(int storeId = 0, string Gender = null, string phone = null, string company = null, string city = null, string StreetAddress = null, string StreetAddress2 = null, int origin = -999, int? minPoints = default(int?), int? maxPoints = default(int?), bool? gift1 = default(bool?), bool? gift2 = default(bool?), bool? gift3 = default(bool?), bool? gift4 = default(bool?), DateTime? createdFromUtc = default(DateTime?), DateTime? createdToUtc = default(DateTime?), DateTime? DateOfBirthFrom = default(DateTime?), DateTime? DateOfBirthTo = default(DateTime?), DateTime? date2From = default(DateTime?), DateTime? date2To = default(DateTime?), DateTime? date3From = default(DateTime?), DateTime? date3To = default(DateTime?), bool multiOrigin = false, bool ezDevice = false, bool facebook = false, bool instagram = false, bool website = false, bool internet = false, bool legacyDb = false, bool other = false)
        {
            throw new NotImplementedException();
        }


        public virtual int GetStats(string email = null,
           DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
           int storeId = 0, bool? isActive = null, int customerRoleId = 0,
           int pageIndex = 0, int pageSize = int.MaxValue, string FirstName = null, string LastName = null, string Month = null, bool? gif1 = null, int origin = 0, int firstOrigin = 0)
        {
            
                //do not filter by customer role
                var query = _subscriptionRepository.Table;
                if (!String.IsNullOrEmpty(email))
                    query = query.Where(nls => nls.Email.Contains(email));
                if (createdFromUtc.HasValue)
                    query = query.Where(nls => nls.CreatedOnUtc >= createdFromUtc.Value);
                if (createdToUtc.HasValue)
                    query = query.Where(nls => nls.CreatedOnUtc <= createdToUtc.Value);
                if (storeId > 0)
                    query = query.Where(nls => nls.StoreId == storeId);
                if (isActive.HasValue)
                    query = query.Where(nls => nls.Active == isActive.Value);
                if (!string.IsNullOrWhiteSpace(FirstName))
                    query = query.Where(nls => nls.FirstName == FirstName);
                if (!string.IsNullOrWhiteSpace(LastName))
                    query = query.Where(nls => nls.LastName == LastName);
                if (origin != 0)
                    query = query.Where(nls => nls.origin == origin);
                if (firstOrigin != 0)
                    query = query.Where(nls => nls.firstOrigin == firstOrigin);




                return query.Count();
            
        }

        #endregion
    }
}
