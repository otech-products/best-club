using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Orders;
using System;

namespace Nop.Services.Orders
{
    /// <summary>
    /// Reward point service interface
    /// </summary>
    public partial interface IRewardPointService
    {
        /// <summary>
        /// Load reward point history records
        /// </summary>
        /// <param name="customerId">Customer identifier; 0 to load all records</param>
        /// <param name="showHidden">A value indicating whether to show hidden records (filter by current store if possible)</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Reward point history records</returns>
        IPagedList<RewardPointsHistory> GetRewardPointsHistory(int customerId = 0, bool showHidden = false,
            bool showNotActivated = false, int pageIndex = 0, int pageSize = int.MaxValue);

        /// <summary>
        /// Add reward points history record
        /// </summary>
        /// <param name="customer">Customer</param>
        /// <param name="points">Number of points to add</param>
        /// <param name="storeId">Store identifier</param>
        /// <param name="message">Message</param>
        /// <param name="usedWithOrder">the order for which points were redeemed as a payment</param>
        /// <param name="usedAmount">Used amount</param>
        void AddRewardPointsHistoryEntry(Customer customer,
            int points, int storeId, string message = "",
            Order usedWithOrder = null, decimal usedAmount = 0M);

        /// <summary>
        /// Gets reward points balance
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identifier; pass </param>
        /// <returns>Balance</returns>
        int GetRewardPointsBalance(int customerId, int storeId);

        /// <summary>
        /// Updates the reward point history entry
        /// </summary>
        /// <param name="rewardPointsHistory">Reward point history entry</param>
        void UpdateRewardPointsHistoryEntry(RewardPointsHistory rewardPointsHistory);

        IPagedList<RewardPointsHistory> GetRewardPointsHistory2(int newsLetterSubscriptionId = 0, bool showHidden = false,
          int pageIndex = 0, int pageSize = int.MaxValue);


        IPagedList<RewardPointsHistory> GetRewardPointsHistoryByStoreId(int storeId = 0, bool showHidden = false,
          int pageIndex = 0, int pageSize = int.MaxValue);

        IPagedList<RewardPointsHistory> GetRewardPointsHistoryByParamsAndStoreId(int storeId = 0, string Employee = null, string Message = null, string Email = null, DateTime? createdOnUtc = null, DateTime? createdToUtc = null, bool showHidden = false,
          int pageIndex = 0, int pageSize = int.MaxValue);

        void AddRewardPointsHistoryEntry2(NewsLetterSubscription newsLetterSubscription,
             int incPoints, int dicPoints, int storeId, int sum = 0, string employee = "", string message = "", string giftName = "", DateTime? createdOn = null);

        int GetVisits(int storeId = 0, string giftName = "", DateTime? createdFromUtc = null, DateTime? createdToUtc = null,

   int pageIndex = 0, int pageSize = int.MaxValue, string Month = null, int origin = 0, int firstOrigin = 0);


        int GetGifts(int storeId = 0, string giftName = "", DateTime? createdFromUtc = null, DateTime? createdToUtc = null,

      int pageIndex = 0, int pageSize = int.MaxValue, string Month = null, int origin = 0, int firstOrigin = 0);


        int GetDics(int storeId = 0, DateTime? createdFromUtc = null, DateTime? createdToUtc = null,

       int pageIndex = 0, int pageSize = int.MaxValue, string Month = null, int origin = 0, int firstOrigin = 0, string email = null);


         int GetSums(int storeId = 0, DateTime? createdFromUtc = null, DateTime? createdToUtc = null,

         int pageIndex = 0, int pageSize = int.MaxValue, string Month = null, int origin = 0, int firstOrigin = 0, string email = null);
        void AddRewardPointsHistoryEntry3(NewsLetterSubscription newsLetterSubscription,
          int incPoints, int dicPoints, int storeId, int sum = 0, string employee = "", string message = "", string giftName = "", DateTime? createdOn = null, int pointBalance = 0);

    }
}
