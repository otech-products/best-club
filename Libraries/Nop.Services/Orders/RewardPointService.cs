using System;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Services.Events;
using Nop.Core.Domain.Messages;

namespace Nop.Services.Orders
{
    /// <summary>
    /// Reward point service
    /// </summary>
    public partial class RewardPointService : IRewardPointService
    {
        #region Fields

        private readonly IRepository<RewardPointsHistory> _rphRepository;
        private readonly RewardPointsSettings _rewardPointsSettings;
        private readonly IStoreContext _storeContext;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="rphRepository">RewardPointsHistory repository</param>
        /// <param name="rewardPointsSettings">Reward points settings</param>
        /// <param name="storeContext">Store context</param>
        /// <param name="eventPublisher">Event published</param>
        public RewardPointService(IRepository<RewardPointsHistory> rphRepository,
            RewardPointsSettings rewardPointsSettings,
            IStoreContext storeContext,
            IEventPublisher eventPublisher)
        {
            this._rphRepository = rphRepository;
            this._rewardPointsSettings = rewardPointsSettings;
            this._storeContext = storeContext;
            this._eventPublisher = eventPublisher;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Load reward point history records
        /// </summary>
        /// <param name="customerId">Customer identifier; 0 to load all records</param>
        /// <param name="showHidden">A value indicating whether to show hidden records (filter by current store if possible)</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Reward point history records</returns>
        public virtual IPagedList<RewardPointsHistory> GetRewardPointsHistory(int customerId = 0, bool showHidden = false,
            bool showNotActivated = false, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _rphRepository.Table;
            if (customerId > 0)
                query = query.Where(rph => rph.NewsLetterSubscriptionId == customerId);
            if (!showHidden && !_rewardPointsSettings.PointsAccumulatedForAllStores)
            {
                //filter by store
                var currentStoreId = _storeContext.CurrentStore.Id;
                query = query.Where(rph => rph.StoreId == currentStoreId);
            }
            if (!showNotActivated)
            {
                //show only the points that already activated

                //The function 'CurrentUtcDateTime' is not supported by SQL Server Compact. 
                //That's why we pass the date value
                var nowUtc = DateTime.UtcNow;
                query = query.Where(rph => rph.CreatedOnUtc < nowUtc);
            }

            //update points balance


            query = query.OrderByDescending(rph => rph.CreatedOnUtc).ThenByDescending(rph => rph.Id);

            var records = new PagedList<RewardPointsHistory>(query, pageIndex, pageSize);
            return records;
        }

        public virtual IPagedList<RewardPointsHistory> GetRewardPointsHistory2(int newsLetterSubscriptionId = 0, bool showHidden = false,
           int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _rphRepository.Table;
            if (newsLetterSubscriptionId > 0)
                query = query.Where(rph => rph.NewsLetterSubscriptionId == newsLetterSubscriptionId);

            query = query.OrderByDescending(rph => rph.CreatedOnUtc).ThenByDescending(rph => rph.Id);

            var records = new PagedList<RewardPointsHistory>(query, pageIndex, pageSize);
            return records;
        }

        /// <summary>
        /// Add reward points history record
        /// </summary>
        /// <param name="customer">Customer</param>
        /// <param name="points">Number of points to add</param>
        /// <param name="storeId">Store identifier</param>
        /// <param name="message">Message</param>
        /// <param name="usedWithOrder">The order for which points were redeemed (spent) as a payment</param>
        /// <param name="usedAmount">Used amount</param>
        public virtual void AddRewardPointsHistoryEntry2(NewsLetterSubscription newsLetterSubscription,
            int incPoints, int dicPoints, int storeId, int sum = 0, string employee = "", string message = "", string giftName = "",DateTime? createdOn = null )
        {
            /////////   message is inovice
            if (newsLetterSubscription == null)
                throw new ArgumentNullException("customer");

            if (storeId <= 0)
                throw new ArgumentException("Store ID should be valid");
            DateTime CreatedOnTemp;
            if (createdOn.HasValue)
            {
                CreatedOnTemp = createdOn.Value;
            }
            else
            {
                CreatedOnTemp = DateTime.Now;
            }
            var rph = new RewardPointsHistory
            {
                NewsLetterSubscription = newsLetterSubscription,
                StoreId = storeId,
                incPoints = incPoints,
                dicPoints = dicPoints,
                PointsBalance = newsLetterSubscription.points,
                UsedAmount = sum,
                Message = message,
                Employee = employee,
                CreatedOnUtc = CreatedOnTemp,
                GiftName = giftName,
                //Gift1Name = gift1Name,
                //Gift2Name = gift2Name,
                //Gift3Name = gift3Name,
                //Gift4Name = gift4Name,
                FirstName = newsLetterSubscription.FirstName,
                LastName = newsLetterSubscription.LastName,
                Email = newsLetterSubscription.Email

            };

            _rphRepository.Insert(rph);

            //event notification
            _eventPublisher.EntityInserted(rph);
        }






        public virtual void AddRewardPointsHistoryEntry3(NewsLetterSubscription newsLetterSubscription,
          int incPoints, int dicPoints, int storeId, int sum = 0, string employee = "", string message = "", string giftName = "", DateTime? createdOn = null,int pointBalance=0)
        {
            /////////   message is inovice
            if (newsLetterSubscription == null)
                throw new ArgumentNullException("customer");

            if (storeId <= 0)
                throw new ArgumentException("Store ID should be valid");
            DateTime CreatedOnTemp;
            if (createdOn.HasValue)
            {
                CreatedOnTemp = createdOn.Value;
            }
            else
            {
                CreatedOnTemp = DateTime.Now;
            }
            var rph = new RewardPointsHistory
            {
                NewsLetterSubscription = newsLetterSubscription,
                StoreId = storeId,
                incPoints = incPoints,
                dicPoints = dicPoints,
                PointsBalance = pointBalance,
                UsedAmount = sum,
                Message = message,
                Employee = employee,
                CreatedOnUtc = CreatedOnTemp,
                GiftName = giftName,
                //Gift1Name = gift1Name,
                //Gift2Name = gift2Name,
                //Gift3Name = gift3Name,
                //Gift4Name = gift4Name,
                FirstName = newsLetterSubscription.FirstName,
                LastName = newsLetterSubscription.LastName,
                Email = newsLetterSubscription.Email

            };

            _rphRepository.Insert(rph);

            //event notification
            _eventPublisher.EntityInserted(rph);
        }



        //public virtual void ImportRewardPointsHistoryEntry2(NewsLetterSubscription newsLetterSubscription,
        //    int incPoints, int dicPoints, int storeId, int sum = 0, string employee = "", string message = "", string giftName = "", DateTime? createdOn = null)
        //{
        //    /////////   message is inovice
        //    if (newsLetterSubscription == null)
        //        throw new ArgumentNullException("customer");

        //    if (storeId <= 0)
        //        throw new ArgumentException("Store ID should be valid");
        //    DateTime CreatedOnTemp;
        //    if (createdOn.HasValue)
        //    {
        //        CreatedOnTemp = createdOn.Value;
        //    }
        //    else
        //    {
        //        CreatedOnTemp = DateTime.Now;
        //    }
        //    var rph = new RewardPointsHistory
        //    {
        //        NewsLetterSubscription = newsLetterSubscription,
        //        StoreId = storeId,
        //        incPoints = incPoints,
        //        dicPoints = dicPoints,
        //        PointsBalance = newsLetterSubscription.points,
        //        UsedAmount = sum,
        //        Message = message,
        //        Employee = employee,
        //        CreatedOnUtc = CreatedOnTemp,
        //        GiftName = giftName,
        //        //Gift1Name = gift1Name,
        //        //Gift2Name = gift2Name,
        //        //Gift3Name = gift3Name,
        //        //Gift4Name = gift4Name,
        //        FirstName = newsLetterSubscription.FirstName,
        //        LastName = newsLetterSubscription.LastName,
        //        Email = newsLetterSubscription.Email

        //    };

        //    _rphRepository.Insert(rph);

        //    //event notification
        //    _eventPublisher.EntityInserted(rph);
        //}

        /// <summary>
        /// Gets reward points balance
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        /// <param name="storeId">Store identifier; pass </param>
        /// <returns>Balance</returns>
        public virtual int GetRewardPointsBalance(int customerId, int storeId)
        {
            var query = _rphRepository.Table;
            if (customerId > 0)
                query = query.Where(rph => rph.NewsLetterSubscriptionId == customerId);
            if (!_rewardPointsSettings.PointsAccumulatedForAllStores)
                query = query.Where(rph => rph.StoreId == storeId);
            query = query.OrderByDescending(rph => rph.CreatedOnUtc).ThenByDescending(rph => rph.Id);

            var lastRph = query.FirstOrDefault();
            return lastRph != null ? lastRph.PointsBalance : 0;
        }

        /// <summary>
        /// Updates the reward point history entry
        /// </summary>
        /// <param name="rewardPointsHistory">Reward point history entry</param>
        public virtual void UpdateRewardPointsHistoryEntry(RewardPointsHistory rewardPointsHistory)
        {
            if (rewardPointsHistory == null)
                throw new ArgumentNullException("rewardPointsHistory");

            _rphRepository.Update(rewardPointsHistory);

            //event notification
            _eventPublisher.EntityUpdated(rewardPointsHistory);
        }

        public virtual IPagedList<RewardPointsHistory> GetRewardPointsHistoryByStoreId(int storeId = 0, bool showHidden = false,
         int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _rphRepository.Table;
            if (storeId > 0)
                query = query.Where(rph => rph.StoreId == storeId);

            query = query.OrderByDescending(rph => rph.CreatedOnUtc).ThenByDescending(rph => rph.Id);

            var records = new PagedList<RewardPointsHistory>(query, pageIndex, pageSize);
            return records;

        }

        public IPagedList<RewardPointsHistory> GetRewardPointsHistoryByParamsAndStoreId(int storeId = 0, string Employee = null, string Message = null, string Email = null, DateTime? createdOnUtc = null, DateTime? createdToUtc = null, bool showHidden = false,
          int pageIndex = 1, int pageSize = int.MaxValue)
        {
            var query = _rphRepository.Table;
            if (storeId > 0)

            {
                query = query.Where(rph => rph.StoreId == storeId);
                if (!String.IsNullOrWhiteSpace(Email))
                {
                    query = query.Where(rph => rph.Email == Email);
                }
                if (!String.IsNullOrWhiteSpace(Employee))
                {
                    query = query.Where(rph => rph.Employee == Employee);
                }
                if (!String.IsNullOrWhiteSpace(Message))
                {
                    query = query.Where(rph => rph.Message == Message);
                }
                if (createdOnUtc.HasValue && !createdToUtc.HasValue)
                    query = query.Where(rph => rph.CreatedOnUtc >= createdOnUtc.Value);
                if (createdToUtc.HasValue && !createdOnUtc.HasValue)
                    query = query.Where(rph => rph.CreatedOnUtc <= createdToUtc.Value);
                if (createdToUtc.HasValue && createdOnUtc.HasValue)
                    query = query.Where(rph => rph.CreatedOnUtc <= createdToUtc.Value && rph.CreatedOnUtc >= createdOnUtc);


            }

            query = query.OrderByDescending(rph => rph.CreatedOnUtc).ThenByDescending(rph => rph.Id);
            PagedList<RewardPointsHistory> records;
            try
            {
                records = new PagedList<RewardPointsHistory>(query, pageIndex, pageSize);
            }
            catch (Exception e)
            {
                var x = e.Data;
                var y = e.InnerException;
                records = null;
            }
            return records;


        }

        public void AddRewardPointsHistoryEntry(Customer customer, int points, int storeId, string message = "", Order usedWithOrder = null, decimal usedAmount = 0)
        {
            throw new NotImplementedException();
        }



        public virtual int GetSums(int storeId = 0, DateTime? createdFromUtc = null, DateTime? createdToUtc = null,

          int pageIndex = 0, int pageSize = int.MaxValue, string Month = null, int origin = 0, int firstOrigin = 0, string email = null)
        {
            var query = _rphRepository.Table;
            if (storeId > 0)
                query = query.Where(rph => rph.StoreId == storeId);
            if (createdFromUtc.HasValue)
                query = query.Where(rph => rph.CreatedOnUtc >= createdFromUtc.Value);
            if (createdToUtc.HasValue)
                query = query.Where(rph => rph.CreatedOnUtc <= createdToUtc.Value);
           
            if (!String.IsNullOrEmpty(email))
                query = query.Where(rph => rph.Email.Contains(email));
            int x;
            try
            {
                x  = query.Sum(rph => (int)rph.UsedAmount);
            }
            catch (Exception e)
            {
                x = 0;
            }
            return  x ;

            
        }



        public virtual int GetDics(int storeId = 0, DateTime? createdFromUtc = null, DateTime? createdToUtc = null,

        int pageIndex = 0, int pageSize = int.MaxValue, string Month = null, int origin = 0, int firstOrigin = 0, string email = null)
        {
            var query = _rphRepository.Table;
            if (storeId > 0)
                query = query.Where(rph => rph.StoreId == storeId);
            if (createdFromUtc.HasValue)
                query = query.Where(rph => rph.CreatedOnUtc >= createdFromUtc.Value);
            if (createdToUtc.HasValue)
                query = query.Where(rph => rph.CreatedOnUtc <= createdToUtc.Value);
            if (!String.IsNullOrEmpty(email))
                query = query.Where(rph => rph.Email.Contains(email));


            int x;
            try
            {
                x = query.Sum(rph => (int)rph.dicPoints);
            }
            catch (Exception e)
            {
                x = 0;
            }
            return x;


        }


        public virtual int GetGifts(int storeId = 0,string giftName= "", DateTime? createdFromUtc = null, DateTime? createdToUtc = null,

      int pageIndex = 0, int pageSize = int.MaxValue, string Month = null, int origin = 0, int firstOrigin = 0)
        {
            var query = _rphRepository.Table;
            if (storeId > 0)
                query = query.Where(rph => rph.StoreId == storeId);
            if (createdFromUtc.HasValue)
                query = query.Where(rph => rph.CreatedOnUtc >= createdFromUtc.Value);
            if (createdToUtc.HasValue)
                query = query.Where(rph => rph.CreatedOnUtc <= createdToUtc.Value);
            query = query.Where(rph => rph.GiftName == giftName);



            int x;
            try
            {
                x = query.Count();
            }
            catch (Exception e)
            {
                x = 0;
            }
            return x;


        }

        public virtual int GetVisits(int storeId = 0, string giftName = "", DateTime? createdFromUtc = null, DateTime? createdToUtc = null,

    int pageIndex = 0, int pageSize = int.MaxValue, string Month = null, int origin = 0, int firstOrigin = 0)
        {
            var query = _rphRepository.Table;
            if (storeId > 0)
                query = query.Where(rph => rph.StoreId == storeId);
            if (createdFromUtc.HasValue)
                query = query.Where(rph => rph.CreatedOnUtc >= createdFromUtc.Value);
            if (createdToUtc.HasValue)
                query = query.Where(rph => rph.CreatedOnUtc <= createdToUtc.Value);




            int x;
            try
            {
                x = query.Count();
            }
            catch (Exception e)
            {
                x = 0;
            }
            return x;



        }



        #endregion
    }

}
