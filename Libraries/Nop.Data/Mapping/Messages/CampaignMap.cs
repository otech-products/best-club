using Nop.Core.Domain.Messages;

namespace Nop.Data.Mapping.Messages
{
    public partial class CampaignMap : NopEntityTypeConfiguration<Campaign>
    {
        public CampaignMap()
        {
            this.ToTable("Campaign");
            this.HasKey(ea => ea.Id);

            this.Property(ea => ea.Name).IsRequired();
            this.Property(ea => ea.Subject).IsRequired();
            this.Property(ea => ea.Body).IsRequired();
            this.Property(ea => ea.CreatedOnUtc).HasColumnType("datetime2");
            this.Property(ea => ea.Date2).HasColumnType("datetime2");
            this.Property(ea => ea.Date2To).HasColumnType("datetime2");
            this.Property(ea => ea.Date3).HasColumnType("datetime2");
            this.Property(ea => ea.Date3To).HasColumnType("datetime2");
            this.Property(ea => ea.DateOfBirth).HasColumnType("datetime2");
            
            this.Property(ea => ea.DateOfBirthTo).HasColumnType("datetime2");
            this.Property(ea => ea.CreatedOn).HasColumnType("datetime2");
            this.Property(ea => ea.CreatedTo).HasColumnType("datetime2");
            //campaign.CreatedOnUtcTo = DateTime.MinValue;
            //campaign.Date2 = DateTime.MinValue;
            //campaign.Date2To = DateTime.MinValue;
            //campaign.Date3 = DateTime.MinValue;
            //campaign.Date3To = DateTime.MinValue;
            //campaign.DateOfBirth = DateTime.MinValue;
            //campaign.DontSendBeforeDateUtc = DateTime.MinValue;

            //this.Property(ea => ea.CreatedOnUtc).HasColumnType("datetime2");
        }
    }
}